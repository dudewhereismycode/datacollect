package mx.com.madd.datacollect.activity.inventory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.adapters.SubProductAdapter;
import mx.com.madd.datacollect.model.inventory.TableSubProduct;

public class SubProductActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SubProductAdapter mAdapter;
    Context context;
    Boolean resp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_product);

        Intent myIntent = getIntent();
        String idprincipal = myIntent.getStringExtra("idprincipal");

        recyclerView = (RecyclerView) findViewById(R.id.subproduct_layout);
        GridLayoutManager mGrid = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mGrid);
        recyclerView.setHasFixedSize(true);

        List<TableSubProduct> principalProducts = TableSubProduct.find(TableSubProduct.class,"idprincipal = ?",idprincipal);
        //List<TableSubProduct> principalProducts = TableSubProduct.listAll(TableSubProduct.class);
        mAdapter = new SubProductAdapter(this,principalProducts);
        recyclerView.setAdapter(mAdapter);
        context=getApplicationContext();
    }
}
