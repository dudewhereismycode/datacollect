package mx.com.madd.datacollect.utility;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Toast;
import androidx.core.app.ActivityCompat;

import java.util.Timer;

public class SingleShotLocationProvider {
    public static Location gpsLocation;
    public static int remainseconds;
    public static CountDownTimer timer4gps;
    public static Timer myTimer;
    public static int timecounter=0;
    public static interface LocationCallback {
        public void onNewLocationAvailable(GPSCoordinates location);
    }

    // calls back to calling thread, note this is for low grain: if you want higher precision, swap the
    // contents of the else and if. Also be sure to check gps permission/settings are allowed.
    // call usually takes <10ms

    public static void requestSingleUpdate(final Boolean onlyGPSSatellite,final Boolean requestPanic, final Context context, final LocationCallback callback) {
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        // new
        final String connection;

        final LocationListener gpslocationListener;
        final GpsStatus.Listener gpsStatusListener;
        gpsLocation= new Location("");//provider name is unecessary
        final LocationManager gpslocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isPermited=true;
        SharedPreferences prefs = context.getSharedPreferences("MyPrefsFile", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = context.getSharedPreferences("MyPrefsFile", context.MODE_PRIVATE).edit();
        String dwim_get_loc_alt=prefs.getString("sp_get_loc_alt", "");
        if (dwim_get_loc_alt== null || dwim_get_loc_alt.length()==0)
            editor.putString("sp_get_loc_alt", "NO");
        if (Build.VERSION.SDK_INT >= 23)
            isPermited=checkPermission(context);
        if (isPermited){
            if (onlyGPSSatellite){
                connection=haveNetworkConnection(context);
                gpsLocation.setLatitude(-1);
                gpsLocation.setLongitude(-1);
                gpsLocation.setAccuracy(-1);
                gpsStatusListener=new GpsStatus.Listener(){
                    @Override
                    public void onGpsStatusChanged(int event){
                        if (checkPermission(context)){
                            GpsStatus gpsStatus = locationManager.getGpsStatus(null);
                            if(gpsStatus != null) {
                                Log.d("INFO",""+gpsStatus.getSatellites()) ;

                            }
                        }

                    }
                };
                gpslocationListener = new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        gpsLocation = location;
                        int time;
                        time = 120 - remainseconds;
                        SharedPreferences prefs = context.getSharedPreferences("MyPrefsFile", context.MODE_PRIVATE);
                        String debug = prefs.getString("sp_debug", "");
                        if (debug == null || debug.length() == 0) {
                            debug = "NO";
                        }
                        if (debug.equals("YES")) {
                            Toast.makeText(context.getApplicationContext(), "GPS took " + time + " secs", Toast.LENGTH_LONG).show();
                        }
                    }


                    public void onStatusChanged(String provider, int status, Bundle extras) {
                        Log.d("INFO","On status changed");
                    }

                    public void onProviderEnabled(String provider) {
                    }

                    public void onProviderDisabled(String provider) {
                    }
                };

                int asking4requestUpdates = 1000; // Every 1000 ms = 1 sec
                int minDistance; // Distance of change in meters. if greater than 0 the provider will send only when has changed x meters.
                //SharedPreferences prefs = context.getSharedPreferences("MyPrefsFile", context.MODE_PRIVATE);
                String dwim_distance4update = prefs.getString("sp_distance4update", "");
                if (dwim_distance4update == null || dwim_distance4update.length() == 0) {
                    minDistance = 40;
                } else {
                    minDistance = Integer.parseInt(dwim_distance4update);
                }
                Log.i("DBG", "Min distance" + minDistance);
                gpslocationManager.addGpsStatusListener(gpsStatusListener);
                gpslocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, gpslocationListener);
                int seconds4timer = 120; // 60 seconds x 2 = 2 minutes
                int every = 1000; // 1000 ms =1 sec tic

                timer4gps = new CountDownTimer(Integer.valueOf(seconds4timer) * 1000, every) {
                    public void onTick(long millisUntilFinished) {
                        float rs = millisUntilFinished / 1000;
                        remainseconds = (int) rs;
                        Log.i("secs: ", "" + millisUntilFinished / 1000+" Acc:"+gpsLocation.getAccuracy());
                        if (gpsLocation.getAccuracy() >1 && gpsLocation.getAccuracy() <=100 ) {
                            if (Build.VERSION.SDK_INT >= 23 && context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                                    && context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                Log.i("dbg: ", "Found Location at:" + millisUntilFinished / 1000);
                            }
                            gpslocationManager.removeUpdates(gpslocationListener);
                            String mysource;
                            if (connection.equals("3G"))
                                mysource = "AGPS";
                            else
                                mysource = "GPS";
                            callback.onNewLocationAvailable(new GPSCoordinates(gpsLocation.getLatitude(), gpsLocation.getLongitude(), gpsLocation.getSpeed(), gpsLocation.getAccuracy(), gpsLocation.getProvider(), mysource, getCellId(context), requestPanic));
                            timer4gps.cancel();
                        }

                    }

                    public void onFinish() {
                        if (Build.VERSION.SDK_INT >= 23 && context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                                && context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            SharedPreferences prefs = context.getSharedPreferences("MyPrefsFile", context.MODE_PRIVATE);
                            String debug = prefs.getString("sp_debug", "");
                            if (debug == null || debug.length() == 0) {
                                debug = "NO";
                            }
                            if (debug.equals("YES")) {
                                Toast.makeText(context, "Location services disabled", Toast.LENGTH_SHORT).show();
                            }
                        }
                        gpslocationManager.removeUpdates(gpslocationListener);
                        if (gpsLocation.getLatitude() == -1 && gpsLocation.getLongitude() == -1) {

                            // Getting location by 3g
                            Criteria criteria = new Criteria();
                            criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                            locationManager.requestSingleUpdate(criteria, new LocationListener() {
                                @Override
                                public void onLocationChanged(Location location) {
                                    String mysource = "3G";
                                    callback.onNewLocationAvailable(new GPSCoordinates(location.getLatitude(), location.getLongitude(), location.getSpeed(), location.getAccuracy(), location.getProvider(), mysource, getCellId(context), requestPanic));
                                }

                                @Override
                                public void onStatusChanged(String provider, int status, Bundle extras) {
                                }

                                @Override
                                public void onProviderEnabled(String provider) {
                                }

                                @Override
                                public void onProviderDisabled(String provider) {
                                }
                            }, null);

                        } else {
                            String mysource;
                            if (connection.equals("3G"))
                                mysource = "AGPS";
                            else
                                mysource = "GPS";
                            callback.onNewLocationAvailable(new GPSCoordinates(gpsLocation.getLatitude(), gpsLocation.getLongitude(), gpsLocation.getSpeed(), gpsLocation.getAccuracy(), gpsLocation.getProvider(), mysource, getCellId(context), requestPanic));
                        }

                    }
                };
                timer4gps.start();
            }
            else
            {
                Log.d("INFO","Adquiring New method FUSED");
                if (isNetworkEnabled) {
                    Criteria criteria = new Criteria();
                    criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                    criteria.setAltitudeRequired(false);
                    locationManager.requestSingleUpdate(criteria, new LocationListener() {
                        @Override
                        public void onLocationChanged(Location location) {
                            callback.onNewLocationAvailable(new GPSCoordinates( location.getLatitude(),location.getLongitude(),location.getSpeed(),location.getAccuracy(),location.getProvider(),"WIFI",getCellId(context),requestPanic));
                        }

                        @Override public void onStatusChanged(String provider, int status, Bundle extras) { }
                        @Override public void onProviderEnabled(String provider) { }
                        @Override public void onProviderDisabled(String provider) { }
                    }, null);
                } else {
                    boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (isGPSEnabled) {
                        Criteria criteria = new Criteria();
                        criteria.setAccuracy(Criteria.ACCURACY_FINE);
                        criteria.setAltitudeRequired(false);
                        locationManager.requestSingleUpdate(criteria, new LocationListener() {
                            @Override
                            public void onLocationChanged(Location location) {
                                callback.onNewLocationAvailable(new GPSCoordinates( location.getLatitude(),location.getLongitude(),location.getSpeed(),location.getAccuracy(),location.getProvider(),"WIFI",getCellId(context),requestPanic));
                            }

                            @Override public void onStatusChanged(String provider, int status, Bundle extras) { }
                            @Override public void onProviderEnabled(String provider) { }
                            @Override public void onProviderDisabled(String provider) { }
                        }, null);
                    }
                }
            }
        }
    }


    public static boolean checkPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                //&& ActivityCompat.checkSelfPermission(context, Manifest.permission.INSTALL_LOCATION_PROVIDER) == PackageManager.PERMISSION_GRANTED
                //&& ActivityCompat.checkSelfPermission(context, Manifest.permission.LOCATION_HARDWARE) == PackageManager.PERMISSION_GRANTED
                ;
    }

    private static String haveNetworkConnection(Context context) {
        String state="none";
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                Log.i("INFO","conected to wifi");
                state="WIFI";
                Toast.makeText(context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                Log.i("INFO","conected to 3G");
                Toast.makeText(context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                state="3G";
            }
        }
        else{
            Log.i("INFO", "not connected");
        }
        return state;
    }

    public static class GPSCoordinates {
        public float longitude = -1;
        public float latitude = -1;
        public float speed = -1;
        public float accuracy = -1;
        public String provider = "";
        public String source = "";
        public int cellid = -1;
        public Boolean panic = false;

        public GPSCoordinates(float theLatitude, float theLongitude , float theSpeed, float theAccuracy, String theProvider, String theSource, int theCellid, Boolean thepanic) {
            longitude = theLongitude;
            latitude = theLatitude;
            speed = theSpeed;
            accuracy = theAccuracy;
            provider = theProvider;
            source = theSource;
            cellid = theCellid;
            panic =thepanic;
        }

        public GPSCoordinates( double theLatitude,double theLongitude, double theSpeed, double theAccuracy, String theProvider,String theSource, int theCellid,Boolean thepanic) {
            longitude = (float) theLongitude;
            latitude = (float) theLatitude;
            speed = (float) theSpeed;
            accuracy = (float) theAccuracy;
            provider = theProvider;
            source = theSource;
            cellid = theCellid;
            panic =thepanic;
        }

    }
    private static int getCellId(Context context){
        int cellid;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            GsmCellLocation cellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
            cellid = cellLocation.getCid();
        }
        catch (Exception ex){
            cellid=0;
        }
        return cellid;
    }

}

