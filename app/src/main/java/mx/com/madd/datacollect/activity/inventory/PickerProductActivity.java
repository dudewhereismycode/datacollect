package mx.com.madd.datacollect.activity.inventory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.adapters.PrincipalProductAdapter;
import mx.com.madd.datacollect.activity.listeners.PrincipalProductTouchListener;
import mx.com.madd.datacollect.model.inventory.TablePrincipalProduct;
import mx.com.madd.datacollect.model.inventory.TableSubProduct;

public class PickerProductActivity extends AppCompatActivity {

    int laposicion;
    String type;

    RecyclerView recyclerView;
    PrincipalProductAdapter mAdapter;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picker_product);


        Intent resultIntent = getIntent();
        laposicion = Integer.parseInt(resultIntent.getStringExtra("posicion"));
        type = resultIntent.getStringExtra("type");

        recyclerView = (RecyclerView) findViewById(R.id.principal_layout);
        GridLayoutManager mGrid = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mGrid);
        recyclerView.setHasFixedSize(true);

        List<TablePrincipalProduct> principalProducts = TablePrincipalProduct.listAll(TablePrincipalProduct.class);
        mAdapter = new PrincipalProductAdapter(this,principalProducts);
        recyclerView.setAdapter(mAdapter);
        context=getApplicationContext();


        recyclerView.addOnItemTouchListener(new PrincipalProductTouchListener(getApplicationContext(), new PrincipalProductTouchListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                TextView id = (TextView)view.findViewById(R.id.id_subproduct);
                String data = (String) id.getText();
                List<TablePrincipalProduct> principalProducts = TablePrincipalProduct.find(TablePrincipalProduct.class,"idprincipal = ?",data);
                List<TableSubProduct> subProducts = TableSubProduct.find(TableSubProduct.class,"idprincipal = ?",String.valueOf(principalProducts.get(0).getIdprincipal()));

                for(int index = 0; index < principalProducts.size(); index++){
                    AlertDialog.Builder builderSingle = new AlertDialog.Builder(PickerProductActivity.this);
                    builderSingle.setTitle("Selecciona");

                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(PickerProductActivity.this,android.R.layout.select_dialog_singlechoice);
                    ArrayList<String> values = new ArrayList<String>();
                    if(!values.contains(principalProducts.get(0).getSeries1()) && !(principalProducts.get(0).getSeries1().equals("0"))){
                        values.add(principalProducts.get(0).getSeries1());
                        arrayAdapter.add(principalProducts.get(0).getSeries1());
                    }

                    if(!values.contains(principalProducts.get(0).getSeries2())  && !(principalProducts.get(0).getSeries2().equals("0"))){
                        values.add(principalProducts.get(0).getSeries2());
                        arrayAdapter.add(principalProducts.get(0).getSeries2());
                    }

                    for(int index2 = 0; index2 < subProducts.size(); index2++){
                        if(!values.contains(subProducts.get(index2).getSeries1())  && !(subProducts.get(0).getSeries1().equals("0"))){
                            values.add(subProducts.get(index2).getSeries1());
                            arrayAdapter.add(subProducts.get(index2).getSeries1());
                        }

                        if(!values.contains(subProducts.get(index2).getSeries2())  && !(subProducts.get(0).getSeries2().equals("0"))){
                            values.add(subProducts.get(index2).getSeries2());
                            arrayAdapter.add(subProducts.get(index2).getSeries2());
                        }
                    }

                    builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String data = arrayAdapter.getItem(which);
                            chooseItem(data);
                        }
                    });

                    builderSingle.show();
                }
            }
        }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

    public void chooseItem(String dato) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("text",dato);
        returnIntent.putExtra("number",dato);
        returnIntent.putExtra("position",Integer.toString(laposicion));
        returnIntent.putExtra("type",type);
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }
}
