package mx.com.madd.datacollect.model;

import com.orm.SugarRecord;

public class TableFormFields extends SugarRecord {
    String typeoption, value, url_post, url_validate, field_name,type;
    int id_form, id_field, required, longitud, position,version,id_savedForm, inventory, id_type;


    public TableFormFields() {
    }

    public TableFormFields(String typeoption, String value, String url_post, String url_validate, String field_name, String type, int id_form, int id_field, int required, int longitud, int position, int version, int id_savedForm, int inventory, int id_type) {
        this.typeoption = typeoption;
        this.value = value;
        this.url_post = url_post;
        this.url_validate = url_validate;
        this.field_name = field_name;
        this.type = type;
        this.id_form = id_form;
        this.id_field = id_field;
        this.required = required;
        this.longitud = longitud;
        this.position = position;
        this.version = version;
        this.id_savedForm = id_savedForm;
        this.inventory = inventory;
        this.id_type = id_type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId_type() {
        return id_type;
    }

    public void setId_type(int id_type) {
        this.id_type = id_type;
    }

    public String getTypeOption() {
        return typeoption;
    }

    public void setTypeOption(String typeoption) {
        this.typeoption = typeoption;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUrlPost() {
        return url_post;
    }

    public void setUrlPost(String url_post) {
        this.url_post = url_post;
    }

    public String getUrlValidate() {
        if(url_validate == null){
            return "";
        }else {
            return url_validate;
        }
    }

    public void setUrlValidate(String url_validate) {
        this.url_validate = url_validate;
    }

    public String getFieldName() {
        return field_name;
    }

    public void setFieldName(String field_name) {
        this.field_name = field_name;
    }

    public int getId_form() {
        return id_form;
    }

    public void setId_form(int id_form) {
        this.id_form = id_form;
    }

    public int getId_field() {
        return id_field;
    }

    public void setId_field(int id_field) {
        this.id_field = id_field;
    }

    public int getRequired() {
        return required;
    }

    public void setRequired(int required) {
        this.required = required;
    }

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getId_savedForm() {
        return id_savedForm;
    }

    public void setId_savedForm(int id_savedForm) {
        this.id_savedForm = id_savedForm;
    }

    public int getInventory() {
        return inventory;
    }

    public void setInventory(int inventory) {
        this.inventory = inventory;
    }
}
