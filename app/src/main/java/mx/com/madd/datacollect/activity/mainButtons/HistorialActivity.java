package mx.com.madd.datacollect.activity.mainButtons;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import mx.com.madd.datacollect.R;

public class HistorialActivity extends AppCompatActivity {

    DatePickerDialog datepickerdialog;
    public final Calendar c = Calendar.getInstance();
    final int mes = c.get(Calendar.MONTH);
    final int dia = c.get(Calendar.DAY_OF_MONTH);
    final int anio = c.get(Calendar.YEAR);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);final EditText startDate = (EditText)findViewById(R.id.startDate);
        startDate.setFocusable(false);
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vv) {
                datepickerdialog = new DatePickerDialog(HistorialActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        if(month<9){
                            startDate.setText(dayOfMonth + "/0" + (month + 1) + "/" + year);
                        }else{
                            startDate.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                        }

                    }
                }, anio, mes, dia);
                datepickerdialog.show();
            }
        });

        final EditText endDate = (EditText)findViewById(R.id.endDate);
        endDate.setFocusable(false);
        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vv) {
                datepickerdialog = new DatePickerDialog(HistorialActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        if(month<9){
                            endDate.setText(dayOfMonth + "/0" + (month + 1) + "/" + year);
                        }else{
                            endDate.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                        }
                    }
                }, anio, mes, dia);
                datepickerdialog.show();
            }
        });
    }

    public void goToMars(View v){
        if(haveNetworkConnection(HistorialActivity.this)){
            EditText startDate = (EditText)findViewById((R.id.startDate));
            EditText endDate = (EditText)findViewById((R.id.endDate));
            CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
            String searchby = "no";
            if (checkBox.isChecked()) {
                searchby = "si";
            }
            if(startDate.getText().length()>0){
                if(endDate.getText().length()>0){
                    Intent myIntent = new Intent(HistorialActivity.this, GetFormsActivity.class);
                    myIntent.putExtra("start",startDate.getText().toString());
                    myIntent.putExtra("end",endDate.getText().toString());
                    myIntent.putExtra("searchby",searchby);
                    startActivityForResult(myIntent,0);
                    finish();
                }else{
                    Toast.makeText(HistorialActivity.this,"Fecha final invalida",Toast.LENGTH_LONG).show();
                }
            }else{
                Toast.makeText(HistorialActivity.this,"Fecha inicial invalida",Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(HistorialActivity.this,"Necesita internet para consultar su historial",Toast.LENGTH_LONG).show();
        }
    }

    private boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
