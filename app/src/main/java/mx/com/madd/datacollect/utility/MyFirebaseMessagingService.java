package mx.com.madd.datacollect.utility;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "INFO";
    private static Context context;
    public static  Boolean successUploadFlag = true;
    private String lostpositions="";
    public static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Handle data payload of FCM messages.
        Log.d(TAG, "FCM Message Id: " + remoteMessage.getMessageId());
        Log.d(TAG, "FCM Notification Message: " + remoteMessage.getNotification());
        Log.d(TAG, "FCM Data Message: " + remoteMessage.getData());
        context=getApplicationContext();
        //
        if (remoteMessage.getNotification()!=null) {
            String title,body;
            title = remoteMessage.getNotification().getTitle();
            body = remoteMessage.getNotification().getBody();
            Log.d(TAG, "FCM SEND Alarm Notification!!!: " + remoteMessage.getData());
            ToolBox newtool = new ToolBox();
            newtool.sendNotification(title,body,getApplicationContext());
        }
        else {

            String command = (String) remoteMessage.getData().get("command");

            if (command.equals("request4GPSlocation")) {
                Log.d("INFO", "REQUEST4GPSLOCATION");
                Thread thread = new Thread(){
                    public void run(){
                        Looper.prepare();//Call looper.prepare()
                        ToolBox newtool = new ToolBox();
                        Handler nhandler=new httpHandler();
                        Bundle bundle = new Bundle();
                        Message msg= new Message();
                        bundle.putString("urldata", "location"+newtool.getUserIdentifier(context));
                        msg.setData(bundle);
                        nhandler.sendMessage(msg);
                        Looper.loop();
                    }
                };
                thread.setName("request4GPSlocation");
                thread.start();
            }
            if (command.equals("message")) {
                String title = (String) remoteMessage.getData().get("title");
                String body = (String) remoteMessage.getData().get("body");
                Log.d(TAG, "Receive Message:"+title+":"+body);
                ToolBox newtool = new ToolBox();
                newtool.sendNotification(title,body,getApplicationContext());
            }
            if (command.equals("request4LostPos")) {
                Log.d("INFO", "request4LostPos");
                updateLostPositions();
            }
        }
    }
    private static class httpHandler extends Handler {
        public void handleMessage(Message msg) {
            // Go to Main thread to launch async task from  Firebase Service
            Bundle bundle = msg.getData();
            String urldata = bundle.getString("urldata");
            Log.d(TAG,"http url handler:"+urldata);
            LocationHttpUrl httpTask = new LocationHttpUrl(urldata,context, new OnEventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject result) {
                    Log.d(TAG, "Location sent to webapp");

                }
                @Override
                public void onFailure(Exception e) {
                    Log.d(TAG, "Location can't sent to webapp");
                }
            });
            httpTask.execute();
        }
    };
    private static class losthttpHandler extends Handler {
        public void handleMessage(Message msg) {
            // Go to Main thread to launch async task from  Firebase Service
            Bundle bundle = msg.getData();
            String urldata = bundle.getString("urldata");
            Log.d(TAG,"http url handler:"+urldata);

            HttpUrl httpTask = new HttpUrl(urldata,context, new OnEventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject result) {
                    Log.d(TAG, "Location sent to webapp");

                }
                @Override
                public void onFailure(Exception e) {
                    Log.d(TAG, "Location can't sent to webapp");
                    successUploadFlag=false;
                }
            });
            httpTask.execute();
        }
    };
    public void updateLostPositions(){
        SharedPreferences prefs = context.getSharedPreferences("MyPrefsFile", context.MODE_PRIVATE);
        String dwim_json = prefs.getString("jArray", "");
        if (dwim_json == null || dwim_json.length() == 0) {
            Log.i("INFO", "No lost positions available" );
        } else {

            try {
                successUploadFlag=true;
                JSONArray jsonArray = new JSONArray(dwim_json);
                Log.d("INFO", "json provider send:" + jsonArray.toString());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject arrayElement = jsonArray.getJSONObject(i);
                    String latitude=arrayElement.getString("latitude");
                    String longitude=arrayElement.getString("longitude");
                    String utc=arrayElement.getString("utc");
                    String speed=arrayElement.getString("speed");
                    String battery=arrayElement.getString("battery");
                    String accuracy=arrayElement.getString("accuracy");
                    String source=arrayElement.getString("source");
                    String cellid=arrayElement.getString("cellid");
                    ToolBox newtool = new ToolBox();
                    lostpositions = "lostpositions"+newtool.getUserIdentifier(context)+"&latitude="+latitude+"&longitude="+longitude+"&speed="+speed+"&battery="+battery+"&date="+utc+"&accuracy="+accuracy+"&source="+source+"&cellid="+cellid;
                    Thread thread = new Thread(){
                        public void run(){
                            Looper.prepare();//Call looper.prepare()
                            Handler nhandler=new losthttpHandler();
                            Bundle bundle = new Bundle();
                            Message msg= new Message();
                            bundle.putString("urldata", lostpositions);
                            msg.setData(bundle);
                            nhandler.sendMessage(msg);
                            Looper.loop();
                        }
                    };
                    thread.start();
                }
                if (successUploadFlag) {
                    SharedPreferences mySPrefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
                    SharedPreferences.Editor myeditor = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE).edit();
                    myeditor.remove("jArray");
                    myeditor.apply();
                    myeditor.commit();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}