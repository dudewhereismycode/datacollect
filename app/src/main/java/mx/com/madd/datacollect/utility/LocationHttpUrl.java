package mx.com.madd.datacollect.utility;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Looper;
import android.util.Log;


import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static android.content.Context.BATTERY_SERVICE;

/**
 * Created by Jorge Macias on 27 Ago 2020.
 */

public class LocationHttpUrl {
    private OnEventListener<JSONObject> mCallBack;
    Context mContext;
    public String urldata;
    public Exception mException;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    final Double[] lat = new Double[1];
    final Double[] lon = new Double[1];
    final Float[] spe = new Float[1];
    final Float[] acu = new Float[1];
    private static final String TAG = "INFO";
    public static boolean isNetworkConnected = false;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    public static boolean ready = false;
    Date currentTime;
    Date starttime;
    long diff;
    long diffSeconds;

    public LocationHttpUrl(String urlparam, Context context, OnEventListener<JSONObject> callback) {
        mCallBack = callback;
        mContext = context;
        urldata = urlparam;
    }

    public void playNotificationSound() {
        SharedPreferences prefs = mContext.getSharedPreferences(MY_PREFS_NAME, mContext.MODE_PRIVATE);
        String dwim_valid_account = prefs.getString("sp_valid_account", "");
        if (dwim_valid_account.equals("YES")) {
            String debug = prefs.getString("sp_debug", "");
            if (debug.equals("YES")) {
                try {

                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(mContext, notification);
                    r.play();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void getLocation() {

    }
    protected void execute() {
        lat[0] = 0.0d;
        lon[0] = 0.0d;
        spe[0] = 0.0f;
        acu[0] = 0.0f;
        playNotificationSound();
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkRequest.Builder builder = new NetworkRequest.Builder();

        connectivityManager.registerDefaultNetworkCallback(new ConnectivityManager.NetworkCallback() {
                                                               @Override
                                                               public void onAvailable(Network network) {
                                                                   isNetworkConnected = true; // Global Static Variable
                                                                   ready = true;
                                                               }

                                                               @Override
                                                               public void onLost(Network network) {
                                                                   isNetworkConnected = false; // Global Static Variable
                                                                   ready = true;
                                                               }
                                                           }

        );
        starttime = Calendar.getInstance().getTime();
        while (!ready) {
            currentTime = Calendar.getInstance().getTime();
            diff = currentTime.getTime() - starttime.getTime();
            diffSeconds = diff / 1000;
            //Log.d(TAG,"Difference:"+diff);
            if (diffSeconds > 30)
                ready = true;
        }
        Log.d(TAG, "Getting Location");
        if (Build.VERSION.SDK_INT < 29) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationRequest = LocationRequest.create();
                //locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                locationRequest.setInterval(20 * 1000);
                locationCallback = new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        if (locationResult == null) {
                            Log.d(TAG, "Latitude: Null");
                            return;
                        }
                        for (Location location : locationResult.getLocations()) {
                            if (location != null) {
                                Log.d(TAG, "Latitude:" + location.getLatitude());
                                lat[0] = locationResult.getLocations().get(0).getLatitude();
                                lon[0] = locationResult.getLocations().get(0).getLongitude();
                                spe[0] = locationResult.getLocations().get(0).getSpeed();
                                acu[0] = locationResult.getLocations().get(0).getAccuracy();
                                String latitude = String.format("%.6f", (double) lat[0]);
                                String longitude = String.format("%.6f", (double) lon[0]);
                                String speed = String.valueOf((double) spe[0]);
                                String accuracy = String.format("%.2f", (double) acu[0]);
                                /*
                                 *
                                 * Implement Battery
                                 *
                                 * */
                                String battery = Integer.toString(100);
                                String cellid = "";
                                Log.d(TAG, "Latitude:" + latitude);
                                Log.d(TAG, "Longitude:" + longitude);
                                Log.d(TAG, "Speed:" + speed);
                                Log.d(TAG, "Accuracy:" + accuracy);
                                // Will contain the raw JSON response as a string.
                                String JsonStr = null;
                                Log.d(TAG, "latitude:" + lat[0]);
                                HttpURLConnection urlConnection = null;
                                if (isNetworkConnected) {
                                    try {
                                        Log.d(TAG, "GET URL");
                                        String surl = "http://road-dwim.dwim.mx/api/noa/" + urldata + "&latitude=" + latitude + "&longitude=" + longitude + "&speed=" + speed + "&battery=" + battery + "&accuracy=" + accuracy + "&source=android" + "&cellid=" + cellid + "&valid=A";
                                        Log.d(TAG, "URL:" + surl);
                                        URL url = new URL(surl);
                                        BufferedReader reader = null;
                                        urlConnection = (HttpURLConnection) url.openConnection();
                                        urlConnection.setRequestMethod("GET");
                                        urlConnection.setConnectTimeout(10000); //set timeout to 5 seconds
                                        urlConnection.connect();
                                        // Read the input stream into a String
                                        InputStream inputStream = urlConnection.getInputStream();
                                        StringBuffer buffer = new StringBuffer();
                                        if (inputStream == null) {
                                            // Nothing to do.
                                            mCallBack.onFailure(mException);
                                        }
                                        reader = new BufferedReader(new InputStreamReader(inputStream));
                                        String line;
                                        while ((line = reader.readLine()) != null) {
                                            // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                                            // But it does make debugging a *lot* easier if you print out the completed
                                            // buffer for debugging.
                                            buffer.append(line + "\n");
                                        }

                                        if (buffer.length() == 0) {
                                            // Stream was empty.  No point in parsing.
                                            Log.d(TAG, "Stream Empty error");
                                            mException = new Exception("Stream Empty error");
                                            mCallBack.onFailure(mException);
                                        }
                                        JsonStr = buffer.toString();
                                        JSONObject obj = null;
                                        try {
                                            obj = new JSONObject(JsonStr);
                                        } catch (Throwable t) {
                                            Log.d(TAG, "Could not parse malformed JSON:");
                                            //Toast.makeText(mContext,"Could not parse malformed JSON:" , Toast.LENGTH_SHORT).show();
                                            mException = new Exception("Could not parse malformed JSON");
                                            mCallBack.onFailure(mException);
                                        }
                                        Log.d(TAG, "Sucess GET");
                                        mCallBack.onSuccess(obj);
                                    } catch (Exception e) {
                                        Log.e("PlaceholderFragment", "Error ", e);
                                        // If the code didn't successfully get the data, there's no point in attemping
                                        // to parse it.
                                        mException = e;
                                        Log.d(TAG, "error:" + e);
                                    } finally {
                                        if (urlConnection != null) {
                                            Log.d(TAG, "Disconnecting");
                                            urlConnection.disconnect();
                                        }
                                    }
                                } else {
                                    mException = new Exception("No Internet!");
                                    Log.d(TAG, "No internet");
                                    mCallBack.onFailure(mException);
                                }
                                LocationServices.getFusedLocationProviderClient(mContext).removeLocationUpdates(locationCallback);
                            }
                        }
                    }
                };
                LocationServices.getFusedLocationProviderClient(mContext).requestLocationUpdates(locationRequest, locationCallback,
                        Looper.myLooper());
            }

        } else {
            //
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                locationRequest = LocationRequest.create();
                //locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                locationRequest.setInterval(20 * 1000);
                locationCallback = new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        if (locationResult == null) {
                            Log.d(TAG, "Latitude: Null");
                            return;
                        }
                        for (Location location : locationResult.getLocations()) {
                            if (location != null) {
                                Log.d(TAG, "Latitude:" + location.getLatitude());
                                lat[0] = locationResult.getLocations().get(0).getLatitude();
                                lon[0] = locationResult.getLocations().get(0).getLongitude();
                                spe[0] = locationResult.getLocations().get(0).getSpeed();
                                acu[0] = locationResult.getLocations().get(0).getAccuracy();
                                String latitude = String.format("%.6f", (double) lat[0]);
                                String longitude = String.format("%.6f", (double) lon[0]);
                                String speed = String.valueOf((double) spe[0]);
                                String accuracy = String.format("%.2f", (double) acu[0]);
                                /*
                                 *
                                 * Implement Battery
                                 *
                                 * */
                                String battery = Integer.toString(100);
                                String cellid = "";
                                Log.d(TAG, "Latitude:" + latitude);
                                Log.d(TAG, "Longitude:" + longitude);
                                Log.d(TAG, "Speed:" + speed);
                                Log.d(TAG, "Accuracy:" + accuracy);
                                // Will contain the raw JSON response as a string.
                                String JsonStr = null;
                                Log.d(TAG, "latitude:" + lat[0]);
                                HttpURLConnection urlConnection = null;
                                if (isNetworkConnected) {
                                    try {
                                        Log.d(TAG, "GET URL");
                                        String surl = "http://road-dwim.dwim.mx/api/noa/" + urldata + "&latitude=" + latitude + "&longitude=" + longitude + "&speed=" + speed + "&battery=" + battery + "&accuracy=" + accuracy + "&source=android" + "&cellid=" + cellid + "&valid=A";
                                        Log.d(TAG, "URL:" + surl);
                                        URL url = new URL(surl);
                                        BufferedReader reader = null;
                                        urlConnection = (HttpURLConnection) url.openConnection();
                                        urlConnection.setRequestMethod("GET");
                                        urlConnection.setConnectTimeout(10000); //set timeout to 5 seconds
                                        urlConnection.connect();
                                        // Read the input stream into a String
                                        InputStream inputStream = urlConnection.getInputStream();
                                        StringBuffer buffer = new StringBuffer();
                                        if (inputStream == null) {
                                            // Nothing to do.
                                            mCallBack.onFailure(mException);
                                        }
                                        reader = new BufferedReader(new InputStreamReader(inputStream));
                                        String line;
                                        while ((line = reader.readLine()) != null) {
                                            // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                                            // But it does make debugging a *lot* easier if you print out the completed
                                            // buffer for debugging.
                                            buffer.append(line + "\n");
                                        }

                                        if (buffer.length() == 0) {
                                            // Stream was empty.  No point in parsing.
                                            Log.d(TAG, "Stream Empty error");
                                            mException = new Exception("Stream Empty error");
                                            mCallBack.onFailure(mException);
                                        }
                                        JsonStr = buffer.toString();
                                        JSONObject obj = null;
                                        try {
                                            obj = new JSONObject(JsonStr);
                                        } catch (Throwable t) {
                                            Log.d(TAG, "Could not parse malformed JSON:");
                                            //Toast.makeText(mContext,"Could not parse malformed JSON:" , Toast.LENGTH_SHORT).show();
                                            mException = new Exception("Could not parse malformed JSON");
                                            mCallBack.onFailure(mException);
                                        }
                                        Log.d(TAG, "Sucess GET");
                                        mCallBack.onSuccess(obj);
                                    } catch (Exception e) {
                                        Log.e("PlaceholderFragment", "Error ", e);
                                        // If the code didn't successfully get the data, there's no point in attemping
                                        // to parse it.
                                        mException = e;
                                        Log.d(TAG, "error:" + e);
                                    } finally {
                                        if (urlConnection != null) {
                                            Log.d(TAG, "Disconnecting");
                                            urlConnection.disconnect();
                                        }
                                    }
                                } else {
                                    mException = new Exception("No Internet!");
                                    Log.d(TAG, "No internet");
                                    mCallBack.onFailure(mException);
                                }
                                LocationServices.getFusedLocationProviderClient(mContext).removeLocationUpdates(locationCallback);
                            }
                        }
                    }
                };
                LocationServices.getFusedLocationProviderClient(mContext).requestLocationUpdates(locationRequest, locationCallback,
                        Looper.myLooper());
            }

        }



    }

    public String getUtc(){
        final Date currentTime = new Date();
        final SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        System.out.println("UTC time: " + sdf.format(currentTime));
        String res=sdf.format(currentTime);
        return res;
    }

    public void storePosition2Memory(String imei,String latitude, String longitude,String speed, String accuracy, String cellid) {
        // TODO Auto-generated method stub
        Log.i("lat lon", "" + latitude + ":" + longitude);
        BatteryManager bm = (BatteryManager) mContext.getSystemService(BATTERY_SERVICE);
        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        String battery = Integer.toString(batLevel);

        SharedPreferences prefs = mContext.getSharedPreferences(MY_PREFS_NAME, mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = mContext.getSharedPreferences(MY_PREFS_NAME, mContext.MODE_PRIVATE).edit();
        //
        String dwim_json = prefs.getString("jArray", "");
        if (dwim_json == null || dwim_json.length() == 0) {
            JSONObject positions = new JSONObject();
            try {
                positions.put("latitude", latitude);
                positions.put("longitude", longitude);
                positions.put("utc", getUtc());
                positions.put("imei", imei);
                positions.put("accuracy", accuracy);
                positions.put("speed", speed);
                positions.put("battery", battery);
                positions.put("source", "Fused");
                positions.put("cellid", cellid);
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(positions);
                editor.putString("jArray", jsonArray.toString());
                editor.commit();
                Log.i("INFO", "json first:" + jsonArray.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {

            try {
                JSONArray jsonArray = new JSONArray(dwim_json);
                JSONObject positions = new JSONObject();
                positions.put("latitude", latitude);
                positions.put("longitude", longitude);
                positions.put("utc", getUtc());
                positions.put("imei", imei);
                positions.put("accuracy", accuracy);
                positions.put("speed", speed);
                positions.put("battery", battery);
                positions.put("source", "Fused");
                positions.put("cellid", cellid);
                jsonArray.put(positions);
                editor.putString("jArray", jsonArray.toString());
                editor.commit();
                Log.i("INFO", "json added:" + jsonArray.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}