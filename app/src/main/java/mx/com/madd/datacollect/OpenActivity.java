package mx.com.madd.datacollect;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mx.com.madd.datacollect.activity.mainButtons.ShowFieldsActivity;
import mx.com.madd.datacollect.activity.mainButtons.ViewPagerActivity;
import mx.com.madd.datacollect.model.TableFormFields;
import mx.com.madd.datacollect.model.TableForms;

public class OpenActivity extends AppCompatActivity {

    int id_form;
    String user, jobID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open);
        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();
        String scheme = intent.getScheme();

        String url = data.toString().substring(data.toString().indexOf("?")+1);
        Log.e("LKYO url",url);
        String[] datos = url.split("&");
        Log.e("LKYO datos",datos[0].toString());
        for(int x=0;x<datos.length;x++){
            String[] keys = datos[x].split("=");

            if(keys[0].equals("form")){
                id_form = Integer.valueOf(keys[1]);
            }
            if(keys[0].equals("user")){
                user = keys[1];
            }
            if(keys[0].equals("jobID")){
                jobID = keys[1];
            }
        }
        String [] whereArgs = new String[1];
        whereArgs[0] = Integer.toString(id_form);
        List<TableForms> fieldData = TableForms.find(TableForms.class, "idform = ?",whereArgs);
        if(fieldData.isEmpty()){
            Log.e("LKYO ESTA VACIO",fieldData.toString());
        }else{
            int version = fieldData.get(0).getVersion();
            String pointer = "";
            String nameForm = fieldData.get(0).getName();

            Intent myIntent = new Intent(OpenActivity.this, ViewPagerActivity.class);
            myIntent.putExtra("id_form",id_form);
            myIntent.putExtra("version",version);
            myIntent.putExtra("pointer",0);
            myIntent.putExtra("nameForm",nameForm);
            myIntent.putExtra("jobID",jobID);
            myIntent.putExtra("editFormId",-1);
            startActivity(myIntent);
            finish();
        }

    }
}