package mx.com.madd.datacollect.utility;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;

import java.io.ByteArrayOutputStream;

import mx.com.madd.datacollect.R;

public class DrawActivity extends AppCompatActivity {

    int positionSign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw);
        positionSign=Integer.parseInt(getIntent().getStringExtra("positionSign"));

        final PaintView paintv = (PaintView)findViewById(R.id.paintView_2);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        paintv.init(metrics);

        Button buttonguardar=(Button)findViewById(R.id.botonguardarfirma);
        Button buttonlimpiar=(Button)findViewById(R.id.botonlimpiarfirma);
        Button buttoncancelar=(Button)findViewById(R.id.botoncancelarfirma);

        buttonguardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PaintView thepaintview=(PaintView)findViewById(R.id.paintView_2);
                thepaintview.setDrawingCacheEnabled(true);
                Bitmap b = thepaintview.getDrawingCache();

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                b.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                Intent intent=new Intent();
                intent.putExtra("firma",byteArray);
                intent.putExtra("dimen",String.valueOf(byteArray.length));
                intent.putExtra("mensaje","Firma Guardada");
                intent.putExtra("laposicion",String.valueOf(positionSign));

                setResult(RESULT_OK,intent);
                finish();
            }
        });

        buttonlimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paintv.clear();
            }
        });


        buttoncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                setResult(12345,intent);
                finish();
            }
        });

    }

    @Override public void onBackPressed() {
        Intent intent=new Intent();
        setResult(12345,intent);
        finish();
    }
}
