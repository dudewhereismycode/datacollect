package mx.com.madd.datacollect.activity.profile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.LoginActivity;
import mx.com.madd.datacollect.activity.MainActivity;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableEnterprise;
import mx.com.madd.datacollect.model.TableFormFields;
import mx.com.madd.datacollect.model.TableFormFieldsOptions;
import mx.com.madd.datacollect.model.TableForms;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.model.dataTypes.DateTimeType;
import mx.com.madd.datacollect.model.dataTypes.DateType;
import mx.com.madd.datacollect.model.dataTypes.ImageType;
import mx.com.madd.datacollect.model.dataTypes.LocationType;
import mx.com.madd.datacollect.model.dataTypes.NumberType;
import mx.com.madd.datacollect.model.dataTypes.OptionType;
import mx.com.madd.datacollect.model.dataTypes.QrType;
import mx.com.madd.datacollect.model.dataTypes.SignType;
import mx.com.madd.datacollect.model.dataTypes.TextType;
import mx.com.madd.datacollect.model.inventory.TablePrincipalProduct;
import mx.com.madd.datacollect.model.inventory.TableProduct;
import mx.com.madd.datacollect.model.inventory.TableSubProduct;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.OnEventListener;

import org.json.JSONObject;

import java.util.List;

public class CreateAccountActivity extends AppCompatActivity {

    EditText name;
    EditText phone;
    EditText email;
    EditText user;
    EditText password;
    EditText confirm_password;
    LinearLayout loader;
    Button createAccountButton;

    String nameString;
    String phoneString;
    String emailString;
    String userString;
    String passwordString;
    String confirm_passwordString;
    String authorization;
    String app_id, internal_id;

    String ok, url_sun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        email = findViewById(R.id.email);
        user = findViewById(R.id.user);
        password = findViewById(R.id.pwd);
        confirm_password = findViewById(R.id.pwdConf);
        createAccountButton = findViewById(R.id.createAccount);
        loader = findViewById(R.id.loader);
        loader.setVisibility(View.GONE);

        List<TableConfiguration> configDB = TableConfiguration.listAll(TableConfiguration.class);

        if(configDB.isEmpty()){
            String userP = "https://api4sun.dudewhereismy.com.mx";
            String formP = "https://api4mars.dudewhereismy.com.mx";
            String inventoryP = "";   //"https://venus.dudewhereismy.com.mx/";
            String ticketP = "";      //https://venus.dudewhereismy.com.mx/";
            String inventoryN = "";
            TableConfiguration newConfig = new TableConfiguration(userP,formP,inventoryP,ticketP,inventoryN);
            newConfig.save();
            url_sun = userP;
        }else{
            url_sun = configDB.get(0).getUserPlatform();
        }
    }

    public void alertError(String error){
        Toast.makeText(this,error,Toast.LENGTH_LONG).show();
        createAccountButton.setVisibility(View.VISIBLE);
        loader.setVisibility(View.GONE);
    }

    public void createAccount(View v){
        createAccountButton.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);

        ok="ok";
        nameString = name.getText().toString();

        if(nameString.length()<5){
            alertError("Nombre muy corto");
            return;
        }
        phoneString = phone.getText().toString();
        if(phoneString.length()<10){
            alertError("Celular muy corto");
            return;
        }
        emailString = email.getText().toString();
        if(emailString.length()<5){
            alertError("Correo electrónico obligatorio");
            return;
        }
        userString = user.getText().toString();
        if(userString.length()<6){
            alertError("Usuario muy corto");
            return;
        }
        passwordString = password.getText().toString();
        if(passwordString.length()<6){
            alertError("Contraseña muy corta");
            return;
        }
        confirm_passwordString = confirm_password.getText().toString();
        if(!passwordString.equals(confirm_passwordString)){
            alertError("Las contraseñas no coinciden");
            return;
        }

        String url = url_sun + "/users";
        String postData = "name="+nameString+"&phone="+phoneString+"&email="+emailString+"&user="+userString+"&password="+passwordString;
        Http webService = new Http(url, "POST", postData, "", getApplicationContext(), new OnEventListener<JSONObject>() {
            @Override
            public void onSuccess(JSONObject response) {
                if(response!=null) {
                    if (response.has("error")) {
                        ok = response.optString("error");
                        if (ok.equals("ok")) {
                            TableUser.deleteAll(TableUser.class);
                            TableForms.deleteAll(TableForms.class);
                            TableFormFieldsOptions.deleteAll(TableFormFieldsOptions.class);
                            TableFormFields.deleteAll(TableFormFields.class);
                            TableConfiguration.deleteAll(TableConfiguration.class);
                            TableEnterprise.deleteAll(TableEnterprise.class);

                            TableSubProduct.deleteAll(TableSubProduct.class);
                            TableProduct.deleteAll(TableProduct.class);
                            TablePrincipalProduct.deleteAll(TablePrincipalProduct.class);

                            TextType.deleteAll(TextType.class);
                            SignType.deleteAll(SignType.class);
                            QrType.deleteAll(QrType.class);
                            OptionType.deleteAll(OptionType.class);
                            NumberType.deleteAll(NumberType.class);
                            LocationType.deleteAll(LocationType.class);
                            ImageType.deleteAll(ImageType.class);
                            DateType.deleteAll(DateType.class);
                            DateTimeType.deleteAll(DateTimeType.class);

                            app_id = response.optString("application_id");
                            internal_id = response.optString("internal_id");

                            String url2 = url_sun + "/login";
                            String postData2 = "username="+userString+"&password="+passwordString;
                            Http webService = new Http(url2, "POST", postData2,"", getApplicationContext(), new OnEventListener<JSONObject>() {
                                @Override
                                public void onSuccess(JSONObject response) {

                                    authorization =  response.optString("Authorized");
                                    alertError("Usuario creado con exito");
                                    TableUser.deleteAll(TableUser.class);
                                    TableUser newUser = new TableUser(nameString, phoneString, emailString, userString, passwordString, authorization, Integer.parseInt(app_id), Integer.parseInt(internal_id));
                                    newUser.save();

                                    AlertDialog dialog = new AlertDialog.Builder(CreateAccountActivity.this)
                                            .setTitle("Pertenecer a un Grupo")
                                            .setMessage("Puedes formar un grupo de trabajo registrando o uniendote a una empresa")
                                            .setIcon(R.drawable.team)
                                            .setPositiveButton("Unirme", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent openJoinActivity = new Intent(getApplicationContext(), JoinActivity.class);
                                                    startActivity(openJoinActivity);
                                                    finish();
                                                }
                                            })
                                            .setNegativeButton("Registrar", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent openEnterpriseActivity = new Intent(getApplicationContext(), CreateEnterpriseActivity.class);
                                                    startActivity(openEnterpriseActivity);
                                                    finish();
                                                }
                                            })
                                            .setNeutralButton("Después", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent openMainActivity = new Intent(getApplicationContext(), MainActivity.class);
                                                    startActivity(openMainActivity);
                                                    finish();
                                                }
                                            })
                                            .setCancelable(false)
                                            .create();
                                    dialog.show();
                                }
                                @Override
                                public void onFailure(Exception e) {
                                }
                            });
                            webService.execute();

                        } else {
                            alertError(ok);
                        }
                    }
                }else{
                    alertError("No se pudo conectar con el servidor");
                }
                createAccountButton.setVisibility(View.VISIBLE);
                loader.setVisibility(View.GONE);
            }
            @Override
            public void onFailure(Exception e) {
                //ok = e.toString();
                alertError("Error en el servidor");
            }
        });
        webService.execute();

    }
}
