package mx.com.madd.datacollect.activity.mainButtons;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.adapters.ClasificationAdapter;
import mx.com.madd.datacollect.activity.listeners.FormsTouchListener;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableClasification;

public class ClasificationActivity extends AppCompatActivity {

    List<TableConfiguration> configDB;

    RecyclerView recyclerView;
    List<TableClasification> formsList;
    ClasificationAdapter mAdapter;
    Boolean show;

    String viewList;
    String message;
    int pointer;

    int position;
    String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clasification);

        configDB = TableConfiguration.listAll(TableConfiguration.class);
        if(configDB.isEmpty()){
            String userP = "https://api4sun.dudewhereismy.com.mx";
            String formP = "https://api4mars.dudewhereismy.com.mx";
            String inventoryP = "";   //"https://venus.dudewhereismy.com.mx/";
            String ticketP = "";      //https://venus.dudewhereismy.com.mx/";
            String inventoryN = "";
            TableConfiguration newConfig = new TableConfiguration(userP,formP,inventoryP,ticketP,inventoryN);
            newConfig.save();
            viewList = "";
        }else{
            viewList = configDB.get(0).getTicketPlatform();
        }

        Intent myIntent = getIntent();
        show =  myIntent.getBooleanExtra("show",false);

        recyclerView = findViewById(R.id.forms_recycler);

        GridLayoutManager mGrid = new GridLayoutManager(this,1);
        recyclerView.setLayoutManager(mGrid);
        recyclerView.setHasFixedSize(true);

        formsList = TableClasification.listAll(TableClasification.class);

        mAdapter = new ClasificationAdapter(this,formsList,show);

        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.addOnItemTouchListener(new FormsTouchListener(getApplicationContext(), new FormsTouchListener.OnItemTouchListener() {
            @Override
            public void onItemClick(View w, int position) {
                TextView nameForm = (TextView)w.findViewById(R.id.name_forms);
                String data = (String) nameForm.getText();

                Intent myIntent = new Intent(ClasificationActivity.this, ShowFieldsActivity.class);
                myIntent.putExtra("clasification",data);
                startActivity(myIntent);

            }
        }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_and_config, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });

        MenuItem itemGear = menu.findItem(R.id.gear);
        if(viewList.equals("Clasificación")){
            itemGear.setTitle("Ver Todo");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.gear) {
            configDB = TableConfiguration.listAll(TableConfiguration.class);
            configDB.get(0).setTicketPlatform("Todo");
            configDB.get(0).save();
            finish();
            return true;
        }
        return true;
    }

}
