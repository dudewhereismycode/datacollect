package mx.com.madd.datacollect.activity.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.LoginActivity;
import mx.com.madd.datacollect.activity.MainActivity;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.OnEventListener;

public class ChangePasswordActivity extends AppCompatActivity {

    EditText oldpass;
    EditText newpass;
    EditText confirmpass;
    Button change;
    LinearLayout loader;

    String url_sun;
    String user;
    String pwd;
    String newpas;
    List<TableUser> userSession;
    List<TableConfiguration> configDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        oldpass = findViewById(R.id.password);
        newpass = findViewById(R.id.newPassword);
        confirmpass = findViewById(R.id.confirmPassword);
        change = findViewById(R.id.changePassword);
        loader = findViewById(R.id.loader);
        loader.setVisibility(View.GONE);

        configDB = TableConfiguration.listAll(TableConfiguration.class);

        if(configDB.isEmpty()){
            String userP = "https://api4sun.dudewhereismy.com.mx";
            String formP = "https://api4mars.dudewhereismy.com.mx";
            String inventoryP = "";   //"https://venus.dudewhereismy.com.mx/";
            String ticketP = "";      //https://venus.dudewhereismy.com.mx/";
            String inventoryN = "";
            TableConfiguration newConfig = new TableConfiguration(userP,formP,inventoryP,ticketP,inventoryN);
            newConfig.save();
            url_sun = userP;
        }else{
            url_sun = configDB.get(0).getUserPlatform();
        }
        userSession = TableUser.listAll(TableUser.class);
        user = userSession.get(0).getUser();
        pwd = userSession.get(0).getPassword();

    }

    public void changePassword(View view){
        change.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);

        String oldpas = oldpass.getText().toString();
        newpas = newpass.getText().toString();
        String confirmpas = confirmpass.getText().toString();
        if(newpas.equals(confirmpas)){
            String url = url_sun + "/users/password";
            String postData = "user="+user+"&password="+newpass.getText().toString();
            String session = userSession.get(0).getSession();
            Http webService = new Http(url, "PUT", postData, session, getApplicationContext(), new OnEventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    if(response.has("error")){
                        String error = response.optString("error");
                        if(error.equals("ok")){
                            userSession.get(0).setPassword(newpas);
                            userSession.get(0).setSession("ok");
                            userSession.get(0).save();
                            finish();
                        }else{
                            Toast.makeText(ChangePasswordActivity.this,error,Toast.LENGTH_LONG).show();
                        }
                    }
                    change.setVisibility(View.VISIBLE);
                    loader.setVisibility(View.GONE);

                }
                @Override
                public void onFailure(Exception e) {
                    change.setVisibility(View.VISIBLE);
                    loader.setVisibility(View.GONE);
                    Toast.makeText(ChangePasswordActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                }
            });
            webService.execute();
        }else{
            change.setVisibility(View.VISIBLE);
            loader.setVisibility(View.GONE);
            Toast.makeText(ChangePasswordActivity.this,"Las nuevas contraseñas no coinciden",Toast.LENGTH_LONG).show();
        }
    }
}
