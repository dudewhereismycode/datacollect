package mx.com.madd.datacollect.activity.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import mx.com.madd.datacollect.R;

public class PrincipalProductViewHolder extends RecyclerView.ViewHolder{
    public TextView productName, loteSeries1, idsubproduct;
    public View mItemView;
    public ImageView imgproduct;

    public PrincipalProductViewHolder(View itemView) {
        super(itemView);
        mItemView = itemView;
        productName = (TextView)itemView.findViewById(R.id.product_name);
        loteSeries1 = (TextView)itemView.findViewById(R.id.lote_series_1);
        imgproduct = (ImageView) itemView.findViewById(R.id.fImage);
        idsubproduct = (TextView) itemView.findViewById(R.id.id_subproduct);
        idsubproduct.setVisibility(View.INVISIBLE);
    }

    public View gettheitemview(){
        return mItemView;
    }
}
