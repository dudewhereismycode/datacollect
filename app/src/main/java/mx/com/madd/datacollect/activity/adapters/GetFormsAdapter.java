package mx.com.madd.datacollect.activity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.model.inventory.TableHistorial;

public class GetFormsAdapter extends RecyclerView.Adapter<GetFormsAdapter.GetFormsViewHolder> implements Filterable {

    Context context;
    private List<TableHistorial> historialList;
    private List<TableHistorial> historialListAll;

    public GetFormsAdapter(Context context, List historialList) {
        this.context = context;
        this.historialList = historialList;
        historialListAll=new ArrayList<>();
        historialListAll.addAll(historialList);
    }


    @Override
    public GetFormsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate our view holder
        LayoutInflater inflater = LayoutInflater.from(context);
        View view1 = inflater.inflate(R.layout.form_list_layout,null);
        return new GetFormsViewHolder(view1);
    }

    @Override
    public void onBindViewHolder(GetFormsViewHolder holder, int position) {

        TableHistorial fruits = historialList.get(position);
        holder.fName.setText(fruits.getName());
        holder.fDesc.setText("Folio: "+fruits.getId_saved_form());
        holder.fid.setText(fruits.getCreated());
    }

    @Override
    public int getItemCount() {
        return historialList.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<TableHistorial> filteredList = new ArrayList<>();
            if(constraint.toString().isEmpty()|| constraint.length()==0){
                filteredList.addAll(historialListAll);
            }else{
                for(TableHistorial product: historialListAll){
                    if(product.getName().toLowerCase().contains(constraint.toString().toLowerCase())){
                        filteredList.add(product);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            historialList.clear();
            historialList.addAll((Collection<? extends TableHistorial>) results.values);
            notifyDataSetChanged();
        }
    };

    public class GetFormsViewHolder extends RecyclerView.ViewHolder {
        TextView fName,fDesc,fid;
        ImageView fImage;


        public GetFormsViewHolder(View itemView) {
            super(itemView);
            fName = itemView.findViewById(R.id.name_forms);
            fDesc = itemView.findViewById(R.id.fecha_forms);
            fImage = itemView.findViewById(R.id.fImage);
            fid = itemView.findViewById(R.id.savedFormId);
            //fid.setVisibility(View.INVISIBLE);


        }
    }
}