package mx.com.madd.datacollect.utility;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUrl extends AsyncTask<Void, Void, String> {
    private OnEventListener<JSONObject> mCallBack;
    Context mContext;
    public String urldata;
    public Exception mException;
    private static final String TAG = "INFO";
    public static boolean isNetworkConnected = false;
    public static boolean ready = false;

    public HttpUrl(String urlparam,Context context, OnEventListener<JSONObject> callback) {
        mCallBack = callback;
        mContext = context;
        urldata=urlparam;
    }

    @Override
    protected void onPreExecute() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkRequest.Builder builder = new NetworkRequest.Builder();

        connectivityManager.registerDefaultNetworkCallback(new ConnectivityManager.NetworkCallback(){
                                                               @Override
                                                               public void onAvailable(Network network) {
                                                                   isNetworkConnected = true; // Global Static Variable
                                                                   ready = true;
                                                               }
                                                               @Override
                                                               public void onLost(Network network) {
                                                                   isNetworkConnected = false; // Global Static Variable
                                                                   ready = true;
                                                               }
                                                           }

        );

        while (!ready) {
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        Log.d(TAG,"HttpUrl in background");
        // Will contain the raw JSON response as a string.
        String JsonStr = null;
        if(isNetworkConnected){
            try {
                Log.d(TAG, "GET URL");
                String surl="http://road-dwim.dwim.mx/api/noa/"+urldata;
                //+"&latitude="+latitude+"&longitude="+longitude+"&speed="+speed+"&battery="+battery+"&accuracy="+accuracy+"&source=android"+"&cellid="+cellid+"&valid=A";
                Log.d(TAG,"URL:"+surl);
                URL url = new URL(surl);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setConnectTimeout(10000); //set timeout to 5 seconds
                urlConnection.connect();
                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return null;
                }
                JsonStr = buffer.toString();

                return JsonStr;
            } catch (Exception e) {
                Log.e("PlaceholderFragment", "Error ", e);
                // If the code didn't successfully get the data, there's no point in attemping
                // to parse it.
                mException = e;
                return null;
            } finally{
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("PlaceholderFragment", "Error closing stream", e);
                    }
                }
            }
        }
        else {
            mException=new Exception("No Internet!");
            Uri uri = Uri.parse(urldata);
            String imei = uri.getQueryParameter("imei");
            return null;
        }
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        Log.d(TAG,"Post Execute HttlUrl");
        if (mCallBack != null) {
            if (mException == null) {
                JSONObject obj=null;
                JSONObject errorjson=null;
                try {
                    String errorstring = "{\"Error\":\"Could not parse malformed JSON\"}";
                    errorjson = new JSONObject(errorstring);
                }
                catch(Throwable t)
                {Log.d(TAG, "Could not parse malformed ERORR JSON");}
                try {
                    obj = new JSONObject(response);
                } catch (Throwable t) {
                    Log.d(TAG, "Could not parse malformed JSON:" );
                    try {
                        mCallBack.onSuccess(errorjson);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    mCallBack.onSuccess(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            mCallBack.onFailure(mException);
        }
    }
}
