package mx.com.madd.datacollect.activity.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.holders.SendFormsViewHolder;
import mx.com.madd.datacollect.model.TableSavedForm;

public class SendFormsAdapter extends RecyclerView.Adapter<SendFormsViewHolder>  implements Filterable {

    private List<TableSavedForm> sendFormObject;
    private List<TableSavedForm> sendFormObjectAll;

    public SendFormsAdapter(Context context, List<TableSavedForm> sendFormObject) {
        this.sendFormObject = sendFormObject;
        sendFormObjectAll=new ArrayList<>();
        sendFormObjectAll.addAll(sendFormObject);
    }

    @Override
    public SendFormsViewHolder onCreateViewHolder(@androidx.annotation.NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.send_layout, parent, false);
        return new SendFormsViewHolder(layoutView);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<TableSavedForm> filteredList = new ArrayList<>();
            if(constraint.toString().isEmpty()|| constraint.length()==0){
                filteredList.addAll(sendFormObjectAll);
            }else{
                for(TableSavedForm product: sendFormObjectAll){
                    if(product.getName_form().toLowerCase().contains(constraint.toString().toLowerCase())){
                        filteredList.add(product);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            sendFormObject.clear();
            sendFormObject.addAll((Collection<? extends TableSavedForm>) results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public void onBindViewHolder(@androidx.annotation.NonNull final SendFormsViewHolder holder, int position) {
        final TableSavedForm currentForms = sendFormObject.get(position);
        String a1=currentForms.getName_form();
        String a2=currentForms.getCreated();
        int a3=currentForms.getId_savedform();
        holder.nameForm.setText(a1);
        holder.createdForm.setText(a2);
        holder.savedFormId.setText(Integer.toString(a3));

        holder.nameForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentForms.setSelected(!currentForms.isSelected());
                currentForms.save();
                holder.itemView.setBackgroundColor(currentForms.isSelected() ? Color.LTGRAY : Color.TRANSPARENT);
            }
        });
        holder.createdForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentForms.setSelected(!currentForms.isSelected());
                currentForms.save();
                holder.itemView.setBackgroundColor(currentForms.isSelected() ? Color.LTGRAY : Color.TRANSPARENT);
            }
        });

        holder.savedFormId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentForms.setSelected(!currentForms.isSelected());
                currentForms.save();
                holder.itemView.setBackgroundColor(currentForms.isSelected() ? Color.LTGRAY : Color.TRANSPARENT);
            }
        });

        holder.imageForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentForms.setSelected(!currentForms.isSelected());
                currentForms.save();
                holder.itemView.setBackgroundColor(currentForms.isSelected() ? Color.LTGRAY : Color.TRANSPARENT);
            }
        });

        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentForms.setSelected(!currentForms.isSelected());
                currentForms.save();
                holder.itemView.setBackgroundColor(currentForms.isSelected() ? Color.LTGRAY : Color.TRANSPARENT);
            }
        });

    }


    @Override
    public int getItemCount() {
        return sendFormObject.size();
    }
}