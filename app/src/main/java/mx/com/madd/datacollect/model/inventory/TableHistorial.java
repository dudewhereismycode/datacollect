package mx.com.madd.datacollect.model.inventory;

public class TableHistorial {

    String name,created;
    int id_saved_form;

    public TableHistorial() {

    }

    public TableHistorial(String name, String created, int id_saved_form) {
        this.name = name;
        this.created = created;
        this.id_saved_form = id_saved_form;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public int getId_saved_form() {
        return id_saved_form;
    }

    public void setId_saved_form(int id_saved_form) {
        this.id_saved_form = id_saved_form;
    }

}
