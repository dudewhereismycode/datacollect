package mx.com.madd.datacollect.activity.mainButtons;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.LoginActivity;
import mx.com.madd.datacollect.activity.adapters.ViewPagerAdapter;
import mx.com.madd.datacollect.activity.inventory.PickerProductActivity;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableFormFields;
import mx.com.madd.datacollect.model.TableFormFieldsOptions;
import mx.com.madd.datacollect.model.TableForms;
import mx.com.madd.datacollect.model.TableSavedForm;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.model.dataTypes.DateTimeType;
import mx.com.madd.datacollect.model.dataTypes.DateType;
import mx.com.madd.datacollect.model.dataTypes.ImageType;
import mx.com.madd.datacollect.model.dataTypes.LocationType;
import mx.com.madd.datacollect.model.dataTypes.NumberType;
import mx.com.madd.datacollect.model.dataTypes.OptionType;
import mx.com.madd.datacollect.model.dataTypes.QrType;
import mx.com.madd.datacollect.model.dataTypes.SignType;
import mx.com.madd.datacollect.model.dataTypes.TextType;
import mx.com.madd.datacollect.model.inventory.TableProduct;
import mx.com.madd.datacollect.utility.DrawActivity;
import mx.com.madd.datacollect.utility.EventListener;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.HttpGetApi;
import mx.com.madd.datacollect.utility.HttpPost;
import mx.com.madd.datacollect.utility.OnEventListener;
import mx.com.madd.datacollect.utility.PaintView;
import mx.com.madd.datacollect.utility.QrActivity;
import mx.com.madd.datacollect.utility.SingleShotLocationProvider;
import mx.com.madd.datacollect.utility.ToolBox;

public class ViewPagerActivity extends AppCompatActivity implements LocationListener {

    int id_form, pointer, version, counter, pointerbefore,formEdit;
    String nameForm,jobID;

    ViewPagerAdapter pagerAdapter;
    ViewPager pager;
    Boolean send_error;
    List<TableFormFields> formFields;

    // Imagen
    List<ImageView> typesimagen = new ArrayList<>();
    // Fecha
    DatePickerDialog datepickerdialog;
    public final Calendar calendar = Calendar.getInstance();
    final int month = calendar.get(Calendar.MONTH);
    final int day = calendar.get(Calendar.DAY_OF_MONTH);
    final int year = calendar.get(Calendar.YEAR);
    EditText layout_fieldValue2; //Para validacion
    // Firma
    List<ImageView> signs=new ArrayList<>();
    // Texto
    List<EditText> text_list=new ArrayList<>();

    // Selected/Radiobutton
    List<String> selected_list = new ArrayList<>();
    // Checked
    List<String> checkbox_list = new ArrayList<>();
    // Numero
    List<EditText> number_list=new ArrayList<>();
    // Ubicacion
    LocationManager locationManager;
    List<EditText> latitud = new ArrayList<>();
    List<EditText> longitud = new ArrayList<>();
    List<EditText> altitud = new ArrayList<>();
    // Qr
    List<EditText> qr_code = new ArrayList<>();
    // Guardar Información
    String alt, lat, lon;
    //Para dialogo
    List<String> hijostodosglobal;

    String inventory_platform;
    View lastView;
    int idForm;

    List<TableSavedForm> sendSavedForms;
    String sendSession;
    List<TableUser> sendUserSession;
    String sendUrlsesion;
    String sendUrlsesiontexto;
    String sendUrlsesionimagen;
    String sendUrlsesionnumero;
    String sendUrlsesionopcion;
    String sendUrlsesionfecha;
    String sendUrlsesionhorafecha;
    String sendUrlsesionfirma;
    String sendUrlsesionubicacion;
    String sendUrlsesionqr;
    String sendUrlsesionfinal;
    String url_mars;
    List<TextType> alltext;
    List<ImageType> allimage;
    List<NumberType> allnumber;
    List<OptionType> alloption;
    List<DateType> alldate;
    List<DateTimeType> alldatetime;
    List<SignType> allsign;
    List<LocationType> alllocation;
    List<QrType> allqr;
    List<TableConfiguration> config;
    ProgressDialog dialogpro;
    int fw,fx,fn,fo,fd,ft,fl,fq,fi,fs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        getLocation();
        Intent myIntent = getIntent();
        id_form =  myIntent.getIntExtra("id_form",0);
        version =  myIntent.getIntExtra("version",0);
        pointer =  myIntent.getIntExtra("pointer",0);
        nameForm =  myIntent.getStringExtra("nameForm");
        formEdit = myIntent.getIntExtra("editFormId",0);
        jobID = myIntent.getStringExtra("jobID");
        config = TableConfiguration.listAll(TableConfiguration.class);
        inventory_platform = "";
        if(config.isEmpty()){
            String userP = "https://api4sun.dudewhereismy.com.mx";
            String formP = "https://api4mars.dudewhereismy.com.mx";
            url_mars = formP;
            String inventoryP = "";   //"https://venus.dudewhereismy.com.mx/";
            String ticketP = "";      //https://venus.dudewhereismy.com.mx/";
            String inventoryN = "";
            TableConfiguration newConfig = new TableConfiguration(userP,formP,inventoryP,ticketP,inventoryN);
            newConfig.save();
        }else{
            url_mars = config.get(0).getFormPlatform();
            inventory_platform = config.get(0).getInventoryPlatform();
        }

        counter = 0;
        alt = "";
        lat = "";
        lon = "";

        Button buttonEnd = findViewById(R.id.buttonEndPager);
        buttonEnd.setVisibility(View.INVISIBLE);
        buttonEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(pagerAdapter.getCount()-1);
            }
        });

        Button buttonStart = findViewById(R.id.buttonStartPager);
        buttonStart.setVisibility(View.INVISIBLE);
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(0);
            }
        });
        pagerAdapter = new ViewPagerAdapter();
        pager = findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);

        buildForm(0,id_form,version,nameForm,"init");

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }

            @Override
            public void onPageSelected(int position) {

                pointerbefore = position - 1;
                if(pointerbefore>=0){
                    View last_layout = pagerAdapter.getView(pointerbefore);

                    TextView layout_fieldname = last_layout.findViewById(R.id.field_name);
                    TextView layout_fieldtype = last_layout.findViewById(R.id.texttipoid);
                    TextView layout_fieldform = last_layout.findViewById(R.id.textidform);
                    int original_form = Integer.parseInt(layout_fieldform.getText().toString());
                    TextView layout_fieldversion = last_layout.findViewById(R.id.textversionform);

                    EditText layout_fieldValue = last_layout.findViewById(R.id.editTextValue);
                    layout_fieldValue2 = last_layout.findViewById(R.id.editTextValue2);
                    EditText layout_fieldValue3 = last_layout.findViewById(R.id.editTextValue3);

                    ImageView layout_fieldImage = last_layout.findViewById(R.id.imageViewValue);
                    RadioGroup layout_fieldSelect = last_layout.findViewById(R.id.opcionesselect);
                    TextView layout_fieldoption = last_layout.findViewById(R.id.tipodeopcionid);

                    String [] whereArgs = new String[4];
                    whereArgs[0] = layout_fieldtype.getText().toString();
                    whereArgs[1] = layout_fieldname.getText().toString();
                    whereArgs[2] = layout_fieldversion.getText().toString();
                    whereArgs[3] = Integer.toString(id_form);

                    List<TableFormFields> fieldData = TableFormFields.find(TableFormFields.class, "idtype = ? and fieldname = ? and version = ? and idform = ?",whereArgs);

                    String url_validate = "";
                    int required = 2;
                    int longitud = -1;

                    if(!fieldData.isEmpty()) {
                        url_validate = fieldData.get(0).getUrlValidate();
                        required = fieldData.get(0).getRequired();
                        longitud = fieldData.get(0).getLongitud();
                    }

                    Boolean ok;

                    if(url_validate.length() > 0){
                        ok = urlValidate(layout_fieldname,url_validate,layout_fieldValue);
                        if(!ok){
                            pager.setCurrentItem(pointerbefore);
                        }
                    }

                    if(required == 1){
                        ok = required(fieldData.get(0).getId_type(),layout_fieldname,layout_fieldoption,layout_fieldValue,layout_fieldImage,layout_fieldSelect, pointerbefore);
                        if(!ok){
                            pager.setCurrentItem(pointerbefore);
                        }
                    }
                    if(longitud > 0){
                        ok = longitud(fieldData.get(0).getId_type(),layout_fieldname,layout_fieldValue,longitud);
                        if(!ok){
                            pager.setCurrentItem(pointerbefore);
                        }
                    }
                }
            }
        });
    }

    // FUNCIONES DE VALIDACION

    public boolean urlValidate(TextView name,String url, EditText value){
        String data = value.getText().toString();
        data = data.replace(" ","%20");
        List<TableUser> userSession = TableUser.listAll(TableUser.class);
        String session = userSession.get(0).getSession();
        String user = userSession.get(0).getUser();
        url += "="+data+"&user="+user+"&appID="+userSession.get(0).getApplication_id();
        HttpGetApi webService = new HttpGetApi(url,  session,getApplicationContext(),new OnEventListener<JSONObject>() {
            @Override
            public void onSuccess(JSONObject object) {
                String okmessage = object.optString("error");
                if (!okmessage.equals("ok")) {
                    Toast.makeText(ViewPagerActivity.this, okmessage, Toast.LENGTH_LONG).show();
                    pager.setCurrentItem(pointerbefore);
                }
            }
            @Override
            public void onFailure(Exception e) {
                //Toast.makeText(ViewPagerActivity.this, "Error en el servidor de validación:"+e.toString(), Toast.LENGTH_LONG).show();
                ToolBox newtool = new ToolBox();
                newtool.sendNotification("Error:",e.toString(),getApplicationContext());
                //Toast.makeText(LoginActivity.this, "Error en el servidor", Toast.LENGTH_LONG).show();
                pager.setCurrentItem(pointerbefore);
            }
        });
        webService.execute();
        return true;
    }

    public boolean required(int id_field,TextView fieldName, TextView name,EditText value, ImageView image, RadioGroup select, int position){
        if(id_field != 2 && id_field != 4 && id_field != 7){
            if(id_field == 6){
                if(value.length()==0 || layout_fieldValue2.getText().length() == 0){
                    Toast.makeText(ViewPagerActivity.this, fieldName.getText()+" Requerido!", Toast.LENGTH_LONG).show();
                    return false;
                }
            }else{
                if(value.length()==0){
                    Toast.makeText(ViewPagerActivity.this, fieldName.getText()+" Requerido!", Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        }else if (id_field != 4){
            if(image.getDrawable() == null){
                Toast.makeText(ViewPagerActivity.this, fieldName.getText()+" Requerido!", Toast.LENGTH_LONG).show();
                return false;
            }
        }else{
            if(name.getText()=="select"){
                int selectedRadioButtonID = select.getCheckedRadioButtonId();
                if (selectedRadioButtonID < 0) {
                    if(formEdit!=-1){
                        int lengthTotal = 0;
                        for(int i=0; i<selected_list.size(); i++){
                            lengthTotal += selected_list.get(i).length();
                        }
                        if(lengthTotal==0){
                            Toast.makeText(ViewPagerActivity.this, fieldName.getText()+" Requerido!", Toast.LENGTH_LONG).show();
                            return false;
                        }
                    }else{
                        Toast.makeText(ViewPagerActivity.this, fieldName.getText()+" Requerido!", Toast.LENGTH_LONG).show();
                        return false;
                    }

                }
            }else{
                if(checkbox_list.get(position).isEmpty()){
                    Toast.makeText(ViewPagerActivity.this, fieldName.getText()+" Requerido!", Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        }
        return true;
    }

    public boolean longitud(int id_field,TextView fieldName,EditText value,int size){
        if(id_field == 1){
            if(value.length()!=size){
                Toast.makeText(ViewPagerActivity.this, fieldName.getText()+" debe tener "+size+" caracteres", Toast.LENGTH_LONG).show();
                return false;
            }
        }else if(id_field == 3){
            String val = value.getText().toString();
            if(val.length() > 0){
                int number = Integer.parseInt(val);
                if(number > size){
                    Toast.makeText(ViewPagerActivity.this, fieldName.getText()+" debe ser menor a "+size, Toast.LENGTH_LONG).show();
                    return false;
                }
            }else{
                Toast.makeText(ViewPagerActivity.this, fieldName.getText()+" debe ser menor a "+size, Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }

    // FUNCIONES PARA CREAR EL FORMULARIO
    private boolean buildForm(int posicionview, final int id_form0,final int version0, final String nameForm0, final String index){

        List<String> allSons = new ArrayList<>();
        final List<String> allGlobalSons = new ArrayList<>();

        String[] whereArgs = new String[2];
        whereArgs[0] = String.valueOf(id_form0);
        whereArgs[1] = String.valueOf(version0);
        List<TableFormFieldsOptions> optionsfields;
        optionsfields = TableFormFieldsOptions.find(TableFormFieldsOptions.class, "idfieldparent = ? and vesion = ?",whereArgs);
        for(int x=0; x < optionsfields.size(); x++){
            allSons.add(String.valueOf(optionsfields.get(x).getId_fieldOption()));
            allGlobalSons.add(String.valueOf(optionsfields.get(x).getId_fieldOption()));
        }

        String optionList = "";
        optionList += optionsBuilder(allSons);

        for(int x = 0; x < optionList.length(); x++){
            String[] split = optionList.split(",");
            if(!(allGlobalSons.contains(optionList.split(",")[x])) && !(optionList.split(",")[x].equals(""))){
                allGlobalSons.add(optionList.split(",")[x]);
            }
        }
        hijostodosglobal = allGlobalSons;

        whereArgs = new  String[2];
        whereArgs[0] = String.valueOf(id_form0);
        whereArgs[1] = String.valueOf(version0);

        formFields = TableFormFields.find(TableFormFields.class,"idform = ? and version = ?", whereArgs,"","position","");
        LayoutInflater layoutInflater = getLayoutInflater();
        List<View> views = new ArrayList<>();

        //Construye todos los datos
        for(int y = 0; y < formFields.size(); y++){
            View layout_view = getLayoutView(formFields.get(y).getId_type(),y,id_form0,version0,nameForm0);

            TextView layout_fieldname = layout_view.findViewById(R.id.field_name);
            layout_fieldname.setText(formFields.get(y).getFieldName());

            TextView layout_fieldtype = layout_view.findViewById(R.id.texttipoid);
            layout_fieldtype.setText(String.valueOf(formFields.get(y).getId_type()));
            layout_fieldtype.setVisibility(View.INVISIBLE);

            TextView layout_fieldform = layout_view.findViewById(R.id.textidform);
            layout_fieldform.setText(String.valueOf(formFields.get(y).getId_form()));
            layout_fieldform.setVisibility(View.INVISIBLE);

            TextView layout_fieldversion = layout_view.findViewById(R.id.textversionform);
            layout_fieldversion.setText(String.valueOf(formFields.get(y).getVersion()));
            layout_fieldversion.setVisibility(View.INVISIBLE);

            if(formFields.get(y).getInventory()==1){
                Button inventory = layout_view.findViewById(R.id.inventoryButton);
                if(inventory != null){
                    List<TableProduct> products = TableProduct.listAll(TableProduct.class);
                    if(products.isEmpty() || products.toString().equals("[]")){
                        inventory.setVisibility(View.INVISIBLE);
                    }
                }
            }else{
                Button inventory = layout_view.findViewById(R.id.inventoryButton);
                if(inventory != null) {

                    inventory.setVisibility(View.INVISIBLE);
                }
            }

            if(!(Integer.toString(formEdit).equals("-1"))){
                String[] whereArgs2 = new  String[4];
                whereArgs2[0] = String.valueOf(formEdit);
                whereArgs2[1] = String.valueOf(id_form0);
                whereArgs2[2] = String.valueOf(y);
                whereArgs2[3] = String.valueOf(version0);
                setLayoutText(layout_view,whereArgs2,formFields.get(y).getId_type());

            }
            views.add(layout_view);
            if (posicionview<pagerAdapter.getCount()){
                pagerAdapter.addView(layout_view,posicionview);
            }else{
                pagerAdapter.addView(layout_view);
                posicionview = pagerAdapter.getCount();
            }

            pagerAdapter.notifyDataSetChanged();
            posicionview++;
        }

        //Construye la actividad para guardarlo
        if(id_form==id_form0){
            Boolean network = haveNetworkConnection(ViewPagerActivity.this);

            LayoutInflater layoutInflater1 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater1.inflate(R.layout.activity_save_form, null);

            EditText nameText = (EditText) view.findViewById(R.id.nameForm);
            nameText.setText(nameForm0);

            if(network) {
            }else{
                CheckBox check = (CheckBox) view.findViewById(R.id.checkSendForm);
                check.setChecked(false);
                check.setEnabled(false);
            }

            Button saveButton = (Button) view.findViewById(R.id.btnsave);
            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastView = pagerAdapter.getView(pagerAdapter.getCount() - 1);
                    dialogpro = ProgressDialog.show(ViewPagerActivity.this, "", "Guardando. Por favor espere un momento...", true);
                    CheckBox check = (CheckBox) lastView.findViewById(R.id.checkBoxtosave);
                    if (check.isChecked()) {
                        Thread thread=new Thread(new Runnable(){
                            public void run(){
                                saveForm(true);
                                runOnUiThread(new Runnable(){
                                    @Override
                                    public void run() {
                                        if(dialogpro.isShowing())
                                            dialogpro.dismiss();
                                        //finish();
                                        ToolBox newtool = new ToolBox();
                                        newtool.sendNotification("Mensaje","Formulario Guardado y Finalizaro",getApplicationContext());
                                        //Toast.makeText(ViewPagerActivity.this,"Formulario Guardado y Finalizado!",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                        thread.start();
                    } else {
                        Thread thread=new Thread(new Runnable(){

                            public void run(){
                                saveForm(false);
                                runOnUiThread(new Runnable(){
                                    @Override
                                    public void run() {
                                        if(dialogpro.isShowing())
                                            dialogpro.dismiss();
                                        //finish();
                                        //toast.makeText(ViewPagerActivity.this,"Formulario Guardado!",Toast.LENGTH_LONG).show();
                                        ToolBox newtool = new ToolBox();
                                        newtool.sendNotification("Mensaje","Formulario Guardado",getApplicationContext());

                                    }
                                });
                            }

                        });
                        thread.start();
                    }
                }
            });

            pagerAdapter.addView(view, pagerAdapter.getCount());
            pagerAdapter.notifyDataSetChanged();

            views.add(view);
        }


        return true;
    }

    public View getLayoutView(int id_type, final int counter,final int id_form0, final int version0,final String nameForm0){
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;

        if(id_type == 1){

            view = inflater.inflate(R.layout.layout_text,null);

            EditText texto = (EditText) view.findViewById(R.id.editTextValue);
            final EditText texto2 = (EditText) view.findViewById(R.id.editTextValue);
            final TextView charCounter = (TextView) view.findViewById(R.id.characterCounter);

            texto.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String txt = texto2.getText().toString();
                    int symbols = txt.length();
                    charCounter.setText(""+Integer.toString(symbols));

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            text_list.add(texto);
            final int auxo;
            auxo = text_list.size() - 1;

            Button getInventory = (Button) view.findViewById(R.id.inventoryButton);
            getInventory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ViewPagerActivity.this, PickerProductActivity.class);
                    intent.putExtra("form",String.valueOf(id_form0));
                    intent.putExtra("pointer",String.valueOf(pointer));
                    intent.putExtra("posicion",String.valueOf(auxo));
                    intent.putExtra("type","text");
                    startActivityForResult(intent, 35795);
                }
            });

        }else if(id_type == 2){

            view = inflater.inflate(R.layout.layout_image,null);

            ImageView imgview = view.findViewById(R.id.imageViewValue);
            typesimagen.add(imgview);
            final int auxo;
            auxo = typesimagen.size() - 1;
            Button buttonuploadimage = view.findViewById(R.id.deleteButton);
            buttonuploadimage.setText("Seleccionar Imagen");
            buttonuploadimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), auxo);
                }
            });

            Button buttontakepicture = view.findViewById(R.id.button4);
            buttontakepicture.setText("Tomar Foto");
            buttontakepicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ContextCompat.checkSelfPermission(ViewPagerActivity.this, Manifest.permission.CAMERA)== PackageManager.PERMISSION_GRANTED){
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, auxo);
                    }else{
                        requestCameraPermission();
                    }
                }
            });

        }else if(id_type == 3){

            view = inflater.inflate(R.layout.layout_number,null);
            EditText num = (EditText) view.findViewById(R.id.editTextValue);
            number_list.add(num);
            final int auxo;
            auxo = number_list.size() - 1;
            Button getInventory = (Button) view.findViewById(R.id.inventoryButton);
            getInventory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ViewPagerActivity.this, PickerProductActivity.class);
                    intent.putExtra("form",String.valueOf(id_form0));
                    intent.putExtra("pointer",String.valueOf(pointer));
                    intent.putExtra("posicion",String.valueOf(String.valueOf(auxo)));
                    intent.putExtra("type","number");
                    startActivityForResult(intent, 35795);
                }
            });

        }else if(id_type == 4){

            String values = formFields.get(counter).getValue();
            String[] values_split = values.split(",");

            if(formFields.get(counter).getTypeOption().equals("select")){
                view = inflater.inflate(R.layout.layout_options_select,null);
                TextView option_tyoe = view.findViewById(R.id.tipodeopcionid);
                option_tyoe.setText("select");
                option_tyoe.setVisibility(View.INVISIBLE);
                RadioGroup group = view.findViewById(R.id.opcionesselect);
                RadioButton button;
                String selected = "";
                final int c = counter;
                if(formEdit!=-1){
                    String[] whereArgs = new  String[4];
                    whereArgs[0] = Integer.toString(formEdit);
                    whereArgs[1] = Integer.toString(id_form0);
                    whereArgs[2] = Integer.toString(counter);
                    whereArgs[3] = "select";
                    final List<OptionType> data = OptionType.find(OptionType.class, "idsavedform = ? and idform = ? and position = ? and type = ?", whereArgs);
                    if(!data.isEmpty()){
                        selected = data.get(0).getOptionType();
                        if(selected_list.size()<c){
                            for(int x =0;x<counter;x++){
                                selected_list.add("");
                            }
                        }
                        selected_list.add(c,selected);
                    }
                }

                for (int i = 0; i < values_split.length; i++) {
                    button = new RadioButton(this);
                    button.setText(values_split[i]);
                    if(selected.equals(values_split[i])){
                        button.setSelected(true);
                        button.setChecked(true);
                    }
                    group.addView(button);
                }
                group.setOrientation(RadioGroup.VERTICAL);

                group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if(hijostodosglobal.size()>0){
                            RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);

                            String valorradio = checkedRadioButton.getText().toString();
                            int count = pagerAdapter.getCount();
                            int currentitemis = pager.getCurrentItem();
                            List<String>formulariospasados=new ArrayList<>();

                            for (int itemdelete =0;itemdelete<=currentitemis;itemdelete++) {
                                View currv = pagerAdapter.getView(itemdelete);
                                TextView formis = (TextView) currv.findViewById(R.id.textidform);
                                if(!(formulariospasados.contains(formis.getText().toString()))){
                                    formulariospasados.add(formis.getText().toString());
                                }
                            }

                            for (int itemdelete = currentitemis + 1; itemdelete < count - 1; itemdelete++) {
                                View currv = pagerAdapter.getView(itemdelete);
                                TextView formis = (TextView) currv.findViewById(R.id.textidform);
                                String texto = formis.getText().toString();
                                if(!(formulariospasados.contains(texto))) {
                                    pagerAdapter.removeView(pager, currentitemis + 1);
                                    pagerAdapter.notifyDataSetChanged();
                                }
                            }

                            pager.setCurrentItem(currentitemis);

                            if (hijostodosglobal.size() > 0) {
                                String[] hasvaluesstrfpo = new String[3];
                                hasvaluesstrfpo[0] = String.valueOf(id_form0);
                                hasvaluesstrfpo[1] = String.valueOf(formFields.get(counter).getId_field());
                                hasvaluesstrfpo[2] = valorradio;
                                List<TableFormFieldsOptions> hasfparentopta = TableFormFieldsOptions.find(TableFormFieldsOptions.class, "idfieldparent = ? and idfield = ? and value = ? ", hasvaluesstrfpo, "", "", "");
                                //List<TableFormFieldsOptions> hasfparentopta = TableFormFieldsOptions.listAll(TableFormFieldsOptions.class);
                                if (hasfparentopta.size() > 0) {
                                    List<TableForms> formularioanexo = TableForms.find(TableForms.class, "idform = ?", String.valueOf(hasfparentopta.get(0).getId_fieldOption()));
                                    if(!formularioanexo.isEmpty()){
                                        for(int x=0; x<formularioanexo.size();x++){
                                            int idformanexo = formularioanexo.get(x).getId_form();
                                            int versionanexo = formularioanexo.get(x).getVersion();
                                            String nameanexo = formularioanexo.get(x).getName();

                                            buildForm(currentitemis + 1,idformanexo,versionanexo,nameanexo,"noinit");
                                        }

                                    }
                                }
                            }
                        }
                    }
                });

            }else{
                view = inflater.inflate(R.layout.layout_options_checkbox,null);
                TextView option_tyoe = view.findViewById(R.id.tipodeopcionid);
                option_tyoe.setText("check");
                option_tyoe.setVisibility(View.INVISIBLE);

                RadioGroup group = view.findViewById(R.id.opcionesselect);
                final int c = counter;
                for (int i = 0; i < values_split.length; i++) {
                    String selected = "";
                    if(formEdit!=-1){
                        String[] whereArgs = new  String[4];
                        whereArgs[0] = Integer.toString(formEdit);
                        whereArgs[1] = Integer.toString(id_form);
                        whereArgs[2] = Integer.toString(counter);
                        whereArgs[3] = "select";

                        final List<OptionType> data = OptionType.find(OptionType.class, "idsavedform = ? and idform = ? and position = ? and type != ?", whereArgs);
                        if(!data.isEmpty()){
                            selected = data.get(0).getOptionType();
                            if(checkbox_list.size()<=c){
                                for(int x=0;x<c+1;x++){
                                    checkbox_list.add("");
                                }
                            }
                            checkbox_list.add(c,selected);
                        }
                    }else{
                        if(checkbox_list.size()<c){
                            for(int x=0;x<c+1;x++){
                                checkbox_list.add("");
                            }
                        }
                    }

                    final CheckBox checkBox = new CheckBox(this);
                    checkBox.setText(values_split[i]);
                    if (selected.contains(values_split[i])) {
                        checkBox.setChecked(true);
                        if(hijostodosglobal.size()>0){
                            String valorradio = checkBox.getText().toString();
                            int count = pagerAdapter.getCount();
                            int currentitemis = pager.getCurrentItem();
                            List<String>formulariospasados=new ArrayList<>();

                            if(currentitemis!=0){
                                for (int itemdelete =0;itemdelete<=currentitemis;itemdelete++) {
                                    View currv = pagerAdapter.getView(itemdelete);
                                    TextView formis = (TextView) currv.findViewById(R.id.textidform);
                                    if(!(formulariospasados.contains(formis.getText().toString()))){
                                        formulariospasados.add(formis.getText().toString());
                                    }
                                }
                            }

                            for (int itemdelete = currentitemis + 1; itemdelete < count - 1; itemdelete++) {
                                View currv = pagerAdapter.getView(itemdelete);
                                TextView formis = (TextView) currv.findViewById(R.id.textidform);
                                String texto = formis.getText().toString();
                                if(!(formulariospasados.contains(texto))) {
                                    pagerAdapter.removeView(pager, currentitemis + 1);
                                    pagerAdapter.notifyDataSetChanged();
                                }
                            }

                            pager.setCurrentItem(currentitemis);

                            if (hijostodosglobal.size() > 0) {
                                String[] hasvaluesstrfpo = new String[3];
                                hasvaluesstrfpo[0] = String.valueOf(id_form0);
                                hasvaluesstrfpo[1] = String.valueOf(formFields.get(counter).getId_field());
                                hasvaluesstrfpo[2] = valorradio;
                                List<TableFormFieldsOptions> hasfparentopta = TableFormFieldsOptions.find(TableFormFieldsOptions.class, "idfieldparent = ? and idfield = ? and value = ? ", hasvaluesstrfpo, "", "", "");
                                //List<TableFormFieldsOptions> hasfparentopta = TableFormFieldsOptions.listAll(TableFormFieldsOptions.class);
                                if (hasfparentopta.size() > 0) {
                                    List<TableForms> formularioanexo = TableForms.find(TableForms.class, "idform = ?", String.valueOf(hasfparentopta.get(0).getId_fieldOption()));
                                    if(!formularioanexo.isEmpty()){
                                        for(int x=0; x<formularioanexo.size();x++){
                                            int idformanexo = formularioanexo.get(x).getId_form();
                                            int versionanexo = formularioanexo.get(x).getVersion();
                                            String nameanexo = formularioanexo.get(x).getName();

                                            buildForm(currentitemis + 1,idformanexo,versionanexo,nameanexo,"noinit");
                                        }

                                    }
                                }
                            }

                        }
                    }

                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            String ck = "";
                            if(isChecked){
                                if(checkbox_list.size()<=c){
                                    for(int x=0;x<c+1;x++){
                                        checkbox_list.add("");
                                    }
                                }
                                ck = checkbox_list.get(c);
                                if(ck.equals("")){
                                    ck = checkBox.getText().toString();
                                }else{
                                    ck += ","+checkBox.getText().toString();
                                }

                                if(hijostodosglobal.size()>0){
                                    String valorradio = checkBox.getText().toString();
                                    int count = pagerAdapter.getCount();
                                    int currentitemis = pager.getCurrentItem();
                                    List<String>formulariospasados=new ArrayList<>();
                                    for (int itemdelete =0;itemdelete<=currentitemis;itemdelete++) {
                                        View currv = pagerAdapter.getView(itemdelete);
                                        TextView formis = (TextView) currv.findViewById(R.id.textidform);
                                        if(!(formulariospasados.contains(formis.getText().toString()))){
                                            formulariospasados.add(formis.getText().toString());
                                        }
                                    }

                                    for (int itemdelete = currentitemis + 1; itemdelete < count - 1; itemdelete++) {
                                        View currv = pagerAdapter.getView(itemdelete);
                                        TextView formis = (TextView) currv.findViewById(R.id.textidform);
                                        String texto = formis.getText().toString();
                                        if(!(formulariospasados.contains(texto))) {
                                            pagerAdapter.removeView(pager, currentitemis + 1);
                                            pagerAdapter.notifyDataSetChanged();
                                        }
                                    }

                                    pager.setCurrentItem(currentitemis);

                                    if (hijostodosglobal.size() > 0) {
                                        String[] hasvaluesstrfpo = new String[3];
                                        hasvaluesstrfpo[0] = String.valueOf(id_form0);
                                        hasvaluesstrfpo[1] = String.valueOf(formFields.get(counter).getId_field());
                                        hasvaluesstrfpo[2] = valorradio;
                                        List<TableFormFieldsOptions> hasfparentopta = TableFormFieldsOptions.find(TableFormFieldsOptions.class, "idfieldparent = ? and idfield = ? and value = ? ", hasvaluesstrfpo, "", "", "");
                                        //List<TableFormFieldsOptions> hasfparentopta = TableFormFieldsOptions.listAll(TableFormFieldsOptions.class);
                                        if (hasfparentopta.size() > 0) {
                                            List<TableForms> formularioanexo = TableForms.find(TableForms.class, "idform = ?", String.valueOf(hasfparentopta.get(0).getId_fieldOption()));
                                            if(!formularioanexo.isEmpty()){
                                                for(int x=0; x<formularioanexo.size();x++){
                                                    int idformanexo = formularioanexo.get(x).getId_form();
                                                    int versionanexo = formularioanexo.get(x).getVersion();
                                                    String nameanexo = formularioanexo.get(x).getName();

                                                    buildForm(currentitemis + 1,idformanexo,versionanexo,nameanexo,"noinit");
                                                }

                                            }
                                        }
                                    }

                                }
                            }else{
                                ck = checkbox_list.get(c);
                                if(!ck.equals("")){
                                    ck = ck.replace(","+checkBox.getText().toString(),"");
                                    ck = ck.replace(checkBox.getText().toString()+",","");
                                    ck = ck.replace(checkBox.getText().toString(),"");
                                }
                            }
                            checkbox_list.add(c,ck);
                        }
                    });
                    group.addView(checkBox);

                }
                group.setOrientation(RadioGroup.VERTICAL);

            }

        }else if(id_type == 5){

            view = inflater.inflate(R.layout.layout_date,null);

            final EditText dateText = view.findViewById(R.id.editTextValue);
            dateText.setFocusable(false);
            dateText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vv) {
                    datepickerdialog = new DatePickerDialog(ViewPagerActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            String m = "";
                            String d = "";

                            if((month+1)<10){
                                m = "0"+(month + 1);
                            }else{
                                m = ""+(month + 1);
                            }
                            if(dayOfMonth<10){
                                d = "0"+dayOfMonth;
                            }else{
                                d = ""+dayOfMonth;
                            }

                            dateText.setText(d + "/" + m + "/" + year);

                        }
                    }, year, month, day);
                    datepickerdialog.show();
                }
            });

        }else if(id_type == 6){

            view = inflater.inflate(R.layout.layout_date_time,null);

            final EditText dateText = view.findViewById(R.id.editTextValue);
            dateText.setFocusable(false);
            dateText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vv) {
                    datepickerdialog = new DatePickerDialog(ViewPagerActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            String m = "";
                            String d = "";

                            if((month+1)<10){
                                m = "0"+(month + 1);
                            }else{
                                m = ""+(month + 1);
                            }
                            if(dayOfMonth<10){
                                d = "0"+dayOfMonth;
                            }else{
                                d = ""+dayOfMonth;
                            }

                            dateText.setText(d + "/" + m + "/" + year);
                        }
                    }, year, month, day);
                    datepickerdialog.show();
                }
            });

            final EditText hourText = view.findViewById(R.id.editTextValue2);
            hourText.setFocusable(false);
            hourText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar c = Calendar.getInstance();
                    int hour = c.get(Calendar.HOUR_OF_DAY);
                    int minute = c.get(Calendar.MINUTE);
                    TimePickerDialog timePicker = new TimePickerDialog(ViewPagerActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            String h = "";
                            String m = "";

                            if(hourOfDay<10){
                                h = "0"+hourOfDay;
                            }else{
                                h = ""+hourOfDay;
                            }
                            if(minute<10){
                                m = "0"+minute;
                            }else{
                                m = ""+minute;
                            }

                            hourText.setText(h + ":" + m);

                        }
                    }, hour, minute, DateFormat.is24HourFormat(ViewPagerActivity.this));
                    timePicker.show();
                }
            });

        }else if(id_type == 7){
            view = inflater.inflate(R.layout.layout_sign,null);

            ImageView signature = view.findViewById(R.id.imageViewValue);
            signs.add(signature);

            final int positionSign = signs.size()-1;

            Button addSign = view.findViewById(R.id.button6);
            final View viewfirmap = inflater.inflate(R.layout.utility_paintview, null);
            final PaintView paintv = viewfirmap.findViewById(R.id.paintViewid);

            addSign.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplayMetrics metrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(metrics);
                    paintv.init(metrics);

                    Intent elintent=new Intent(getApplicationContext(), DrawActivity.class);
                    elintent.putExtra("positionSign",String.valueOf(positionSign));
                    startActivityForResult(elintent,999);
                }
            });
        }else if(id_type == 8){
            view = inflater.inflate(R.layout.layout_location,null);
            Button getLocation = (Button) view.findViewById(R.id.button5);

            EditText latitudText = (EditText) view.findViewById(R.id.editTextValue);
            latitudText.setFocusable(false);
            latitud.add(latitudText);

            EditText longitudText = (EditText) view.findViewById(R.id.editTextValue2);
            longitudText.setFocusable(false);
            longitud.add(longitudText);

            EditText altitudText = (EditText) view.findViewById(R.id.editTextValue3);
            altitudText.setFocusable(false);
            altitud.add(altitudText);

            final int aux = latitud.size() - 1;
            getLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getUserlocation(aux);
                }
            });

        }else if(id_type == 9){
            view = inflater.inflate(R.layout.layout_qr,null);
            EditText qrvf = (EditText) view.findViewById(R.id.editTextValue);
            qrvf.setEnabled(false);
            qr_code.add(qrvf);
            final int auxo;
            auxo = qr_code.size() - 1;
            Button getQr = (Button) view.findViewById(R.id.buttonqrope);
            getQr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ViewPagerActivity.this, QrActivity.class);
                    intent.putExtra("form",String.valueOf(id_form0));
                    intent.putExtra("pointer",String.valueOf(pointer));
                    intent.putExtra("posicionqr",String.valueOf(auxo));
                    startActivityForResult(intent, 7052);
                }
            });
        }else{
            view = inflater.inflate(R.layout.layout_text,null);
        }
        return(view);
    }

    public void setLayoutText(View view, String[] where, int id_type){

        EditText layout_fieldValue = view.findViewById(R.id.editTextValue);
        layout_fieldValue2 = view.findViewById(R.id.editTextValue2);
        EditText layout_fieldValue3 = view.findViewById(R.id.editTextValue3);

        ImageView layout_fieldImage = view.findViewById(R.id.imageViewValue);
        RadioGroup layout_fieldSelect = view.findViewById(R.id.opcionesselect);
        TextView layout_fieldoption = view.findViewById(R.id.tipodeopcionid);

        if(id_type == 1){
            List<TextType> data = TextType.find(TextType.class, "idsavedform = ? and idform = ? and position = ? and version = ? ", where);
            if(!data.isEmpty()){
                layout_fieldValue.setText(data.get(0).getTextType().toString(),TextView.BufferType.EDITABLE);
            }
        }if(id_type == 2){
            List<ImageType> data = ImageType.find(ImageType.class, "idsavedform = ? and idform = ? and position = ? and version = ?", where);
            if(!data.isEmpty()){
                try{
                    String d = data.get(0).getImageType();
                    if(d != null){
                        byte [] encodeByte = Base64.decode(d,Base64.DEFAULT);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte,0,encodeByte.length);
                        layout_fieldImage.setImageBitmap(bitmap);
                    }
                }catch (Error e){
                }
            }
        }else if(id_type == 3){
            List<NumberType> data = NumberType.find(NumberType.class, "idsavedform = ? and idform = ? and position = ? and version = ?", where);
            if(!data.isEmpty()){
                int num = data.get(0).getNumberType();
                layout_fieldValue.setText(Integer.toString(num));
            }
        }else if(id_type == 5){
            List<DateType> data = DateType.find(DateType.class, "idsavedform = ? and idform = ? and position = ? and version = ?", where);
            if(!data.isEmpty()){
                String d = data.get(0).getDateType();
                layout_fieldValue.setText(d);
            }

        }else if(id_type == 6){
            List<DateTimeType> data = DateTimeType.find(DateTimeType.class, "idsavedform = ? and idform = ? and position = ? and version = ?", where);
            if(!data.isEmpty()){
                String d = data.get(0).getDateTimeType().split(" ")[0];
                String t = data.get(0).getDateTimeType().split(" ")[1];
                layout_fieldValue.setText(d);
                layout_fieldValue2.setText(t);
            }
        }else if(id_type == 7){
            List<SignType> data = SignType.find(SignType.class, "idsavedform = ? and idform = ? and position = ? and version = ?", where);
            if(!data.isEmpty()){
                try{
                    String d = data.get(0).getSignType();
                    if(d != null){
                        byte [] encodeByte = Base64.decode(d,Base64.DEFAULT);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte,0,encodeByte.length);
                        layout_fieldImage.setImageBitmap(bitmap);
                    }
                }catch (Error e){
                }
            }
        }else if(id_type == 8){
            List<LocationType> data = LocationType.find(LocationType.class, "idsavedform = ? and idform = ? and position = ? and version = ?", where);
            if(!data.isEmpty()){
                String uno = data.get(0).getLatitude();
                String dos = data.get(0).getLongitude();
                String tres = data.get(0).getAltitude();
                layout_fieldValue.setText(uno);
                layout_fieldValue2.setText(dos);
                layout_fieldValue3.setText(tres);
            }

        }else if(id_type == 9){
            List<QrType> data = QrType.find(QrType.class, "idsavedform = ? and idform = ? and position = ? and version = ?", where);
            if(!data.isEmpty()){
                String qr = data.get(0).getQrType();
                layout_fieldValue.setText(qr);
            }
        }
    }

    private String optionsBuilder(List<String> allSons){ //funcionrecursiva
        String optionList = "";
        List<String> allSonsList=new ArrayList<>();
        for(int auxv=0;auxv<allSons.size();auxv++){

            String[] whereArgs = new String[2];
            whereArgs[0]=String.valueOf(allSons.get(auxv));
            whereArgs[1]=String.valueOf(version);

            List<TableFormFieldsOptions> fparentopta= TableFormFieldsOptions.find(TableFormFieldsOptions.class,"idfieldparent = ? and vesion = ?",whereArgs);
            if(!fparentopta.isEmpty()) {
                for (int y = 0; y < fparentopta.size(); y++) {
                    optionList += fparentopta.get(y).getId_fieldOption()+",";
                    allSonsList.add(String.valueOf(fparentopta.get(y).getId_fieldOption()));
                }
                optionList=optionList+optionsBuilder(allSonsList);
            }
        }
        return optionList;
    }

    public void getLocation(){
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if(ContextCompat.checkSelfPermission(ViewPagerActivity.this,Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){
            final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
            if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            }else{
                Location location = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);
                Boolean OnlySatelliteGPS = false;
                SingleShotLocationProvider.requestSingleUpdate(OnlySatelliteGPS, false, ViewPagerActivity.this,
                        new SingleShotLocationProvider.LocationCallback() {
                            @Override
                            public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                                SharedPreferences prefs = ViewPagerActivity.this.getSharedPreferences("MyPrefsFile", ViewPagerActivity.this.MODE_PRIVATE);
                                String debug = prefs.getString("sp_debug", "");
                                if (location.latitude <= 0 && location.longitude <= 0) {
                                    if (debug.equals("YES")) {
                                    }
                                } else {
                                    if (debug.equals("YES")) {
                                    }
                                    double longitude = location.longitude;
                                    double latitude = location.latitude;
                                    double altitude = location.accuracy;
                                    lat = String.valueOf(latitude);
                                    lon = String.valueOf(longitude);
                                    alt = String.valueOf(altitude);
                                }
                            }
                        });
            }
        }else{
            requestStoragePermission();
        }
    }

    // FUNCION PARA GUARDAR
    public void saveForm(Boolean done){

        View lastview = pagerAdapter.getView(pagerAdapter.getCount() - 1);
        List<TableSavedForm> savedForms = TableSavedForm.listAll(TableSavedForm.class);
        int last_id = -1;
        if (!savedForms.isEmpty()) {
            int id = savedForms.get(savedForms.size() - 1).getId_savedform();
            last_id = id;
        }

        int id_savedform = last_id + 1;

        if (!(Integer.toString(formEdit).equals("-1"))) {
            id_savedform = formEdit;
            DateTimeType.deleteAll(DateTimeType.class, "idsavedform = ?", Integer.toString(formEdit));
            DateType.deleteAll(DateType.class, "idsavedform = ?", Integer.toString(formEdit));
            ImageType.deleteAll(ImageType.class, "idsavedform = ?", Integer.toString(formEdit));
            LocationType.deleteAll(LocationType.class, "idsavedform = ?", Integer.toString(formEdit));
            NumberType.deleteAll(NumberType.class, "idsavedform = ?", Integer.toString(formEdit));
            OptionType.deleteAll(OptionType.class, "idsavedform = ?", Integer.toString(formEdit));
            QrType.deleteAll(QrType.class, "idsavedform = ?", Integer.toString(formEdit));
            SignType.deleteAll(SignType.class, "idsavedform = ?", Integer.toString(formEdit));
            TextType.deleteAll(TextType.class, "idsavedform = ?", Integer.toString(formEdit));
        }
        idForm = id_savedform;
        EditText nameForm = lastview.findViewById(R.id.nameForm);
        List<TableUser> users = TableUser.listAll(TableUser.class);
        int application_id = users.get(0).getApplication_id();
        int internal_id = users.get(0).getInternal_id();
        String user = users.get(0).getUser();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat today = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String created = today.format(calendar.getTime());
        for (int counter = 0; counter < pagerAdapter.getCount() - 1; counter++) {
            View layout_counter = pagerAdapter.getView(counter);
            TextView layout_fieldname = (TextView) layout_counter.findViewById(R.id.field_name);
            TextView layout_fieldtype = (TextView) layout_counter.findViewById(R.id.texttipoid);
            TextView layout_fieldform = (TextView) layout_counter.findViewById(R.id.textidform);
            int original_form = Integer.parseInt(layout_fieldform.getText().toString());
            TextView layout_fieldversion = layout_counter.findViewById(R.id.textversionform);

            EditText layout_fieldValue = layout_counter.findViewById(R.id.editTextValue);
            layout_fieldValue2 = layout_counter.findViewById(R.id.editTextValue2);
            EditText layout_fieldValue3 = layout_counter.findViewById(R.id.editTextValue3);

            ImageView layout_fieldImage = layout_counter.findViewById(R.id.imageViewValue);
            RadioGroup layout_fieldSelect = layout_counter.findViewById(R.id.opcionesselect);
            TextView layout_fieldoption = layout_counter.findViewById(R.id.tipodeopcionid);

            String[] whereArgs = new String[4];
            whereArgs[0] = layout_fieldtype.getText().toString();
            whereArgs[1] = layout_fieldname.getText().toString();
            whereArgs[2] = layout_fieldversion.getText().toString();
            whereArgs[3] = layout_fieldform.getText().toString();

            List<TableFormFields> fieldData = TableFormFields.find(TableFormFields.class, "idtype = ? and fieldname = ? and version = ? and idform = ?", whereArgs);
            int idfield = fieldData.get(0).getId_field();

            if (layout_fieldtype.getText().toString().equals("1")) { // Texto
                List<TextType> allTexts = TextType.listAll(TextType.class);
                int id_texttype = 0;
                if (!allTexts.isEmpty()) {
                    id_texttype = allTexts.get(allTexts.size() - 1).getId_textType() + 1;
                }
                TextType newtext = new TextType(id_texttype, id_form, application_id, internal_id, version, id_savedform, counter, idfield, original_form, layout_fieldValue.getText().toString(), created);
                newtext.save();
            } else if (layout_fieldtype.getText().toString().equals("2")) { // Imagen

                List<ImageType> allImages = ImageType.listAll(ImageType.class);
                int id_imagetype = 0;
                if (!allImages.isEmpty()) {
                    id_imagetype = allImages.get(allImages.size() - 1).getId_imageType() + 1;
                }

                String imagetype = null;
                if (layout_fieldImage != null) { // Si exite una imagen seleccionada
                    try {  //Proceso para convertirla a String
                        byte[] byteArray;
                        layout_fieldImage.buildDrawingCache();
                        Bitmap bitmap = layout_fieldImage.getDrawingCache();
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                        byteArray = byteArrayOutputStream.toByteArray();
                        imagetype = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    } catch (Exception e) {
                    }
                }

                ImageType newimage = new ImageType(id_imagetype, id_form, application_id, internal_id, version, id_savedform, counter, idfield, original_form, imagetype, created);
                newimage.save();

            } else if (layout_fieldtype.getText().toString().equals("3") && !(layout_fieldValue.getText().toString().equals(""))) { // Number

                List<NumberType> allNumbers = NumberType.listAll(NumberType.class);
                int id_numbertype = 0;
                if (!allNumbers.isEmpty()) {
                    id_numbertype = allNumbers.get(allNumbers.size() - 1).getId_numberType() + 1;
                }
                int number = Integer.parseInt(layout_fieldValue.getText().toString());

                NumberType newnumber = new NumberType(id_numbertype, id_form, application_id, internal_id, version, id_savedform, counter, idfield, original_form, number, created);
                newnumber.save();

            } else if (layout_fieldtype.getText().toString().equals("4")) { // Select or Checkbox

                String selectedtext = "";
                List<OptionType> allOptions = OptionType.listAll(OptionType.class);
                int id_optiontype = 0;
                if (!allOptions.isEmpty()) {
                    id_optiontype = allOptions.get(allOptions.size() - 1).getId_optionType() + 1;
                }

                if (layout_fieldoption.getText() == "select") {
                    int selectedRadioButtonID = layout_fieldSelect.getCheckedRadioButtonId();
                    View radioButton = layout_fieldSelect.findViewById(selectedRadioButtonID);
                    int idx = layout_fieldSelect.indexOfChild(radioButton);
                    RadioButton r = (RadioButton) layout_fieldSelect.getChildAt(idx);
                    if (r != null) {
                        selectedtext = r.getText().toString();
                    } else {
                        if (formEdit != -1) {
                            selectedtext = selected_list.get(counter);
                        }
                    }

                    OptionType newoption = new OptionType(id_optiontype, id_form, application_id, internal_id, version, id_savedform, counter, idfield, original_form, selectedtext, created, "select");
                    newoption.save();
                } else {
                    selectedtext = checkbox_list.get(counter);
                    OptionType newoption = new OptionType(id_optiontype, id_form, application_id, internal_id, version, id_savedform, counter, idfield, original_form, selectedtext, created, "check");
                    newoption.save();
                }

            } else if (layout_fieldtype.getText().toString().equals("5") && !(layout_fieldValue.getText().toString().equals(""))) { //Date

                List<DateType> allDates = DateType.listAll(DateType.class);
                int id_datetype = 0;
                if (!allDates.isEmpty()) {
                    id_datetype = allDates.get(allDates.size() - 1).getId_dateType() + 1;
                }
                DateType newdate = new DateType(id_datetype, id_form, application_id, internal_id, version, id_savedform, counter, idfield, original_form, layout_fieldValue.getText().toString(), created);
                newdate.save();

            } else if (layout_fieldtype.getText().toString().equals("6") && !(layout_fieldValue.getText().toString().equals(""))) { //DateTime

                List<DateTimeType> allDateTimes = DateTimeType.listAll(DateTimeType.class);
                int id_datetimetype = 0;
                if (!allDateTimes.isEmpty()) {
                    id_datetimetype = allDateTimes.get(allDateTimes.size() - 1).getId_datetimetype() + 1;
                }
                String datetime = layout_fieldValue.getText().toString() + " " + layout_fieldValue2.getText().toString();
                DateTimeType newdate = new DateTimeType(id_datetimetype, id_form, application_id, internal_id, version, id_savedform, counter, idfield, original_form, datetime, created);
                newdate.save();
            } else if (layout_fieldtype.getText().toString().equals("7")) { //Firma

                List<SignType> allSigns = SignType.listAll(SignType.class);
                int id_signtype = 0;
                if (!allSigns.isEmpty()) {
                    id_signtype = allSigns.get(allSigns.size() - 1).getId_signType() + 1;
                }
                String signtype = null;
                if (layout_fieldImage != null) { // Si exite una imagen seleccionada
                    try {  //Proceso para convertirla a String
                        byte[] byteArray;
                        layout_fieldImage.buildDrawingCache();
                        Bitmap bitmap = layout_fieldImage.getDrawingCache();
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                        byteArray = byteArrayOutputStream.toByteArray();
                        signtype = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    } catch (Exception e) {
                    }
                }

                SignType newsign = new SignType(id_signtype, id_form, application_id, internal_id, version, id_savedform, counter, idfield, original_form, signtype, created);
                newsign.save();

            } else if (layout_fieldtype.getText().toString().equals("8")) { //Location
                List<LocationType> allLocations = LocationType.listAll(LocationType.class);
                int id_locationtype = 0;
                if (!allLocations.isEmpty()) {
                    id_locationtype = allLocations.get(allLocations.size() - 1).getId_locationType() + 1;
                }

                LocationType newlocation = new LocationType(id_locationtype, id_form, application_id, internal_id, version, id_savedform, counter, idfield, original_form, layout_fieldValue.getText().toString(), layout_fieldValue2.getText().toString(), layout_fieldValue3.getText().toString(), "0", created);
                newlocation.save();

            } else if (layout_fieldtype.getText().toString().equals("9")) { //QR

                List<QrType> allqrs = QrType.listAll(QrType.class);
                int id_qrtype = 0;
                if (!allqrs.isEmpty()) {
                    id_qrtype = allqrs.get(allqrs.size() - 1).getId_qrType() + 1;
                }

                QrType newqr = new QrType(id_qrtype, id_form, application_id, internal_id, version, id_savedform, counter, idfield, original_form, layout_fieldValue.getText().toString(), created);
                newqr.save();
            }
        }
        if (formEdit == -1) {
            TableSavedForm savedForm = new TableSavedForm(id_savedform, id_form, version, nameForm.getText().toString(), user, created, false, done, lat, lon, alt,jobID);
            savedForm.save();
        } else {
            String data = Integer.toString(formEdit);
            List<TableSavedForm> savedForm = TableSavedForm.find(TableSavedForm.class, "idsavedform = ?", data);
            if (!savedForm.isEmpty()) {
                savedForm.get(0).setName_form(nameForm.getText().toString());
                savedForm.get(0).setCreated(created);
                savedForm.get(0).setFinished(done);
                savedForm.get(0).save();
            }
        }
        if(lastView != null){
            CheckBox check = (CheckBox) lastView.findViewById(R.id.checkSendForm);
            if (check.isChecked()) {
                sendForm();
            }
            finish();
        }else{
            finish();
        }
    }

    // FUNCIONES PARA LLENAR EL FORMULARMIO
    public void getUserlocation(final int numelement){
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if(ContextCompat.checkSelfPermission(ViewPagerActivity.this,Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){
            final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
            if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                ToolBox newtool = new ToolBox();
                newtool.sendNotification("Error","GPS Deshabilitado!",getApplicationContext());

                //Toast.makeText(ViewPagerActivity.this,"GPS Deshabilitado!",Toast.LENGTH_SHORT).show();
            }else{
                //Toast.makeText(ViewPagerActivity.this,"Ubicacion Habilitada!",Toast.LENGTH_SHORT).show();
                ToolBox newtool = new ToolBox();
                newtool.sendNotification("Mensaje:","Ubicacion Habilitada!",getApplicationContext());
                Location location = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);
                Boolean OnlySatelliteGPS = false;
                SingleShotLocationProvider.requestSingleUpdate(OnlySatelliteGPS, false, ViewPagerActivity.this,
                        new SingleShotLocationProvider.LocationCallback() {
                            @Override
                            public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                                SharedPreferences prefs = ViewPagerActivity.this.getSharedPreferences("MyPrefsFile", ViewPagerActivity.this.MODE_PRIVATE);
                                String debug = prefs.getString("sp_debug", "");
                                if (location.latitude <= 0 && location.longitude <= 0) {
                                    if (debug.equals("YES")) {
                                    }
                                } else {
                                    if (debug.equals("YES")) {
                                    }
                                    double longitude = location.longitude;
                                    double latitude = location.latitude;
                                    double altitude = location.accuracy;
                                    latitud.get(numelement).setText(String.valueOf(latitude));
                                    longitud.get(numelement).setText(String.valueOf(longitude));
                                    altitud.get(numelement).setText(String.valueOf(altitude));
                                }
                            }
                        });
            }
        }else{
            requestStoragePermission();
        }
    }

    // FUNCIONES PARA PEDIR PERMISOS DE CAMARA Y UBICACION
    private void requestCameraPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CAMERA)){
            new AlertDialog.Builder(this)
                    .setTitle("Permiso Necesario")
                    .setMessage("Permiso para acceder a la camara")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(ViewPagerActivity.this,new String[]{Manifest.permission.CAMERA},2);

                        }
                    }).setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();

        }else{
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},2);
        }
    }

    private void requestStoragePermission() { //requestStoragePermission
        if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION)){
            new AlertDialog.Builder(this)
                    .setTitle("Permiso Necesario")
                    .setMessage("Guardar Información en su celular")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(ViewPagerActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
                        }
                    }).setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();

        }else{
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
        }
    }

    // FUNCIONES HEREDADAS

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.saving) {
            final ProgressDialog dialogpro = ProgressDialog.show(ViewPagerActivity.this, "", "Guardando. Por favor espere un momento...", true);

            Thread thread=new Thread(new Runnable(){

                public void run(){
                    saveForm(false);
                    runOnUiThread(new Runnable(){
                        @Override
                        public void run() {
                            if(dialogpro.isShowing())
                                dialogpro.dismiss();
                            //finish();
                            ToolBox newtool = new ToolBox();
                            newtool.sendNotification("Mensaje:","Formulario Guardado",getApplicationContext());
                            //Toast.makeText(ViewPagerActivity.this,"Formulario Guardado!",Toast.LENGTH_LONG).show();
                        }

                    });
                }

            });
            thread.start();
        }
        if (id == R.id.goback) {
            AlertDialog dialog=new AlertDialog.Builder(this)
                    .setTitle("Atención")
                    .setMessage("¿Deseas Guardar el formulario? de lo contrario perderás tus avances")
                    .setIcon(R.drawable.alert)
                    .setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("No Guardar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNeutralButton("Guardar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialogpro = ProgressDialog.show(ViewPagerActivity.this, "", "Guardando. Por favor espere un momento...", true);

                            Thread thread=new Thread(new Runnable(){

                                public void run(){
                                    saveForm(false);
                                    runOnUiThread(new Runnable(){
                                        @Override
                                        public void run() {
                                            if(dialogpro.isShowing())
                                                dialogpro.dismiss();
                                            //finish();
                                            ToolBox newtool = new ToolBox();
                                            newtool.sendNotification("Mensaje","Formulario Guardado",getApplicationContext());
                                            //Toast.makeText(ViewPagerActivity.this,"Formulario Guardado!",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }

                            });
                            thread.start();

                        }
                    })
                    .setCancelable(false)
                    .create();
            dialog.show();
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == 999) {
                if (resultCode != 12345) {
                    byte[] bytes = data.getByteArrayExtra("firma");
                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    int laposicion = Integer.parseInt(data.getStringExtra("laposicion"));
                    signs.get(laposicion).setImageBitmap(Bitmap.createScaledBitmap(bmp, signs.get(laposicion).getWidth(),signs.get(laposicion).getHeight(), false));
                }
            } else if (requestCode == 7052) {
                String mensaje = "";
                if (data.getStringExtra("qrlector_display") != null) {
                    mensaje = data.getStringExtra("qrlector_display");
                }
                int laposicion = Integer.parseInt(data.getStringExtra("laposicion"));
                qr_code.get(laposicion).setText(mensaje);
            } else if(requestCode==35795){
                String dato = "";
                int dat = 0;
                int laposicion = Integer.parseInt(data.getStringExtra("position"));
                String type = data.getStringExtra("type");
                if(type.equals("text")){
                    if (data.getStringExtra("text") != null) {
                        dato = data.getStringExtra("text");
                    }
                    text_list.get(laposicion).setText(dato);
                }else{
                    if (data.getStringExtra("number") != null) {
                        dato = data.getStringExtra("number");
                    }
                    number_list.get(laposicion).setText(data.getStringExtra("number"));
                }
            }else if (resultCode != 0){
                try{
                    Bitmap bitmap =(Bitmap)data.getExtras().get("data");
                    typesimagen.get(requestCode).setImageBitmap(bitmap);
                }catch (Exception e){
                    Uri selectedImage = data.getData();
                    typesimagen.get(requestCode).setImageURI(selectedImage);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @androidx.annotation.NonNull String[] permissions, int [] grantResults){
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        if(requestCode==1){
            if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                // Toast.makeText(this,"Granted",Toast.LENGTH_SHORT).show();

            }
            //Toast.makeText(this,"Denied",Toast.LENGTH_SHORT).show();
        }

        if(requestCode==2){
            if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                // Toast.makeText(this,"Granted",Toast.LENGTH_SHORT).show();

            }
            //Toast.makeText(this,"Denied",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override public void onBackPressed() {
        AlertDialog dialog=new AlertDialog.Builder(this)
                .setTitle("Atención")
                .setMessage("¿Deseas Guardar el formulario? de lo contrario perderás tus avances")
                .setIcon(R.drawable.alert)
                .setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No Guardar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();

                    }
                })
                .setNeutralButton("Guardar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialogpro = ProgressDialog.show(ViewPagerActivity.this, "", "Guardando. Por favor espere un momento...", true);

                        Thread thread=new Thread(new Runnable(){

                            public void run(){
                                saveForm(false);
                                runOnUiThread(new Runnable(){
                                    @Override
                                    public void run() {
                                        if(dialogpro.isShowing())
                                            dialogpro.dismiss();
                                        finish();
                                        ToolBox newtool = new ToolBox();
                                        newtool.sendNotification("Mensaje","Formulario Guardado",getApplicationContext());
                                        //Toast.makeText(ViewPagerActivity.this,"Formulario Guardado!",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }

                        });
                        thread.start();

                    }
                })
                .setCancelable(false)
                .create();
        dialog.show();
    }

    public void sendForm(){
        Log.d("INFO","Send Form");
        sendSavedForms = TableSavedForm.find(TableSavedForm.class, "idsavedform = ?", Integer.toString(idForm));

        sendUserSession = TableUser.listAll(TableUser.class);
        sendSession = sendUserSession.get(0).getSession();
        counter = 0;
        fw=fx=fn=fo=fd=ft=fl=fq=fi=fs=0;
        sendUrlsesion = url_mars + "/form/addsavedform?appID="+sendUserSession.get(0).getApplication_id();
        sendUrlsesiontexto = url_mars + "/form/addtexto?appID="+sendUserSession.get(0).getApplication_id();
        sendUrlsesionimagen = url_mars + "/form/addimagen?appID="+sendUserSession.get(0).getApplication_id();
        sendUrlsesionnumero = url_mars + "/form/addnumero?appID="+sendUserSession.get(0).getApplication_id();
        sendUrlsesionopcion = url_mars + "/form/addopcion?appID="+sendUserSession.get(0).getApplication_id();
        sendUrlsesionfecha = url_mars + "/form/addfecha?appID="+sendUserSession.get(0).getApplication_id();
        sendUrlsesionhorafecha = url_mars + "/form/addhorafecha?appID="+sendUserSession.get(0).getApplication_id();
        sendUrlsesionfirma = url_mars + "/form/addfirma?appID="+sendUserSession.get(0).getApplication_id();
        sendUrlsesionubicacion = url_mars + "/form/addubicacion?appID="+sendUserSession.get(0).getApplication_id();
        sendUrlsesionqr = url_mars + "/form/addqr?appID="+sendUserSession.get(0).getApplication_id();
        sendUrlsesionfinal = url_mars + "/form/finalanswer?appID="+sendUserSession.get(0).getApplication_id();
        String postData = "nombre_form="+sendSavedForms.get(0).getName_form()+"&fechacreacion="+sendSavedForms.get(0).getCreated()+"&id_form="+Integer.toString(sendSavedForms.get(0).getId_form())+"&version="+Integer.toString(sendSavedForms.get(0).getVersion())+
                "&item="+Integer.toString(0)+"&user="+sendSavedForms.get(0).getUser()+"&appid="+Integer.toString(sendUserSession.get(0).getApplication_id())+"&latitude="+sendSavedForms.get(0).getLatitude()+"&longitud="+sendSavedForms.get(0).getLongitude()+
                "&altitud="+sendSavedForms.get(0).getAltitude()+"&internalid="+sendUserSession.get(0).getInternal_id()+"&terminado="+Boolean.toString(sendSavedForms.get(0).getFinished())+"&jobID="+jobID;
        Log.d("INFO","PRIMER WEBService");
        Log.d("INFO","PRIMER WEBService");
        Log.d("INFO","PRIMER WEBService");
        Log.d("INFO","PRIMER WEBService");
        Log.d("INFO","PRIMER WEBService");
        Log.d("INFO","PRIMER WEBService");

        HttpPost webService = new HttpPost(sendUrlsesion, "POST", postData, sendSession, getApplicationContext(), new EventListener<JSONObject>() {
            @Override
            public void onSuccess(JSONObject response) {
                if(response.has("error")){
                    fw=1;
                    Log.i("lkyo","DONE addsavedform");
                }
            }
            @Override
            public void onFailure(Exception e) {
                fw=2;
                ToolBox newtool = new ToolBox();
                newtool.sendNotification("Error",e.toString(),getApplicationContext());
                //Toast.makeText(ViewPagerActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
            }
        });
        webService.execute();
        send_error=false;
        while (fw == 0) {
            Log.d("INFO","==== WAITING FOR ID ====");
            // Wait until finish
        }
        alltext = TextType.find(TextType.class,"idsavedform = ?",String.valueOf(sendSavedForms.get(0).getId_savedform()));
        for(int item = 0; item<alltext.size();item++){
            String postTextType = "nombre_form="+sendSavedForms.get(0).getName_form()+"&fechacreacion="+sendSavedForms.get(0).getCreated()+"&version="+Integer.toString(sendSavedForms.get(0).getVersion())+
                    "&terminado="+Boolean.toString(sendSavedForms.get(0).getFinished())+ "&texto="+alltext.get(item).getTextType()+"&created="+alltext.get(item).getCreated()+"&id_campo="+alltext.get(item).getId_field()+"&id_form="+alltext.get(item).getId_form()+"&posiciontomo="+alltext.get(item).getPosition();

            HttpPost dataText = new HttpPost(sendUrlsesiontexto, "POST", postTextType, sendSession,getApplicationContext(), new EventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    fx=1;
                    if(response.has("error")){
                        Log.i("lkyo","DONE addtexto");
                    }
                }
                @Override
                public void onFailure(Exception e) {
                    fx=2;
                    ToolBox newtool = new ToolBox();
                    newtool.sendNotification("Error",e.toString(),getApplicationContext());
                    //Toast.makeText(ViewPagerActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                }
            });
            dataText.execute();
        }

        allnumber = NumberType.find(NumberType.class,"idsavedform = ?",String.valueOf(sendSavedForms.get(0).getId_savedform()));
        for(int item = 0; item<allnumber.size();item++){
            String postNumberType = "nombre_form="+sendSavedForms.get(0).getName_form()+"&fechacreacion="+sendSavedForms.get(0).getCreated()+"&version="+Integer.toString(sendSavedForms.get(0).getVersion())+
                    "&terminado="+Boolean.toString(sendSavedForms.get(0).getFinished())+ "&numero="+allnumber.get(item).getNumberType()+"&created="+allnumber.get(item).getCreated()+ "&id_campo="+allnumber.get(item).getId_field()+ "&id_form="+allnumber.get(item).getId_form()+"&posiciontomo="+allnumber.get(item).getPosition();

            HttpPost dataNumber = new HttpPost(sendUrlsesionnumero, "POST", postNumberType, sendSession,getApplicationContext(), new EventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    fn=1;
                    if(response.has("error")){
                    }
                }
                @Override
                public void onFailure(Exception e) {
                    fn=2;
                    ToolBox newtool = new ToolBox();
                    newtool.sendNotification("Error",e.toString(),getApplicationContext());
                    //Toast.makeText(ViewPagerActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                }
            });
            dataNumber.execute();

        }

        alloption = OptionType.find(OptionType.class,"idsavedform = ?",String.valueOf(sendSavedForms.get(0).getId_savedform()));
        for(int item = 0; item<alloption.size();item++){
            String postOptionType = "nombre_form="+sendSavedForms.get(0).getName_form()+"&fechacreacion="+sendSavedForms.get(0).getCreated()+"&version="+Integer.toString(sendSavedForms.get(0).getVersion())+
                    "&terminado="+Boolean.toString(sendSavedForms.get(0).getFinished())+ "&opcion="+alloption.get(item).getOptionType()+"&created="+alloption.get(item).getCreated()+"&id_campo="+alloption.get(item).getId_field()+"&id_form="+alloption.get(item).getId_form()+"&posiciontomo="+alloption.get(item).getPosition();

            HttpPost dataOption = new HttpPost(sendUrlsesionopcion, "POST", postOptionType,sendSession, getApplicationContext(), new EventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    fo=1;
                    if(response.has("error")){
                    }
                }
                @Override
                public void onFailure(Exception e) {
                    fo=2;
                    ToolBox newtool = new ToolBox();
                    newtool.sendNotification("Error",e.toString(),getApplicationContext());
                    //Toast.makeText(ViewPagerActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                }
            });
            dataOption.execute();
        }

        alldate = DateType.find(DateType.class,"idsavedform = ?",String.valueOf(sendSavedForms.get(0).getId_savedform()));
        for(int item = 0; item<alldate.size();item++){
            String postDateType = "nombre_form="+sendSavedForms.get(0).getName_form()+"&fechacreacion="+sendSavedForms.get(0).getCreated()+"&version="+Integer.toString(sendSavedForms.get(0).getVersion())+
                    "&terminado="+Boolean.toString(sendSavedForms.get(0).getFinished())+ "&fecha="+alldate.get(item).getDateType()+"&created="+alldate.get(item).getCreated()+"&id_campo="+alldate.get(item).getId_field()+"&id_form="+alldate.get(item).getId_form()+"&posiciontomo="+alldate.get(item).getPosition();

            HttpPost dataDate = new HttpPost(sendUrlsesionfecha, "POST", postDateType, sendSession,getApplicationContext(), new EventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    fd=1;
                    if(response.has("error")){
                    }
                }
                @Override
                public void onFailure(Exception e) {
                    fd=2;
                    ToolBox newtool = new ToolBox();
                    newtool.sendNotification("Error",e.toString(),getApplicationContext());
                    //Toast.makeText(ViewPagerActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                }
            });
            dataDate.execute();
        }

        alldatetime = DateTimeType.find(DateTimeType.class,"idsavedform = ?",String.valueOf(sendSavedForms.get(0).getId_savedform()));
        for(int item = 0; item<alldatetime.size();item++){
            String nombre_form = sendSavedForms.get(0).getName_form();
            String fechacreacion = sendSavedForms.get(0).getCreated();
            String version = Integer.toString(sendSavedForms.get(0).getVersion());
            String terminado = Boolean.toString(sendSavedForms.get(0).getFinished());
            String horafecha = alldatetime.get(item).getDateTimeType();
            String created = alldatetime.get(item).getCreated();
            String id_campo = Integer.toString(alldatetime.get(item).getId_field());
            String id_form = Integer.toString(alldatetime.get(item).getId_form());
            String posiciontomo = Integer.toString(alldatetime.get(item).getPosition());
            String postDateTimeType = "nombre_form="+nombre_form+"&fechacreacion="+fechacreacion+"&version="+version+
                    "&terminado="+terminado+ "&horafecha="+horafecha+"&created="+created+"&id_campo="+id_campo+"&id_form="+id_form+
                    "&posiciontomo="+posiciontomo;

            HttpPost dataDateTime = new HttpPost(sendUrlsesionhorafecha, "POST", postDateTimeType, sendSession,getApplicationContext(), new EventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    ft=1;
                    if(response.has("error")){
                    }
                }
                @Override
                public void onFailure(Exception e) {
                    ft=2;
                    ToolBox newtool = new ToolBox();
                    newtool.sendNotification("Error",e.toString(),getApplicationContext());
                    //Toast.makeText(ViewPagerActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                }
            });
            dataDateTime.execute();
        }

        alllocation = LocationType.find(LocationType.class,"idsavedform = ?",String.valueOf(sendSavedForms.get(0).getId_savedform()));
        for(int item = 0; item<alllocation.size();item++){
            String postLocationType = "nombre_form="+sendSavedForms.get(0).getName_form()+"&fechacreacion="+sendSavedForms.get(0).getCreated()+"&version="+Integer.toString(sendSavedForms.get(0).getVersion())+
                    "&terminado="+Boolean.toString(sendSavedForms.get(0).getFinished())+ "&latitud="+alllocation.get(item).getLatitude()+"&created="+alllocation.get(item).getCreated()+"&longitud="+alllocation.get(item).getLongitude()+"&altitud="+alllocation.get(item).getAltitude()+"&id_campo="+alllocation.get(item).getId_field()+"&id_form="+alllocation.get(item).getId_form()+"&posiciontomo="+alllocation.get(item).getPosition();

            HttpPost dataLocation = new HttpPost(sendUrlsesionubicacion, "POST", postLocationType, sendSession, getApplicationContext(), new EventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    fl=1;
                    if(response.has("error")){
                    }
                }
                @Override
                public void onFailure(Exception e) {
                    fl=2;
                    ToolBox newtool = new ToolBox();
                    newtool.sendNotification("Error",e.toString(),getApplicationContext());
                    //Toast.makeText(ViewPagerActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                }
            });
            dataLocation.execute();
        }

        allqr = QrType.find(QrType.class,"idsavedform = ?",String.valueOf(sendSavedForms.get(0).getId_savedform()));
        for(int item = 0; item<allqr.size();item++){
            String postQrType = "nombre_form="+sendSavedForms.get(0).getName_form()+"&fechacreacion="+sendSavedForms.get(0).getCreated()+"&version="+Integer.toString(sendSavedForms.get(0).getVersion())+
                    "&terminado="+Boolean.toString(sendSavedForms.get(0).getFinished())+ "&texto="+allqr.get(item).getQrType()+"&created="+allqr.get(item).getCreated()+"&id_campo="+allqr.get(item).getId_field()+"&id_form="+allqr.get(item).getId_form()+"&posiciontomo="+allqr.get(item).getPosition();

            HttpPost dataQr = new HttpPost(sendUrlsesionqr, "POST", postQrType, sendSession, getApplicationContext(), new EventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    fq=1;
                    if(response.has("error")){
                    }
                }
                @Override
                public void onFailure(Exception e) {
                    fq=2;
                    ToolBox newtool = new ToolBox();
                    newtool.sendNotification("Error",e.toString(),getApplicationContext());
                    //Toast.makeText(ViewPagerActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                }
            });
            dataQr.execute();
        }

        allimage = ImageType.find(ImageType.class,"idsavedform = ?",String.valueOf(sendSavedForms.get(0).getId_savedform()));
        for(int item = 0; item<allimage.size();item++){
            String im = allimage.get(item).getImageType();
            if(im!=null){
                String postImageType = "";
                try {
                    postImageType = "nombre_form="+sendSavedForms.get(0).getName_form()+"&fechacreacion="+sendSavedForms.get(0).getCreated()+"&version="+Integer.toString(sendSavedForms.get(0).getVersion())+
                            "&terminado="+Boolean.toString(sendSavedForms.get(0).getFinished())+ "&imagen="+ URLEncoder.encode(im, "UTF-8")+"&id_campo="+allimage.get(item).getId_field()+"&created="+allimage.get(item).getCreated()+"&id_form="+allimage.get(item).getId_form()+"&posiciontomo="+allimage.get(item).getPosition();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                HttpPost dataImage = new HttpPost(sendUrlsesionimagen, "POST", postImageType, sendSession,getApplicationContext(), new EventListener<JSONObject>() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        fi=1;
                        if(response.has("error")){
                        }
                    }
                    @Override
                    public void onFailure(Exception e) {
                        fi=2;
                        ToolBox newtool = new ToolBox();
                        newtool.sendNotification("Error",e.toString(),getApplicationContext());
                        //Toast.makeText(ViewPagerActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                    }
                });
                dataImage.execute();
            }
        }

        allsign = SignType.find(SignType.class,"idsavedform = ?",String.valueOf(sendSavedForms.get(0).getId_savedform()));
        for(int item = 0; item<allsign.size();item++){
            String im = allsign.get(item).getSignType();
            String postDataType = "";
            if(im!=null){
                try {
                    postDataType = "nombre_form="+sendSavedForms.get(0).getName_form()+"&fechacreacion="+sendSavedForms.get(0).getCreated()+"&version="+Integer.toString(sendSavedForms.get(0).getVersion())+
                            "&terminado="+Boolean.toString(sendSavedForms.get(0).getFinished())+ "&firma="+ URLEncoder.encode(im, "UTF-8")+"&id_campo="+allsign.get(item).getId_field()+"&created="+allsign.get(item).getCreated()+"&id_form="+allsign.get(item).getId_form()+"&posiciontomo="+allsign.get(item).getPosition();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                HttpPost dataSign = new HttpPost(sendUrlsesionfirma, "POST", postDataType,sendSession, getApplicationContext(), new EventListener<JSONObject>() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        fs=1;
                        if(response.has("error")){
                        }
                    }
                    @Override
                    public void onFailure(Exception e) {
                        fs=2;
                        ToolBox newtool = new ToolBox();
                        newtool.sendNotification("Error",e.toString(),getApplicationContext());
                        //Toast.makeText(ViewPagerActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                    }
                });
                dataSign.execute();
            }
        }

        if (fw==2){
            send_error=true;
            Log.d("INFO","Error");
        }
        if (alltext.size()>0){
            while (fx == 0) {
                Log.d("INFO","esperando");
                // Wait until finish
            }
            if (fx==2){
                send_error=true;
                Log.d("INFO","Error");
            }
        }
        if (allnumber.size()>0){
            while (fn == 0) {
                Log.d("INFO","esperando");
                // Wait until finish
            }
            if (fn==2){
                send_error=true;
                Log.d("INFO","Error");
            }
        }
        if (alloption.size()>0){
            while (fo == 0) {
                Log.d("INFO","esperando");
                // Wait until finish
            }
            if (fo==2){
                send_error=true;
                Log.d("INFO","Error");
            }
        }
        if (alldate.size()>0){
            while (fd == 0) {
                Log.d("INFO","esperando");
                // Wait until finish
            }
            if (fd==2){
                send_error=true;
                Log.d("INFO","Error");
            }
        }
        if (alldatetime.size()>0){
            while (ft == 0) {
                Log.d("INFO","esperando");
                // Wait until finish
            }
            if (ft==2){
                send_error=true;
                Log.d("INFO","Error");
            }
        }
        if (alllocation.size()>0){
            while (fl == 0) {
                Log.d("INFO","esperando");
                // Wait until finish
            }
            if (fl==2){
                send_error=true;
                Log.d("INFO","Error");
            }
        }
        if (allqr.size()>0){
            while (fq == 0) {
                Log.d("INFO","esperando");
                // Wait until finish
            }
            if (fq==2){
                send_error=true;
                Log.d("INFO","Error");
            }
        }
        if (allimage.size()>0){
            while (fi == 0) {
                Log.d("INFO","esperando");
                // Wait until finish
            }
            if (fi==2){
                send_error=true;
                Log.d("INFO","Error");
            }
        }
        if (allsign.size()>0){
            while (fs == 0) {
                Log.d("INFO","esperando");
                // Wait until finish
            }
            if (fs==2){
                send_error=true;
                Log.d("INFO","Error");
            }
        }



        if (send_error){
            ToolBox newtool = new ToolBox();
            newtool.sendNotification("FR:"+sendSavedForms.get(0).getName_form(),"Error de Envio",getApplicationContext());
            finish();
        } else {
            String postDataType = "nombre_form="+sendSavedForms.get(0).getName_form()+"&fechacreacion="+sendSavedForms.get(0).getCreated()+"&id_form="+Integer.toString(sendSavedForms.get(0).getId_form())+"&version="+Integer.toString(sendSavedForms.get(0).getVersion())+
                    "&terminado="+Boolean.toString(sendSavedForms.get(0).getFinished())+"&store_name="+config.get(0).getInventory_name();

            HttpPost finalrequest = new HttpPost(sendUrlsesionfinal, "POST", postDataType, sendSession, getApplicationContext(), new EventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    if(response.has("error")){
                        Integer savedform = response.optInt("savedform");
                        Log.i("FOLIO:: ",Integer.toString(savedform));
                        runOnUiThread(new Runnable(){
                            @Override
                            public void run() {
                                if (counter == sendSavedForms.size()-1){
                                    if(dialogpro.isShowing())
                                        dialogpro.dismiss();
                                    finish();
                                }
                                counter++;
                            }

                        });
                    }
                }
                @Override
                public void onFailure(Exception e) {
                    ToolBox newtool = new ToolBox();
                    newtool.sendNotification("Error",e.toString(),getApplicationContext());
                    //Toast.makeText(ViewPagerActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                }
            });
            finalrequest.execute();
            sendSavedForms.get(0).setSended(true);
            sendSavedForms.get(0).setSelected(false);
            sendSavedForms.get(0).save();
            ToolBox newtool = new ToolBox();
            newtool.sendNotification("Formulario Guardado y Finalizado",sendSavedForms.get(0).getName_form(),getApplicationContext());
            //Toast.makeText(ViewPagerActivity.this,"Formulario Guardado y Finalizado!",Toast.LENGTH_LONG).show();
        }
    }

    private boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}
