package mx.com.madd.datacollect.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class HttpPost extends AsyncTask<Void, Void, String> {
    private EventListener<JSONObject> mCallBack;
    Context mContext;
    public String urldata;
    public Exception mException;
    String method;
    String postData;
    String authorization;


    public static final String MY_PREFS_NAME = "MyPrefsFile";

    public HttpPost(String urlparam,String method,String postData,String auth,Context context, EventListener callback) {
        authorization = auth;
        mContext = context;
        mCallBack = callback;
        urldata=urlparam;
        this.method = method;

        if (postData != null) {
            this.postData = postData;
        }
    }
    @Override
    protected String doInBackground(Void... params) {
        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        Log.d("INFO","Executing in background<<<<<<<<<<");
        // Will contain the raw JSON response as a string.
        String JsonStr = null;

        if(haveNetworkConnection(mContext)){
            try {
                Log.d("INFO","HTTP Class URL:"+urldata);
                URL url = new URL(urldata);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod(method);
                urlConnection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setRequestProperty( "charset", "utf-8");
                urlConnection.setRequestProperty ("Authorization", authorization);
                urlConnection.setConnectTimeout(10000);
                String urlParameters  = postData;
                Log.d("INFO","Parameter:"+urlParameters);
                mException=new Exception("Http unknown Error");
                byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
                int    postDataLength = postData.length;
                urlConnection.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
                urlConnection.setUseCaches( false );
                try( DataOutputStream wr = new DataOutputStream( urlConnection.getOutputStream())) {
                    wr.write( postData );
                }
                int statusCode = urlConnection.getResponseCode();
                String response="";
                if (statusCode ==  200) {
                    Log.d("INFO","Status:"+statusCode);
                    InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    response = convertInputStreamToString(inputStream);
                } else {
                    Log.d("INFO","Status:"+statusCode);
                    // Status code is not 200
                    // Do something to handle the error
                    mException=new Exception("Http Error:"+statusCode);
                }
                return response;
            } catch (Exception e) {
                Log.d("INFO", "Error "+e.toString());
                // If the code didn't successfully get the data, there's no point in attemping
                // to parse it.
                mException = e;
                return "";
            } finally{
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        mException = e;
                        Log.d("INFO", "Error:"+e.toString());
                    }
                }
            }
        }
        else {
            mException=new Exception("No Internet!");
            return "";
        }
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        //Log.d("INFO","Response:"+response);
        if (response.equals("")){
            //Log.d("INFO","Response equals space");
            mCallBack.onFailure(mException);
        } else {
            try {
                JSONObject obj = new JSONObject(response);
                //Log.d("INFO","Call Back Success");
                mCallBack.onSuccess(obj);
            } catch (Throwable t) {
                //Log.e("My App", "Could not parse malformed JSON:" );
                mCallBack.onFailure(mException);
            }
        }
    }

    private String convertInputStreamToString(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}

