package mx.com.madd.datacollect.activity.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.holders.PrincipalProductViewHolder;
import mx.com.madd.datacollect.model.inventory.TablePrincipalProduct;
import mx.com.madd.datacollect.model.inventory.TableProduct;

public class PrincipalProductAdapter extends RecyclerView.Adapter<PrincipalProductViewHolder> implements Filterable {

    List<TablePrincipalProduct> principalList;
    static List<TablePrincipalProduct> principalListAll;


    @Override
    public PrincipalProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.principal_layout, parent, false);
        return new PrincipalProductViewHolder(layoutView);
    }

    public PrincipalProductAdapter(Context context, List<TablePrincipalProduct> principal){
        principalList = principal;
        principalListAll=new ArrayList<>();
        principalListAll.addAll(principalList);
    }

    @Override
    public void onBindViewHolder(PrincipalProductViewHolder holder, int position) {
        final TablePrincipalProduct currentProduct = principalList.get(position);
        String a2=currentProduct.getSeries1();
        String a3=currentProduct.getSeries2();
        String a1=Integer.toString(currentProduct.getIdprincipal());
        int idproduct = currentProduct.getIdproduct();
        List<TableProduct> products = TableProduct.find(TableProduct.class,"idproduct = ?", Integer.toString(idproduct));
        String serie2 = products.get(0).getSeriesName2();
        holder.productName.setText(a2);
        holder.loteSeries1.setText(serie2+": "+a3);
        holder.idsubproduct.setText(a1);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<TablePrincipalProduct> filteredList = new ArrayList<>();
            if(constraint.toString().isEmpty()|| constraint.length()==0){
                filteredList.addAll(principalListAll);
            }else{
                for(TablePrincipalProduct product: principalListAll){
                    if(product.getSeries1().toLowerCase().contains(constraint.toString().toLowerCase()) || product.getSeries2().toLowerCase().contains(constraint.toString().toLowerCase()) ){
                        filteredList.add(product);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            principalList.clear();
            principalList.addAll((Collection<? extends TablePrincipalProduct>) results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public int getItemCount() {
        return principalList.size();
    }

}