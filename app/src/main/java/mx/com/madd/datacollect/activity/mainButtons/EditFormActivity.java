package mx.com.madd.datacollect.activity.mainButtons;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.adapters.EditFormsAdapter;
import mx.com.madd.datacollect.activity.listeners.EditFormsTouchListener;
import mx.com.madd.datacollect.model.TableSavedForm;

public class EditFormActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    EditFormsAdapter mAdapter;
    Context context;
    int position;
    String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_form);

        recyclerView = findViewById(R.id.edit_recycler);
        GridLayoutManager mGrid = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mGrid);
        recyclerView.setHasFixedSize(true);

        List<TableSavedForm> savedForms = TableSavedForm.listAll(TableSavedForm.class);
        List<TableSavedForm> editForms=new ArrayList<>();
        for(int index = 0; index < savedForms.size(); index ++){
            if(!savedForms.get(index).getFinished()){
                editForms.add(savedForms.get(index));
            }
        }
        mAdapter = new EditFormsAdapter(this,editForms);
        recyclerView.setAdapter(mAdapter);
        context=getApplicationContext();

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallbackItemTouchHelper);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.addOnItemTouchListener(new EditFormsTouchListener(getApplicationContext(), new EditFormsTouchListener.OnItemTouchListener() {
            @Override
            public void onItemClick(View view, int position) {
                TextView id = view.findViewById(R.id.savedFormId);
                String data = (String) id.getText();
                List<TableSavedForm> tableFormsList = TableSavedForm.find(TableSavedForm.class, "idsavedform = ?",data);
                if(!tableFormsList.isEmpty()){

                }
                int id_form = tableFormsList.get(0).getId_form();
                int version = tableFormsList.get(0).getVersion();
                String jobID = tableFormsList.get(0).getJobID();
                int pointer = position;
                String nameForm = tableFormsList.get(0).getName_form();
                Intent myIntent = new Intent(EditFormActivity.this, ViewPagerActivity.class);
                myIntent.putExtra("id_form",id_form);
                myIntent.putExtra("version",version);
                myIntent.putExtra("pointer",pointer);
                myIntent.putExtra("nameForm",nameForm);
                myIntent.putExtra("editFormId",Integer.parseInt(data));
                myIntent.putExtra("jobID",jobID);
                startActivity(myIntent);
                finish();
            }
        }));

    }

    ItemTouchHelper.SimpleCallback simpleCallbackItemTouchHelper = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT){

                @Override
                public boolean onMove(@androidx.annotation.NonNull RecyclerView recyclerView, @androidx.annotation.NonNull RecyclerView.ViewHolder viewHolder, @androidx.annotation.NonNull RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(@androidx.annotation.NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                    position = viewHolder.getAdapterPosition();
                    View view = viewHolder.itemView;
                    TextView id = view.findViewById(R.id.savedFormId);
                    data = (String) id.getText();
                    List<TableSavedForm> tableFormsList = TableSavedForm.find(TableSavedForm.class, "idsavedform = ?",data);
                    String nameForm = tableFormsList.get(0).getName_form();
                    if (direction == ItemTouchHelper.LEFT){
                        String message = "¿Seguro que deseas borrar el formulario: '"+nameForm+"'?";
                        AlertDialog dialog=new AlertDialog.Builder(EditFormActivity.this)
                                .setTitle("Borrar")
                                .setMessage(message)
                                .setIcon(R.drawable.borrarnegro)
                                .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .setNeutralButton("Borrar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        List<TableSavedForm> tableFormsList = TableSavedForm.find(TableSavedForm.class, "idsavedform = ?",data);
                                        tableFormsList.get(0).delete();
                                        recreate();
                                    }
                                })
                                .setCancelable(false)
                                .create();
                        dialog.show();
                    }

                    mAdapter.notifyDataSetChanged();
                }

                //La magia del dibujo
                @Override
                public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                    Paint p = new Paint();
                    Bitmap icon;
                    if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){
                        View itemView = viewHolder.itemView;
                        float height = (float) itemView.getBottom() - (float) itemView.getTop();
                        float width = height / 3;

                        if(dX > 0){  //DERECHA

                        } else { //IZQUIERDA
                            p.setColor(Color.parseColor("#D01020"));
                            RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                            c.drawRect(background,p);
                            icon = BitmapFactory.decodeResource(getResources(), R.drawable.borraricono);
                            RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                            c.drawBitmap(icon,null,icon_dest,p);
                        }
                    }
                    if(isCurrentlyActive == false){
                        c.drawColor(Color.TRANSPARENT);
                    }
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }

        });
        return true;
    }
}
