package mx.com.madd.datacollect.model.inventory;

import com.orm.SugarRecord;

public class TableProduct extends SugarRecord {

    int idproduct,total;
    String productName,codesku,seriesName1,seriesName2;

    public TableProduct() {
    }

    public TableProduct(int idproduct, int total, String productName, String codesku, String seriesName1, String seriesName2) {
        this.idproduct = idproduct;
        this.total = total;
        this.productName = productName;
        this.codesku = codesku;
        this.seriesName1 = seriesName1;
        this.seriesName2 = seriesName2;
    }

    public int getIdproduct() {
        return idproduct;
    }

    public void setIdproduct(int idproduct) {
        this.idproduct = idproduct;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCodesku() {
        return codesku;
    }

    public void setCodesku(String codesku) {
        this.codesku = codesku;
    }

    public String getSeriesName1() {
        return seriesName1;
    }

    public void setSeriesName1(String seriesName1) {
        this.seriesName1 = seriesName1;
    }

    public String getSeriesName2() {
        return seriesName2;
    }

    public void setSeriesName2(String seriesName2) {
        this.seriesName2 = seriesName2;
    }
}
