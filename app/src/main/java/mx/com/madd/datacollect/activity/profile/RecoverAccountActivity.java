package mx.com.madd.datacollect.activity.profile;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.OnEventListener;

import org.json.JSONObject;

public class RecoverAccountActivity extends AppCompatActivity {

    Button recover;
    EditText user;
    LinearLayout loader;

    String usr;
    String error;

    String url_sun = "https://api4sun.dudewhereismy.com.mx";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover_account);
        recover = findViewById(R.id.recoverAccount);
        user = findViewById(R.id.userRecover);
        loader = findViewById(R.id.loader);
        loader.setVisibility(View.GONE);
    }

    public void recoverAccount(View v){
        usr = user.getText().toString();
        recover.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);

        String url = url_sun + "/users/recovery";
        String postData = "user="+usr;
        Http webService = new Http(url, "POST", postData, "", getApplicationContext(), new OnEventListener<JSONObject>() {
            @Override
            public void onSuccess(JSONObject response) {
                if(response.has("error")){
                    error = response.optString("error");
                    if(error.equals("ok")){
                        recover.setText("Enviar correo nuevamente");
                    }else{
                        Toast.makeText(RecoverAccountActivity.this,error,Toast.LENGTH_LONG).show();
                    }
                }
                recover.setVisibility(View.VISIBLE);
                loader.setVisibility(View.GONE);
            }
            @Override
            public void onFailure(Exception e) {
                //error = e.toString();
                recover.setVisibility(View.VISIBLE);
                loader.setVisibility(View.GONE);
                Toast.makeText(RecoverAccountActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
            }
        });
        webService.execute();
    }
}
