package mx.com.madd.datacollect.activity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.holders.InventoriesViewHolder;
import mx.com.madd.datacollect.model.inventory.TableProduct;

public class InventoriesAdapter extends RecyclerView.Adapter<InventoriesViewHolder> implements Filterable {
    private Context context;
    private List<TableProduct> inventoryObject;
    private List<TableProduct> inventoryObjectAll;

    public InventoriesAdapter(Context context, List<TableProduct> inventoryObject){
        this.context = context;
        this.inventoryObject = inventoryObject;
        inventoryObjectAll=new ArrayList<>();
        inventoryObjectAll.addAll(inventoryObject);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<TableProduct> filteredList = new ArrayList<>();
            if(constraint.toString().isEmpty()|| constraint.length()==0){
                filteredList.addAll(inventoryObjectAll);
            }else{
                for(TableProduct product: inventoryObjectAll){
                    if(product.getProductName().toLowerCase().contains(constraint.toString().toLowerCase())){
                        filteredList.add(product);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            inventoryObject.clear();
            inventoryObject.addAll((Collection<? extends TableProduct>) results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public InventoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.inventories_layout, parent, false);
        return new InventoriesViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(InventoriesViewHolder holder, int position) {
        final TableProduct currentProduct = inventoryObject.get(position);
        String a1=currentProduct.getProductName();
        String a2=Integer.toString(currentProduct.getTotal());
        String a3= Integer.toString(currentProduct.getIdproduct());
        holder.productName.setText(a1);
        holder.loteSeries1.setText("En Stock: "+a2);
        holder.idProduct.setText(a3);
    }

    @Override
    public int getItemCount() {
        return inventoryObject.size();
    }
}
