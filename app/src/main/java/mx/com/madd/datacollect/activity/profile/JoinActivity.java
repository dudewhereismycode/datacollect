package mx.com.madd.datacollect.activity.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.MainActivity;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableEnterprise;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.OnEventListener;

public class JoinActivity extends AppCompatActivity {

    EditText rfc;
    String rfcString;

    LinearLayout loader;
    Button createJoinButton;
    String ok, url_sun;

    List<TableUser> userSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);
        rfc = findViewById(R.id.enterprise_rfc);
        loader = findViewById(R.id.loader);
        loader.setVisibility(View.INVISIBLE);

        createJoinButton = findViewById(R.id.createJoin);

        List<TableConfiguration> configDB = TableConfiguration.listAll(TableConfiguration.class);
        if(configDB.isEmpty()){
            String userP = "https://api4sun.dudewhereismy.com.mx";
            String formP = "https://api4mars.dudewhereismy.com.mx";
            String inventoryP = "";   //"https://venus.dudewhereismy.com.mx/";
            String ticketP = "";      //https://venus.dudewhereismy.com.mx/";
            String inventoryN = "";
            TableConfiguration newConfig = new TableConfiguration(userP,formP,inventoryP,ticketP,inventoryN);
            newConfig.save();
            url_sun = userP;
        }else{
            url_sun = configDB.get(0).getUserPlatform();
        }
    }

    public void alertError(String error){
        Toast.makeText(this,error,Toast.LENGTH_LONG).show();
        createJoinButton.setVisibility(View.VISIBLE);
        loader.setVisibility(View.GONE);
    }

    public void createEnterprise(View v){
        createJoinButton.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);

        ok="ok";

        rfcString = rfc.getText().toString();
        if(rfcString.length()<11){
            alertError("RFC muy corto");
            return;
        }

        String url = url_sun + "/enterprise/join";
        userSession = TableUser.listAll(TableUser.class);
        String session = userSession.get(0).getSession();
        String postData = "internal_id="+userSession.get(0).getInternal_id()+"&user="+userSession.get(0).getUser()+"&rfc="+rfcString;
        Http webService = new Http(url, "POST", postData, session,getApplicationContext(), new OnEventListener<JSONObject>() {
            @Override
            public void onSuccess(JSONObject response) {
                if(response!=null){
                    if(response.has("error")){
                        ok = response.optString("error");
                        if(ok.equals("ok")){
                            TableEnterprise.deleteAll(TableEnterprise.class);
                            String app_id = response.optString("application_id");
                            String enterpriseString = response.optString("enterprise");
                            String emailString = response.optString("email");
                            String phoneString = response.optString("phone");
                            String internal_id = response.optString("internal_id");
                            alertError("Se unio a la empresa con exito");
                            TableEnterprise newEnterprise = new TableEnterprise(enterpriseString,rfcString,emailString,phoneString,Integer.parseInt(app_id),Integer.parseInt(internal_id));
                            newEnterprise.save();
                            userSession.get(0).setApplication_id(Integer.parseInt(app_id));
                            userSession.get(0).setInternal_id(Integer.parseInt(internal_id));
                            userSession.get(0).save();
                            Intent openMainActivity = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(openMainActivity);
                            finish();
                        }else{
                            alertError(ok);
                        }
                    }
                }else{
                    alertError("No se pudo conectar con el servidor");
                }
                createJoinButton.setVisibility(View.VISIBLE);
                loader.setVisibility(View.GONE);
            }
            @Override
            public void onFailure(Exception e) {
                //ok = e.toString();
                alertError("Error en el servidor");
            }
        });
        webService.execute();
    }
}
