package mx.com.madd.datacollect.utility;

import org.json.JSONException;

public interface EventListener<T> {
    public void onSuccess(T object);
    public void onFailure(Exception e);
}