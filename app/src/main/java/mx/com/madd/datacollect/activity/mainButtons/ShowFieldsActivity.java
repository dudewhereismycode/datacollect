package mx.com.madd.datacollect.activity.mainButtons;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.text.Html;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.adapters.FormsAdapter;
import mx.com.madd.datacollect.activity.listeners.FormsTouchListener;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableFormFields;
import mx.com.madd.datacollect.model.TableForms;

public class ShowFieldsActivity extends AppCompatActivity {

    List<TableConfiguration> configDB;

    RecyclerView recyclerView;
    List<TableForms> formsList;
    FormsAdapter mAdapter;
    Boolean show;

    String viewList;
    String message;
    int pointer;

    int position;
    String data,clasification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_fields);

        configDB = TableConfiguration.listAll(TableConfiguration.class);
        if(configDB.isEmpty()){
            String userP = "https://api4sun.dudewhereismy.com.mx";
            String formP = "https://api4mars.dudewhereismy.com.mx";
            String inventoryP = "";   //"https://venus.dudewhereismy.com.mx/";
            String ticketP = "";      //https://venus.dudewhereismy.com.mx/";
            String inventoryN = "";
            TableConfiguration newConfig = new TableConfiguration(userP,formP,inventoryP,ticketP,inventoryN);
            newConfig.save();
            viewList = "";
        }else{
            viewList = configDB.get(0).getTicketPlatform();
        }
        Intent myIntent = getIntent();
        show =  myIntent.getBooleanExtra("show",false);

        Intent resultIntent = getIntent();
        clasification = resultIntent.getStringExtra("clasification");
        recyclerView = findViewById(R.id.forms_recycler);

        GridLayoutManager mGrid = new GridLayoutManager(this,1);
        recyclerView.setLayoutManager(mGrid);
        recyclerView.setHasFixedSize(true);

        formsList = TableForms.listAll(TableForms.class);
        if(!clasification.equals("0")){
            formsList = TableForms.find(TableForms.class,"clasification = ? ",clasification);
        }

        mAdapter = new FormsAdapter(this,formsList,show);

        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallbackItemTouchHelper);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        recyclerView.addOnItemTouchListener(new FormsTouchListener(getApplicationContext(), new FormsTouchListener.OnItemTouchListener() {
            @Override
            public void onItemClick(View w, int position) {
                TextView id = (TextView)w.findViewById(R.id.idForm);
                String data = (String) id.getText();
                pointer = Integer.parseInt(data);

                List<TableForms> touchFormsList = TableForms.find(TableForms.class,"idform = ? ",data);
                int id_form = touchFormsList.get(0).getId_form();
                int version = touchFormsList.get(0).getVersion();
                String nameForm = touchFormsList.get(0).getName();

                Intent myIntent = new Intent(ShowFieldsActivity.this, ViewPagerActivity.class);
                myIntent.putExtra("id_form",id_form);
                myIntent.putExtra("version",version);
                myIntent.putExtra("pointer",pointer);
                myIntent.putExtra("nameForm",nameForm);
                myIntent.putExtra("editFormId",-1);
                myIntent.putExtra("jobID","");
                startActivity(myIntent);
            }
        }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_and_config, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });

        MenuItem itemGear = menu.findItem(R.id.gear);
        if(viewList.equals("Todo")){
            itemGear.setTitle("Ver por Clasificación");
        }else{
            itemGear.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.gear) {
            configDB = TableConfiguration.listAll(TableConfiguration.class);
            configDB.get(0).setTicketPlatform("Clasificación");
            configDB.get(0).save();
            finish();
            return true;
        }
        return true;
    }

    public void showAlerDialog(String title, String message, Boolean button){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(Html.fromHtml(message));
        if(button == true){
            builder.setPositiveButton("OK", null);
        }
        builder.create();
        builder.show();
    }

    ItemTouchHelper.SimpleCallback simpleCallbackItemTouchHelper = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT){

        @Override
        public boolean onMove(@androidx.annotation.NonNull RecyclerView recyclerView, @androidx.annotation.NonNull RecyclerView.ViewHolder viewHolder, @androidx.annotation.NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@androidx.annotation.NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            position = viewHolder.getAdapterPosition();
            View view = viewHolder.itemView;
            TextView id = view.findViewById(R.id.idForm);
            data = (String) id.getText();
            List<TableForms> tableFormsList = TableForms.find(TableForms.class, "idform = ?",data);
            if (direction == ItemTouchHelper.LEFT){
                String[] whereArgs = new String [2];
                whereArgs[0] = String.valueOf(tableFormsList.get(0).getId_form());
                whereArgs[1] = String.valueOf(tableFormsList.get(0).getVersion());
                List<TableFormFields> touchFormFieldsList = TableFormFields.find(TableFormFields.class,"idform = ? and version = ?",whereArgs);

                message = "";
                for(int counter = 0; counter < touchFormFieldsList.size(); counter++){
                    message += "<b>"+touchFormFieldsList.get(counter).getFieldName()+"</b> :: <i>"+touchFormFieldsList.get(counter).getType()+"</i><br>";
                }
                showAlerDialog("Campos del Formulario",message,true);
            }

            mAdapter.notifyDataSetChanged();
        }

        //La magia del dibujo
        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            Paint p = new Paint();
            Bitmap icon;
            if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                View itemView = viewHolder.itemView;
                float height = (float) itemView.getBottom() - (float) itemView.getTop();
                float width = height / 3;

                if(dX > 0){  //DERECHA

                } else { //IZQUIERDA
                    p.setColor(Color.parseColor("#040404"));
                    RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                    c.drawRect(background,p);
                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.ojo_blanco);
                    RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                    c.drawBitmap(icon,null,icon_dest,p);
                }
            }
            if(isCurrentlyActive == false){
                c.drawColor(Color.TRANSPARENT);
            }
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    };


}
