package mx.com.madd.datacollect.activity.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import mx.com.madd.datacollect.R;

public class ClasificationViewHolder extends RecyclerView.ViewHolder {

    public TextView nameForm,createdForm,savedFormId;
    public View mItemView;
    public ImageView imageForm;

    public ClasificationViewHolder(View itemView){
        super(itemView);
        mItemView = itemView;
        nameForm = itemView.findViewById(R.id.name_forms);
        createdForm = itemView.findViewById(R.id.fecha_forms);
        imageForm = itemView.findViewById(R.id.fImage);
        savedFormId = itemView.findViewById(R.id.idForm);
        savedFormId.setVisibility(View.INVISIBLE);
    }

}