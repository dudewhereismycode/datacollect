package mx.com.madd.datacollect.model;

import com.orm.SugarRecord;

public class TableConfiguration extends SugarRecord {

    String user_platform; //plataforma para consultar el login de los usuarios
    String form_platform; //plataforma para consultar los formularios
    String inventory_platform; //plataforma para consultar los inventarios
    String ticket_platform; //plataforma para consultar los tickets
    String inventory_name;

    public TableConfiguration() {
    }

    public TableConfiguration(String user_platform, String form_platform, String inventory_platform, String ticket_platform, String inventory_name) {
        this.user_platform = user_platform;
        this.form_platform = form_platform;
        this.inventory_platform = inventory_platform;
        this.ticket_platform = ticket_platform;
        this.inventory_name = inventory_name;
    }

    public String getInventory_name() {
        return inventory_name;
    }

    public void setInventory_name(String inventory_name) {
        this.inventory_name = inventory_name;
    }

    public String getUserPlatform() {
        return user_platform;
    }

    public void setUserPlatform(String user_platform) {
        this.user_platform = user_platform;
    }

    public String getFormPlatform() {
        return form_platform;
    }

    public void setFormPlatform(String form_platform) {
        this.form_platform = form_platform;
    }

    public String getInventoryPlatform() {
        return inventory_platform;
    }

    public void setInventoryPlatform(String inventory_platform) {
        this.inventory_platform = inventory_platform;
    }

    public String getTicketPlatform() {
        return ticket_platform;
    }

    public void setTicketPlatform(String ticket_platform) {
        this.ticket_platform = ticket_platform;
    }
}
