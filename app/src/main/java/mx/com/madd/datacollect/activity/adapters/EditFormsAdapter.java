package mx.com.madd.datacollect.activity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.holders.EditFormsViewHolder;
import mx.com.madd.datacollect.model.TableSavedForm;

public class EditFormsAdapter extends RecyclerView.Adapter<EditFormsViewHolder>  implements Filterable {

    private List<TableSavedForm> editFormObject;
    private List<TableSavedForm> editFormObjectAll;

    public EditFormsAdapter(Context context, List<TableSavedForm> editFormObject) {
        this.editFormObject = editFormObject;
        editFormObjectAll=new ArrayList<>();
        editFormObjectAll.addAll(editFormObject);
    }

    @Override
    public EditFormsViewHolder onCreateViewHolder(@androidx.annotation.NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.edit_layout, parent, false);
        return new EditFormsViewHolder(layoutView);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<TableSavedForm> filteredList = new ArrayList<>();
            if(constraint.toString().isEmpty()|| constraint.length()==0){
                filteredList.addAll(editFormObjectAll);
            }else{
                for(TableSavedForm product: editFormObjectAll){
                    if(product.getName_form().toLowerCase().contains(constraint.toString().toLowerCase())){
                        filteredList.add(product);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            editFormObject.clear();
            editFormObject.addAll((Collection<? extends TableSavedForm>) results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public void onBindViewHolder(@androidx.annotation.NonNull EditFormsViewHolder holder, int position) {
        final TableSavedForm currentForms = editFormObject.get(position);
        String a1=currentForms.getName_form();
        String a2=currentForms.getCreated();
        int a3=currentForms.getId_savedform();
        holder.nameForm.setText(a1);
        holder.createdForm.setText(a2);
        holder.savedFormId.setText(Integer.toString(a3));
    }

    @Override
    public int getItemCount() {
        return editFormObject.size();
    }
}