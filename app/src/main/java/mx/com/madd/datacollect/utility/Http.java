package mx.com.madd.datacollect.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class Http extends AsyncTask<Void, Void, String> {

    private OnEventListener<JSONObject> mCallBack;
    Context mContext;
    public String urldata;
    String method;
    String postData;
    String authorization;
    public Exception mException;

    public Http(String urlparam,String method,String postData,String auth,Context context, OnEventListener callback) {
        mCallBack = callback;
        mContext = context;
        urldata=urlparam;
        authorization = auth;
        this.method = method;
        if (postData != null) {
            this.postData = postData;
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        // Will contain the raw JSON response as a string.
        String JsonStr = null;
        if(haveNetworkConnection(mContext)){
            try {
                URL url = new URL(urldata);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod(method);
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setRequestProperty("charset", "utf-8");
                urlConnection.setRequestProperty("Authorization", authorization);
                if (method == "GET"){
                    urlConnection.setConnectTimeout(10000); //set timeout to 5 seconds
                    try{
                        urlConnection.connect();
                    }catch (final IOException e) {
                        Log.e("MSG PlaceholderFragment", "Error closing stream", e);
                    }

                    // Read the input stream into a String
                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuffer buffer = new StringBuffer();
                    if (inputStream == null) {
                        // Nothing to do.
                        return null;
                    }
                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                        // But it does make debugging a *lot* easier if you print out the completed
                        // buffer for debugging.
                        buffer.append(line + "\n");
                    }

                    if (buffer.length() == 0) {
                        // Stream was empty.  No point in parsing.
                        return null;
                    }
                    JsonStr = buffer.toString();
                }else{
                    urlConnection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
                    urlConnection.setRequestProperty( "charset", "utf-8");
                    String urlParameters  = postData;
                    byte[] postData       = urlParameters.getBytes(StandardCharsets.UTF_8);
                    int    postDataLength = postData.length;

                    urlConnection.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
                    urlConnection.setUseCaches( false );
                    try(DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream())) {
                        wr.write( postData );
                    }catch (final IOException e) {
                        Log.e("MSG PlaceholderFragment", "Error closing stream", e);
                    }

                    int statusCode = urlConnection.getResponseCode();
                    String response="";
                    if (statusCode ==  200) {
                        InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                        response = convertInputStreamToString(inputStream);
                        // From here you can convert the string to JSON with whatever JSON parser you like to use
                        // After converting the string to JSON, I call my custom callback. You can follow this process too, or you can implement the onPostExecute(Result) method
                    } else {

                        if (statusCode == 400) {
                            response = "{'error': 'Error en petición. Reincie aplicación'}";
                        }
                        if (statusCode == 401) {
                            response = "{'error': 'No autorizado. Error en credenciales'}";
                        }
                        if (statusCode == 404) {
                            response = "{'error': 'No autorizado. Error en credenciales'}";
                        }
                        if (statusCode == 402) {
                            response = "{'error': 'Requiere Pago'}";
                        }
                        if (statusCode == 403) {
                            response = "{'error': 'Cuenta Repetida'}";
                        }
                        if (statusCode == 404) {
                            response = "{'error': 'No se encontro la petición'}";
                        }
                        if (statusCode == 405) {
                            response = "{'error': 'Metodo no permitido'}";
                        }
                        if (statusCode == 406) {
                            response = "{'error': 'No permitido'}";
                        }
                        if (statusCode == 500) {
                            response = "{'error': 'Error Interno de Sevidor'}";
                        }
                        if (statusCode == 504) {
                            response = "{'error': 'No se obtuvo respuesta'}";
                        }
                        if (statusCode == 503) {
                            response = "{'error': 'Servicio no disponible'}";
                        }
                        if (response == "") {
                            response = "{'error': 'Error: "+Integer.toString(statusCode)+"'}";
                        }
                        // Status code is not 200
                        // Do something to handle the error
                    }
                    JsonStr = response.toString();
                }
                return JsonStr;
            } catch (Exception e) {
                Log.e("PlaceholderFragment", "Error ", e);
                // If the code didn't successfully get the data, there's no point in attemping
                // to parse it.
                mException = e;
                return null;
            } finally{
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("PlaceholderFragment", "Error closing stream", e);
                    }
                }
            }
        }
        else {
            mException=new Exception("No Internet!");
            return null;
        }
    }


    @Override
    protected void onPostExecute(String response) {

        super.onPostExecute(response);
        if (mCallBack != null) {
            if (mException == null) {
                JSONObject obj=null;
                JSONObject errorjson=null;
                try {
                    String errorstring = "{\"Error\":\"Could not parse malformed JSON\"}";
                    errorjson = new JSONObject(errorstring);
                }
                catch(Throwable t)
                {Log.e("My App", "Could not parse malformed ERORR JSON");}
                try {
                    obj = new JSONObject(response);
                } catch (Throwable t) {
                    Log.e("My App", "Could not parse malformed JSON:" );
                    mCallBack.onFailure(mException);
                }
                /*
                try {
                    mCallBack.onSuccess(obj);
                } catch (JSONException e) {
                    //e.printStackTrace();
                }
                 */
            }
        } else {
            mCallBack.onFailure(mException);
        }
    }

    private String convertInputStreamToString(InputStream inputStream) {

        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}