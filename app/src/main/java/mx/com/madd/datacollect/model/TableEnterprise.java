package mx.com.madd.datacollect.model;

import com.orm.SugarRecord;

public class TableEnterprise extends SugarRecord{

    String enterprise, rfc, email, phone;
    int application_id, internal_id;

    public TableEnterprise() {
    }

    public TableEnterprise(String enterprise, String rfc, String email, String phone, int application_id, int internal_id) {
        this.enterprise = enterprise;
        this.rfc = rfc;
        this.email = email;
        this.phone = phone;
        this.application_id = application_id;
        this.internal_id = internal_id;
    }

    public String getEnterprice() {
        return enterprise;
    }

    public void setEnterprice(String enterprise) {
        this.enterprise = enterprise;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getApplication_id() {
        return application_id;
    }

    public void setApplication_id(int application_id) {
        this.application_id = application_id;
    }

    public int getInternal_id() {
        return internal_id;
    }

    public void setInternal_id(int internal_id) {
        this.internal_id = internal_id;
    }
}
