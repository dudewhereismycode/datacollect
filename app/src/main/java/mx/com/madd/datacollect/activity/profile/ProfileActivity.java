package mx.com.madd.datacollect.activity.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.MainActivity;
import mx.com.madd.datacollect.activity.mainButtons.ShowFieldsActivity;
import mx.com.madd.datacollect.model.TableEnterprise;
import mx.com.madd.datacollect.model.TableUser;

public class ProfileActivity extends AppCompatActivity {

    TextView user;
    TextView name;
    TextView email;
    TextView phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        user = findViewById(R.id.userView);
        name = findViewById(R.id.nameView);
        email = findViewById(R.id.emailView);
        phone = findViewById(R.id.phoneView);

        List<TableUser> users = TableUser.listAll(TableUser.class);
        user.setText(users.get(0).getUser());
        name.setText(users.get(0).getName());
        email.setText(users.get(0).getEmail());
        phone.setText(users.get(0).getPhone());

    }

    public void changePassword(View w){
        Intent myIntent = new Intent(ProfileActivity.this, ChangePasswordActivity.class);
        startActivity(myIntent);
        finish();
    }

    public void updateAccount(View w){
        Intent myIntent = new Intent(ProfileActivity.this, UpdateAccountActivity.class);
        startActivity(myIntent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile, menu);
        List<TableEnterprise> enterprise = TableEnterprise.listAll(TableEnterprise.class);

        if(enterprise.isEmpty()){
            MenuItem item = menu.findItem(R.id.myenterprice);
            item.setVisible(false);
        }else{
            MenuItem item = menu.findItem(R.id.joinenterprice);
            item.setVisible(false);
            MenuItem item2 = menu.findItem(R.id.createenteprice);
            item2.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.joinenterprice) {
            Intent openJoinActivity = new Intent(getApplicationContext(), JoinActivity.class);
            startActivity(openJoinActivity);
            return true;
        }
        if (id == R.id.createenteprice) {
            Intent openEnterpriceActivity = new Intent(getApplicationContext(), CreateEnterpriseActivity.class);
            startActivity(openEnterpriceActivity);
            return true;
        }
        if (id == R.id.myenterprice) {
            Intent openEnterpriseActivity = new Intent(getApplicationContext(), EnterpriseActivity.class);
            startActivity(openEnterpriseActivity);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
