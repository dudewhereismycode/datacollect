package mx.com.madd.datacollect.activity.inventory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.adapters.PrincipalProductAdapter;
import mx.com.madd.datacollect.activity.listeners.PrincipalProductTouchListener;
import mx.com.madd.datacollect.model.inventory.TablePrincipalProduct;

public class PrincipalProductActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    PrincipalProductAdapter mAdapter;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal_product);

        Intent myIntent = getIntent();
        String idproduct = myIntent.getStringExtra("idproduct");

        recyclerView = (RecyclerView) findViewById(R.id.principal_layout);
        GridLayoutManager mGrid = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mGrid);
        recyclerView.setHasFixedSize(true);

        List<TablePrincipalProduct> principalProducts = TablePrincipalProduct.find(TablePrincipalProduct.class,"idproduct = ?",idproduct);
        mAdapter = new PrincipalProductAdapter(this,principalProducts);
        recyclerView.setAdapter(mAdapter);
        context=getApplicationContext();


        recyclerView.addOnItemTouchListener(new PrincipalProductTouchListener(getApplicationContext(), new PrincipalProductTouchListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                TextView id = (TextView)view.findViewById(R.id.id_subproduct);
                String data = (String) id.getText();
                List<TablePrincipalProduct> principalProducts = TablePrincipalProduct.find(TablePrincipalProduct.class,"idprincipal = ?",data);

                if(!principalProducts.toString().equals("[]")){
                    Intent myIntent = new Intent(PrincipalProductActivity.this, SubProductActivity.class);
                    myIntent.putExtra("idprincipal",data);
                    startActivity(myIntent);
                }else{
                    Toast.makeText(PrincipalProductActivity.this,"No cuenta con subproductos",Toast.LENGTH_LONG).show();
                }
            }
        }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }
}
