package mx.com.madd.datacollect.model;

import com.orm.SugarRecord;

public class TableFormFieldsOptions extends SugarRecord {

    String value;
    int id_fieldparent, id_fieldoption,id_field, position, vesion, id_savedform;

    public TableFormFieldsOptions() {
    }

    public TableFormFieldsOptions(String value, int id_fieldparent, int id_fieldoption, int id_field, int position, int vesion, int id_savedform) {
        this.value = value;
        this.id_fieldparent = id_fieldparent;
        this.id_fieldoption = id_fieldoption;
        this.id_field = id_field;
        this.position = position;
        this.vesion = vesion;
        this.id_savedform = id_savedform;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getId_fieldParent() {
        return id_fieldparent;
    }

    public void setId_fieldParent(int id_fieldparent) {
        this.id_fieldparent = id_fieldparent;
    }

    public int getId_fieldOption() {
        return id_fieldoption;
    }

    public void setId_fieldOption(int id_fieldoption) {
        this.id_fieldoption = id_fieldoption;
    }

    public int getId_field() {
        return id_field;
    }

    public void setId_field(int id_field) {
        this.id_field = id_field;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getVesion() {
        return vesion;
    }

    public void setVesion(int vesion) {
        this.vesion = vesion;
    }

    public int getId_savedform() {
        return id_savedform;
    }

    public void setId_savedform(int id_savedform) {
        this.id_savedform = id_savedform;
    }
}

/*
*
* class FormParentOptative(DeclarativeBase):
    __tablename__='formparentoptative'
    id_formparentoptative=Column(Integer,primary_key=True)
    id_formparent=Column(Integer,ForeignKey('formulario.id_form'))
    id_formoptative=Column(Integer,ForeignKey('formulario.id_form'))
    id_campo=Column(Integer,ForeignKey('campos.id_campo'))
    valor=Column(Unicode(500))
    posicion=Column(Integer)
* */