package mx.com.madd.datacollect.activity.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.MainActivity;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableEnterprise;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.OnEventListener;

public class EnterpriseActivity extends AppCompatActivity {

    TextView enterprise, rfc, email, phone;
    List<TableEnterprise> enterpriseList;
    LinearLayout loader2;
    Button delete;

    String ok, url_sun;
    List<TableUser> userSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise2);

        enterprise = findViewById(R.id.enterprise);
        rfc = findViewById(R.id.enterprise_rfc);
        email = findViewById(R.id.enterprise_email);
        phone = findViewById(R.id.enterpise_phone);
        delete = findViewById(R.id.deleteEnterprise);

        loader2 = findViewById(R.id.loader);
        loader2.setVisibility(View.INVISIBLE);

        enterpriseList = TableEnterprise.listAll(TableEnterprise.class);

        if(!enterpriseList.isEmpty()){
            enterprise.setText(enterpriseList.get(0).getEnterprice());
            rfc.setText(enterpriseList.get(0).getRfc());
            email.setText(enterpriseList.get(0).getEmail());
            phone.setText(enterpriseList.get(0).getPhone());
        }

    }

    public void openDialog(View view){

        AlertDialog dialog=new AlertDialog.Builder(this)
                .setTitle("Atención")
                .setMessage("¿Seguro que desea eliminar la empresa? Si lo hace ya no podrán ver los reportes que usted suba")
                .setIcon(R.drawable.alert)
                .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        loader2.setVisibility(View.INVISIBLE);
                        delete.setVisibility(View.VISIBLE);
                    }
                })
                .setNeutralButton("Borrar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteEnterprise();
                    }
                })
                .setCancelable(false)
                .create();
        dialog.show();

    }

    public void deleteEnterprise(){
        List<TableConfiguration> configDB = TableConfiguration.listAll(TableConfiguration.class);
        if(configDB.isEmpty()){
            url_sun = "https://api4sun.dudewhereismy.com.mx";
            String userP = "https://api4sun.dudewhereismy.com.mx";
            String formP = "https://api4mars.dudewhereismy.com.mx";
            String inventoryP = "";   //"https://venus.dudewhereismy.com.mx/";
            String ticketP = "";      //https://venus.dudewhereismy.com.mx/";
            String inventoryN = "";
            TableConfiguration newConfig = new TableConfiguration(userP,formP,inventoryP,ticketP,inventoryN);
            newConfig.save();
        }else{
            url_sun = configDB.get(0).getUserPlatform();
        }
        userSession = TableUser.listAll(TableUser.class);
        String session = userSession.get(0).getSession();
        String url = url_sun + "/enterprise";
        String postData = "internal_id="+userSession.get(0).getInternal_id()+"&user="+userSession.get(0).getUser()+"&rfc="+enterpriseList.get(0).getRfc();
        Http webService = new Http(url, "DELETE", postData, session,getApplicationContext(), new OnEventListener<JSONObject>() {
            @Override
            public void onSuccess(JSONObject response) {
                if(response.has("error")){
                    ok = response.optString("error");
                    if(ok.equals("ok")){
                        TableEnterprise.deleteAll(TableEnterprise.class);
                        String application_id = response.optString("application_id");
                        String internal_id = response.optString("internal_id");
                        userSession.get(0).setInternal_id(Integer.parseInt(internal_id));
                        userSession.get(0).setApplication_id(Integer.parseInt(application_id));
                        Intent openMainActivity = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(openMainActivity);
                        finish();
                    }
                    recreate();
                }
            }
            @Override
            public void onFailure(Exception e) {
            }
        });
        webService.execute();
    }
}
