package mx.com.madd.datacollect.activity.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.OnEventListener;

public class UpdateAccountActivity extends AppCompatActivity {

    TextView user;
    EditText name;
    EditText email;
    EditText phone;
    Button update;
    LinearLayout loader;

    String url_sun;
    String name0;
    String email0;
    String phone0;
    List<TableUser> userSession;
    List<TableConfiguration> configDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_account);

        user = findViewById(R.id.userView);
        name = findViewById(R.id.nameView);
        email = findViewById(R.id.emailView);
        phone = findViewById(R.id.phoneView);
        update = findViewById(R.id.updateData);
        loader = findViewById(R.id.loader);

        List<TableUser> users = TableUser.listAll(TableUser.class);
        user.setText(users.get(0).getUser());
        name.setText(users.get(0).getName());
        email.setText(users.get(0).getEmail());
        phone.setText(users.get(0).getPhone());

        loader = findViewById(R.id.loader);
        loader.setVisibility(View.GONE);

        configDB = TableConfiguration.listAll(TableConfiguration.class);

        if(configDB.isEmpty()){
            String userP = "https://api4sun.dudewhereismy.com.mx";
            String formP = "https://api4mars.dudewhereismy.com.mx";
            String inventoryP = "";   //"https://venus.dudewhereismy.com.mx/";
            String ticketP = "";      //https://venus.dudewhereismy.com.mx/";
            String inventoryN = "";
            TableConfiguration newConfig = new TableConfiguration(userP,formP,inventoryP,ticketP,inventoryN);
            newConfig.save();
            url_sun = userP;
        }else{
            url_sun = configDB.get(0).getUserPlatform();
        }
        userSession = TableUser.listAll(TableUser.class);
    }

    public void updateAccount(View view){
        update.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);

        name0 = name.getText().toString();
        email0 = email.getText().toString();
        phone0 = phone.getText().toString();

        String url = url_sun + "/users/profile";
        String postData = "user="+user.getText().toString()+"&display_name="+name0+"&email="+email0+"&phone="+phone0;
        String session = userSession.get(0).getSession();
            Http webService = new Http(url, "PUT", postData, session, getApplicationContext(), new OnEventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    if(response.has("error")){
                        String error = response.optString("error");
                        if(error.equals("ok")){
                            userSession.get(0).setName(name0);
                            userSession.get(0).setPhone(phone0);
                            userSession.get(0).setEmail(email0);
                            userSession.get(0).save();
                            finish();
                            Toast.makeText(UpdateAccountActivity.this,"Actualización exitosa",Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(UpdateAccountActivity.this,error,Toast.LENGTH_LONG).show();
                        }
                    }
                    update.setVisibility(View.VISIBLE);
                    loader.setVisibility(View.GONE);

                }
                @Override
                public void onFailure(Exception e) {
                    update.setVisibility(View.VISIBLE);
                    loader.setVisibility(View.GONE);
                    Toast.makeText(UpdateAccountActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                }
            });
            webService.execute();

    }
}
