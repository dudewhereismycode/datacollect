package mx.com.madd.datacollect.model.inventory;

import com.orm.SugarRecord;

public class TableSubProduct extends SugarRecord {

    int idsub, idprincipal, total;
    String productName, series1, series2;

    public TableSubProduct() {
    }

    public TableSubProduct(int idsub, int idprincipal, int total, String productName, String series1, String series2) {
        this.idsub = idsub;
        this.idprincipal = idprincipal;
        this.total = total;
        this.productName = productName;
        this.series1 = series1;
        this.series2 = series2;
    }

    public int getIdsub() {
        return idsub;
    }

    public void setIdsub(int idsub) {
        this.idsub = idsub;
    }

    public int getIdprincipal() {
        return idprincipal;
    }

    public void setIdprincipal(int idprincipal) {
        this.idprincipal = idprincipal;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSeries1() {
        return series1;
    }

    public void setSeries1(String series1) {
        this.series1 = series1;
    }

    public String getSeries2() {
        return series2;
    }

    public void setSeries2(String series2) {
        this.series2 = series2;
    }

}
