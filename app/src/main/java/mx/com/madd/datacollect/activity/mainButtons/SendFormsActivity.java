package mx.com.madd.datacollect.activity.mainButtons;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.MainActivity;
import mx.com.madd.datacollect.activity.adapters.SendFormsAdapter;
import mx.com.madd.datacollect.activity.listeners.SendFormsTouchListener;
import mx.com.madd.datacollect.model.TableClasification;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableEnterprise;
import mx.com.madd.datacollect.model.TableFormFields;
import mx.com.madd.datacollect.model.TableFormFieldsOptions;
import mx.com.madd.datacollect.model.TableForms;
import mx.com.madd.datacollect.model.TableSavedForm;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.model.dataTypes.DateTimeType;
import mx.com.madd.datacollect.model.dataTypes.DateType;
import mx.com.madd.datacollect.model.dataTypes.ImageType;
import mx.com.madd.datacollect.model.dataTypes.LocationType;
import mx.com.madd.datacollect.model.dataTypes.NumberType;
import mx.com.madd.datacollect.model.dataTypes.OptionType;
import mx.com.madd.datacollect.model.dataTypes.QrType;
import mx.com.madd.datacollect.model.dataTypes.SignType;
import mx.com.madd.datacollect.model.dataTypes.TextType;
import mx.com.madd.datacollect.utility.EventListener;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.HttpPost;
import mx.com.madd.datacollect.utility.OnEventListener;
import mx.com.madd.datacollect.utility.ToolBox;
//

public class SendFormsActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    SendFormsAdapter mAdapter;
    Context context;

    List<TableSavedForm> selectedForms;
    List<TableSavedForm> savedForms;

    String url_mars;
    List<TextType> alltext;
    List<ImageType> allimage;
    List<NumberType> allnumber;
    List<OptionType> alloption;
    List<DateType> alldate;
    List<DateTimeType> alldatetime;
    List<SignType> allsign;
    List<LocationType> alllocation;
    List<QrType> allqr;

    int position;
    int index;
    String data;
    String session;

    //List<String> selectedIds;
    String urlsesion;
    String urlsesiontexto;
    String urlsesionimagen;
    String urlsesionnumero;
    String urlsesionopcion;
    String urlsesionfecha;
    String urlsesionhorafecha;
    String urlsesionfirma;
    String urlsesionubicacion;
    String urlsesionqr;
    String urlsesionfinal;
    String userP;
    List<TableUser> user;
    List<TableConfiguration> configDB;
    ProgressDialog dialogpro;
    int counter;
    String jobID;
    int[] data_process = new int[11];
    int fw,fx,fn,fo,fd,ft,fl,fq,fi,fs;
    Boolean send_error;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_forms);


        recyclerView = (RecyclerView) findViewById(R.id.edit_recycler);
        GridLayoutManager mGrid = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mGrid);
        recyclerView.setHasFixedSize(true);
        //selectedIds = new ArrayList<>();

        savedForms = TableSavedForm.listAll(TableSavedForm.class);
        List<TableSavedForm> finishedForms=new ArrayList<>();
        for(int index = 0; index < savedForms.size(); index ++){
            if(savedForms.get(index).getFinished()){
                if(!savedForms.get(index).getSended()){
                    finishedForms.add(savedForms.get(index));
                }
                savedForms.get(index).setSelected(false);

            }
        }

        mAdapter = new SendFormsAdapter(this,finishedForms);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        context=getApplicationContext();

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallbackItemTouchHelper);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        configDB = TableConfiguration.listAll(TableConfiguration.class);
        userP = "";
        if(configDB.isEmpty()){
            userP = "https://api4sun.dudewhereismy.com.mx";
            String formP = "https://api4mars.dudewhereismy.com.mx";
            url_mars = formP;
            String inventoryP = "";   //"https://venus.dudewhereismy.com.mx/";
            String ticketP = "";      //https://venus.dudewhereismy.com.mx/";
            String inventoryN = "";
            TableConfiguration newConfig = new TableConfiguration(userP,formP,inventoryP,ticketP,inventoryN);
            newConfig.save();
        }else{
            url_mars = configDB.get(0).getFormPlatform();
            userP = configDB.get(0).getUserPlatform();
        }

        user = TableUser.listAll(TableUser.class);

        urlsesion = url_mars + "/form/addsavedform?appID="+user.get(0).getApplication_id();
        urlsesiontexto = url_mars + "/form/addtexto?appID="+user.get(0).getApplication_id();
        urlsesionimagen = url_mars + "/form/addimagen?appID="+user.get(0).getApplication_id();
        urlsesionnumero = url_mars + "/form/addnumero?appID="+user.get(0).getApplication_id();
        urlsesionopcion = url_mars + "/form/addopcion?appID="+user.get(0).getApplication_id();
        urlsesionfecha = url_mars + "/form/addfecha?appID="+user.get(0).getApplication_id();
        urlsesionhorafecha = url_mars + "/form/addhorafecha?appID="+user.get(0).getApplication_id();
        urlsesionfirma = url_mars + "/form/addfirma?appID="+user.get(0).getApplication_id();
        urlsesionubicacion = url_mars + "/form/addubicacion?appID="+user.get(0).getApplication_id();
        urlsesionqr = url_mars + "/form/addqr?appID="+user.get(0).getApplication_id();
        urlsesionfinal = url_mars + "/form/finalanswer?appID="+user.get(0).getApplication_id();

/*        recyclerView.addOnItemTouchListener(new SendFormsTouchListener(getApplicationContext(), new SendFormsTouchListener.OnItemTouchListener() {
            @Override
            public void onItemClick(View view, int position) {
                TextView id = (TextView)view.findViewById(R.id.savedFormId);
                String data = id.getText().toString();
                Log.i("TOCADO",data);

                if(!selectedIds.contains(data)){
                    selectedIds.add(data);
                    Log.i("TOCADO","AGREGADO");
                }else{
                    selectedIds.remove(data);
                    Log.i("TOCADO","REMOVIDO");
                }
                Log.i("TOCADO",selectedIds.toString());
                List<TableSavedForm> tableFormsList = TableSavedForm.find(TableSavedForm.class, "idsavedform = ?",data);
            }

        }));*/


    }

    ItemTouchHelper.SimpleCallback simpleCallbackItemTouchHelper = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN ,ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT){

        @Override
        public boolean onMove(@androidx.annotation.NonNull RecyclerView recyclerView, @androidx.annotation.NonNull RecyclerView.ViewHolder viewHolder, @androidx.annotation.NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@androidx.annotation.NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            position = viewHolder.getAdapterPosition();
            View view = viewHolder.itemView;
            TextView id = view.findViewById(R.id.savedFormId);
            data = (String) id.getText();
            List<TableSavedForm> tableFormsList = TableSavedForm.find(TableSavedForm.class, "idsavedform = ?",data);
            String nameForm = tableFormsList.get(0).getName_form();
            if (direction == ItemTouchHelper.LEFT){
                String message = "¿Seguro que deseas borrar el formulario: '"+nameForm+"'?";
                AlertDialog dialog=new AlertDialog.Builder(SendFormsActivity.this)
                        .setTitle("Borrar")
                        .setMessage(message)
                        .setIcon(R.drawable.borrarnegro)
                        .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setNeutralButton("Borrar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                List<TableSavedForm> tableFormsList = TableSavedForm.find(TableSavedForm.class, "idsavedform = ?",data);
                                tableFormsList.get(0).delete();
                                recreate();
                            }
                        })
                        .setCancelable(false)
                        .create();
                dialog.show();
            }

            if (direction == ItemTouchHelper.RIGHT){
                String message = "¿Seguro que deseas editar el formulario: '"+nameForm+"'?";
                AlertDialog dialog=new AlertDialog.Builder(SendFormsActivity.this)
                        .setTitle("Editar")
                        .setMessage(message)
                        .setIcon(R.drawable.editargris)
                        .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setNeutralButton("Editar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                List<TableSavedForm> tableFormsList = TableSavedForm.find(TableSavedForm.class, "idsavedform = ?",data);
                                int id_form = tableFormsList.get(0).getId_form();
                                int version = tableFormsList.get(0).getVersion();
                                jobID = tableFormsList.get(0).getJobID();
                                int pointer = position;
                                String nameForm = tableFormsList.get(0).getName_form();
                                Intent myIntent = new Intent(SendFormsActivity.this, ViewPagerActivity.class);
                                myIntent.putExtra("id_form",id_form);
                                myIntent.putExtra("version",version);
                                myIntent.putExtra("pointer",pointer);
                                myIntent.putExtra("nameForm",nameForm);
                                myIntent.putExtra("editFormId",data);
                                myIntent.putExtra("jobID",jobID);
                                startActivity(myIntent);
                                //Log.e("lkyo","ERA AQUI");
                                //finish();
                            }
                        })
                        .setCancelable(false)
                        .create();
                dialog.show();
            }

            mAdapter.notifyDataSetChanged();
        }

        //La magia del dibujo
        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            Paint p = new Paint();
            Bitmap icon;
            if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                View itemView = viewHolder.itemView;
                float height = (float) itemView.getBottom() - (float) itemView.getTop();
                float width = height / 3;

                if(dX > 0){  //DERECHA
                    p.setColor(Color.parseColor("#040404"));
                    RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom()); //PINTAR RECTANGULO
                    c.drawRect(background,p); //DIBUJAR RECTANGULO
                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.editaricono);
                    //RectF icon_dest = new RectF((float) itemView.getLeft() + width ,(float) itemView.getTop() + width,(float) itemView.getLeft()+ 2*width,(float)itemView.getBottom() - width);
                    //c.drawBitmap(icon,null,icon_dest,p);
                } else { //IZQUIERDA
                    p.setColor(Color.parseColor("#D01020"));//b31302
                    RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                    c.drawRect(background,p);
                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.borraricono);
                    RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                    c.drawBitmap(icon,null,icon_dest,p);

                }
            }
            if(isCurrentlyActive == false){
                c.drawColor(Color.TRANSPARENT);
            }
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        repinta();
    }

    @Override
    protected void onPause() {
        super.onPause();
        repinta();
    }

    private void repinta(){
        recyclerView = (RecyclerView) findViewById(R.id.edit_recycler);
        GridLayoutManager mGrid = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mGrid);
        recyclerView.setHasFixedSize(true);
        //selectedIds = new ArrayList<>();

        savedForms = TableSavedForm.listAll(TableSavedForm.class);
        List<TableSavedForm> finishedForms=new ArrayList<>();
        for(int index = 0; index < savedForms.size(); index ++){
            if(savedForms.get(index).getFinished()){
                if(!savedForms.get(index).getSended()){
                    finishedForms.add(savedForms.get(index));
                }
                savedForms.get(index).setSelected(false);

            }
        }

        mAdapter = new SendFormsAdapter(this,finishedForms);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.send, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }

        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("INFO","Send Pressed");
        int id = item.getItemId();
        if (id == R.id.send) {
            Boolean network = haveNetworkConnection(SendFormsActivity.this);
            if(network) {
                Log.d("INFO","Send Pressed");
                selectedForms=new ArrayList<>();
                for(int index = 0; index < savedForms.size(); index ++){
                    if(savedForms.get(index).getFinished()){
                        if(!savedForms.get(index).getSended()){
                            if(savedForms.get(index).getSelected()){
                                selectedForms.add(savedForms.get(index));
                                Log.d("INFO","selectedForms:"+index);
                            }
                        }
                    }
                }
                if(Integer.toString(selectedForms.size()).equals("0")){
                    Toast.makeText(SendFormsActivity.this,"No hay formularios seleccionados!",Toast.LENGTH_LONG).show();
                }else {

                    List<TableConfiguration> config = TableConfiguration.listAll(TableConfiguration.class);
                    String url = config.get(0).getUserPlatform() + "/users/expiration";
                    String postData = "";
                    Log.d("INFO","URL selected forms:"+url);
                    List<TableUser> userSession = TableUser.listAll(TableUser.class);
                    session = userSession.get(0).getSession();

                    List<TableEnterprise> userEnterprise = TableEnterprise.listAll(TableEnterprise.class);
                    if (!userEnterprise.isEmpty()) {
                        if (userEnterprise.get(0).getRfc() != null && userEnterprise.get(0).getRfc() != "") {
                            url = config.get(0).getUserPlatform() + "/enterprise/expiration?appID=" + user.get(0).getApplication_id();
                            postData = "rfc=" + userEnterprise.get(0).getRfc();
                        }
                    }
                    Log.d("INFO","URL expiration:"+url);
                    HttpPost webService = new HttpPost(url, "POST", postData, session, getApplicationContext(), new EventListener<JSONObject>() {
                        @Override
                        public void onSuccess(JSONObject response) {
                            //Log.e("LKYO s",response.toString());
                            Log.d("INFO","Exito del URL");
                            if (response.has("error")) {
                                AlertDialog dialog = new AlertDialog.Builder(SendFormsActivity.this)
                                        .setTitle("Pago requerido.")
                                        .setMessage("Su cuenta tiene un adeudo, para poder enviar sus formularios hable con el administrador")
                                        .setIcon(R.drawable.info)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .setCancelable(false)
                                        .create();
                                dialog.show();
                            } else {
                                if (response.has("expired")) {
                                    Boolean expired = response.optBoolean("expired");
                                    //Log.e("LKYO",expired.toString());
                                    if (expired.equals(false)) {
                                        Log.d("INFO","antes antes de send forms");
                                        AlertDialog dialog = new AlertDialog.Builder(SendFormsActivity.this)
                                                .setTitle("Enviando")
                                                .setMessage("¿Seguro de enviar los formularios seleccionados al servidor? recuerde tener internet")
                                                .setIcon(R.drawable.info)
                                                .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                })
                                                .setNeutralButton("Enviar", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        dialogpro = ProgressDialog.show(SendFormsActivity.this, "", "Enviando. Por favor espere un momento...", true);

                                                        Thread thread = new Thread(new Runnable() {

                                                            public void run() {
                                                                Log.d("INFO","antes de send forms");
                                                                sendForms();
                                                                runOnUiThread(new Runnable() {

                                                                    @Override
                                                                    public void run() {
                                                                        //if(dialogpro.isShowing())
                                                                        //    dialogpro.dismiss();
                                                                        //finish();
                                                                        //Toast.makeText(SendFormsActivity.this,"Formularios Enviado!",Toast.LENGTH_LONG).show();
                                                                    }

                                                                });
                                                            }
                                                        });
                                                        thread.start();
                                                    }
                                                })
                                                .setCancelable(false)
                                                .create();
                                        dialog.show();
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception e) {
                            ToolBox newtool = new ToolBox();
                            newtool.sendNotification("Error Expiracion:",e.toString(),getApplicationContext());
                            //Toast.makeText(SendFormsActivity.this, "Error en el servidor", Toast.LENGTH_LONG).show();
                        }
                    });
                    webService.execute();
                    return true;
                }
            }else{
                ToolBox newtool = new ToolBox();
                newtool.sendNotification("Error","Necesita internet para enviar formularios",getApplicationContext());
                //Toast.makeText(SendFormsActivity.this,"Necesita internet para enviar formularios",Toast.LENGTH_LONG).show();
                }
        }
        return super.onOptionsItemSelected(item);
    }
    public void sendForms(){
        fw=fx=fn=fo=fd=ft=fl=fq=fi=fs=0;
        Log.d("INFO","Send Forms");
        for(int index = 0; index < 11; index ++) {
            data_process[index]=0;
        }
        savedForms = TableSavedForm.listAll(TableSavedForm.class);
        selectedForms = null;
        selectedForms=new ArrayList<>();
        for(int index = 0; index < savedForms.size(); index ++){
            if(savedForms.get(index).getFinished()){
                if(!savedForms.get(index).getSended()){
                    if(savedForms.get(index).getSelected()){
                        selectedForms.add(savedForms.get(index));
                    }
                }
            }
        }
        Log.d("INFO","Change Dialog");
        dialogpro.setTitle("Iniciando");
        List<TableUser> userSession = TableUser.listAll(TableUser.class);
        session = userSession.get(0).getSession();
        counter = 0;
        Log.d("INFO","Before WS:"+selectedForms.size());
        for(index = 0; index < selectedForms.size(); index++){
            fx=fn=fo=fd=ft=fl=fq=fi=fs=0;
            Log.d("INFO",""+fx+":"+fn+":"+fo+":"+fd+":"+ft+":"+fl+":"+fq+":"+fi+":"+fs);
            /*final List<TableSavedForm> tableFormsList = TableSavedForm.find(TableSavedForm.class, "idsavedform = ?",Integer.toString(selectedForms.get(0).getId_savedform()));
            Log.i("lkyo tableFormsList:",tableFormsList.toString());
*/
            String postData = "nombre_form="+selectedForms.get(index).getName_form()+"&fechacreacion="+selectedForms.get(index).getCreated()+"&id_form="+Integer.toString(selectedForms.get(index).getId_form())+"&version="+Integer.toString(selectedForms.get(index).getVersion())+
                    "&item="+Integer.toString(index)+"&user="+selectedForms.get(index).getUser()+"&appid="+Integer.toString(user.get(0).getApplication_id())+"&latitude="+selectedForms.get(index).getLatitude()+"&longitud="+selectedForms.get(index).getLongitude()+
                    "&altitud="+selectedForms.get(index).getAltitude()+"&internalid="+user.get(0).getInternal_id()+"&terminado="+Boolean.toString(selectedForms.get(index).getFinished())+"&jobID="+selectedForms.get(index).getJobID();
            Log.d("INFO","Send WS");
            HttpPost webService = new HttpPost(urlsesion, "POST", postData, session, getApplicationContext(), new EventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    fw=1;
                    Log.d("INFO","Success send WS");
                }

                @Override
                public void onFailure(Exception e) {
                    fw=2;
                    Log.d("INFO","Error send WS");
                    //Toast.makeText(SendFormsActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                    ToolBox newtool = new ToolBox();
                    newtool.sendNotification("WS:"+selectedForms.get(index).getName_form(),e.toString(),getApplicationContext());
                }
            });
            webService.execute();
            send_error=false;
            while (fw == 0) {
                Log.d("INFO","======ESPERANDO a TERMINA =========");
            }
            alltext = TextType.find(TextType.class,"idsavedform = ?",String.valueOf(selectedForms.get(index).getId_savedform()));
            Log.d("INFO","Before Dat a Text:"+alltext.size());
            for(int item = 0; item<alltext.size();item++){
                String postTextType = "nombre_form="+selectedForms.get(index).getName_form()+"&fechacreacion="+selectedForms.get(index).getCreated()+"&version="+Integer.toString(selectedForms.get(index).getVersion())+
                        "&terminado="+Boolean.toString(selectedForms.get(index).getFinished())+ "&texto="+alltext.get(item).getTextType()+"&created="+alltext.get(item).getCreated()+"&id_campo="+alltext.get(item).getId_field()+"&id_form="+alltext.get(item).getId_form()+"&posiciontomo="+alltext.get(item).getPosition();
                Log.d("INFO","Success send Data Text");
                HttpPost dataText = new HttpPost(urlsesiontexto, "POST", postTextType, session,getApplicationContext(), new EventListener<JSONObject>() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        fx=1;
                        Log.d("INFO","Success send Data Text");
                    }
                    @Override
                    public void onFailure(Exception e) {
                        fx=2;
                        Log.d("INFO","Failure send Data Text");
                        //Toast.makeText(SendFormsActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                        ToolBox newtool = new ToolBox();
                        newtool.sendNotification("DT:"+selectedForms.get(index).getName_form(),e.toString(),getApplicationContext());
                    }
                });
                dataText.execute();
            }
            allnumber = NumberType.find(NumberType.class,"idsavedform = ?",String.valueOf(selectedForms.get(index).getId_savedform()));
            Log.d("INFO","Before Data Number:"+allnumber.size());
            for(int item = 0; item<allnumber.size();item++){
                String postNumberType = "nombre_form="+selectedForms.get(index).getName_form()+"&fechacreacion="+selectedForms.get(index).getCreated()+"&version="+Integer.toString(selectedForms.get(index).getVersion())+
                        "&terminado="+Boolean.toString(selectedForms.get(index).getFinished())+ "&numero="+allnumber.get(item).getNumberType()+"&created="+allnumber.get(item).getCreated()+ "&id_campo="+allnumber.get(item).getId_field()+ "&id_form="+allnumber.get(item).getId_form()+"&posiciontomo="+allnumber.get(item).getPosition();

                HttpPost dataNumber = new HttpPost(urlsesionnumero, "POST", postNumberType, session,getApplicationContext(), new EventListener<JSONObject>() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        fn=1;
                        if(response.has("error")){
                        }
                    }
                    @Override
                    public void onFailure(Exception e) {
                        fn=2;
                        //Toast.makeText(SendFormsActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                        ToolBox newtool = new ToolBox();
                        newtool.sendNotification("DN:"+selectedForms.get(index).getName_form(),e.toString(),getApplicationContext());

                    }
                });
                dataNumber.execute();

            }
            alloption = OptionType.find(OptionType.class,"idsavedform = ?",String.valueOf(selectedForms.get(index).getId_savedform()));
            Log.d("INFO","Before Data option:"+alloption.size());
            for(int item = 0; item<alloption.size();item++){
                String postOptionType = "nombre_form="+selectedForms.get(index).getName_form()+"&fechacreacion="+selectedForms.get(index).getCreated()+"&version="+Integer.toString(selectedForms.get(index).getVersion())+
                        "&terminado="+Boolean.toString(selectedForms.get(index).getFinished())+ "&opcion="+alloption.get(item).getOptionType()+"&created="+alloption.get(item).getCreated()+"&id_campo="+alloption.get(item).getId_field()+"&id_form="+alloption.get(item).getId_form()+"&posiciontomo="+alloption.get(item).getPosition();

                HttpPost dataOption = new HttpPost(urlsesionopcion, "POST", postOptionType,session, getApplicationContext(), new EventListener<JSONObject>() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        fo=1;
                        Log.d("INFO","Sucess DO");
                    }

                    @Override
                    public void onFailure(Exception e) {
                        fo=2;
                        Log.d("INFO","Failure DO");
                        //Toast.makeText(SendFormsActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                        ToolBox newtool = new ToolBox();
                        newtool.sendNotification("DO:"+selectedForms.get(index).getName_form(),e.toString(),getApplicationContext());
                    }
                });
                dataOption.execute();
            }

            alldate = DateType.find(DateType.class,"idsavedform = ?",String.valueOf(selectedForms.get(index).getId_savedform()));
            Log.d("INFO","Before Data Date:"+alldate.size());
            for(int item = 0; item<alldate.size();item++){
                String postDateType = "nombre_form="+selectedForms.get(index).getName_form()+"&fechacreacion="+selectedForms.get(index).getCreated()+"&version="+Integer.toString(selectedForms.get(index).getVersion())+
                        "&terminado="+Boolean.toString(selectedForms.get(index).getFinished())+ "&fecha="+alldate.get(item).getDateType()+"&created="+alldate.get(item).getCreated()+"&id_campo="+alldate.get(item).getId_field()+"&id_form="+alldate.get(item).getId_form()+"&posiciontomo="+alldate.get(item).getPosition();
                HttpPost dataDate = new HttpPost(urlsesionfecha, "POST", postDateType, session,getApplicationContext(), new EventListener<JSONObject>() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        fd=1;
                        Log.d("INFO","Success Data Date");
                    }
                    @Override
                    public void onFailure(Exception e) {
                        fd=2;
                        Log.d("INFO","Error Data Date");
                        //Toast.makeText(SendFormsActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                        ToolBox newtool = new ToolBox();
                        newtool.sendNotification("DD:"+selectedForms.get(index).getName_form(),e.toString(),getApplicationContext());
                    }
                });
                dataDate.execute();
            }

            alldatetime = DateTimeType.find(DateTimeType.class,"idsavedform = ?",String.valueOf(selectedForms.get(index).getId_savedform()));
            Log.d("INFO","Before Data Time:"+alldatetime.size());
            //Log.i("lkyo alldatetime",alldatetime.toString());
            for(int item = 0; item<alldatetime.size();item++){
                Log.i("lkyo selectedForms",Integer.toString(alldatetime.size()));
                String nombre_form = selectedForms.get(index).getName_form();
                String fechacreacion = selectedForms.get(index).getCreated();
                String version = Integer.toString(selectedForms.get(index).getVersion());
                String terminado = Boolean.toString(selectedForms.get(index).getFinished());
                String horafecha = alldatetime.get(item).getDateTimeType();
                String created = alldatetime.get(item).getCreated();
                String id_campo = Integer.toString(alldatetime.get(item).getId_field());
                String id_form = Integer.toString(alldatetime.get(item).getId_form());
                String posiciontomo = Integer.toString(alldatetime.get(item).getPosition());
                String postDateTimeType = "nombre_form="+nombre_form+"&fechacreacion="+fechacreacion+"&version="+version+
                        "&terminado="+terminado+ "&horafecha="+horafecha+"&created="+created+"&id_campo="+id_campo+"&id_form="+id_form+
                        "&posiciontomo="+posiciontomo;

                HttpPost dataDateTime = new HttpPost(urlsesionhorafecha, "POST", postDateTimeType, session,getApplicationContext(), new EventListener<JSONObject>() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        ft=1;
                        Log.d("INFO","Sucess Datatime");
                    }
                    @Override
                    public void onFailure(Exception e) {
                        ft=2;
                        Log.d("INFO","Error Data Time");
                        //Toast.makeText(SendFormsActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                        ToolBox newtool = new ToolBox();
                        newtool.sendNotification("DT:"+selectedForms.get(index).getName_form(),e.toString(),getApplicationContext());
                    }
                });
                dataDateTime.execute();
            }
            alllocation = LocationType.find(LocationType.class,"idsavedform = ?",String.valueOf(selectedForms.get(index).getId_savedform()));
            Log.d("INFO","Before Data Location:"+alllocation.size());
            for(int item = 0; item<alllocation.size();item++){
                String postLocationType = "nombre_form="+selectedForms.get(index).getName_form()+"&fechacreacion="+selectedForms.get(index).getCreated()+"&version="+Integer.toString(selectedForms.get(index).getVersion())+
                        "&terminado="+Boolean.toString(selectedForms.get(index).getFinished())+ "&latitud="+alllocation.get(item).getLatitude()+"&created="+alllocation.get(item).getCreated()+"&longitud="+alllocation.get(item).getLongitude()+"&altitud="+alllocation.get(item).getAltitude()+"&id_campo="+alllocation.get(item).getId_field()+"&id_form="+alllocation.get(item).getId_form()+"&posiciontomo="+alllocation.get(item).getPosition();

                HttpPost dataLocation = new HttpPost(urlsesionubicacion, "POST", postLocationType, session, getApplicationContext(), new EventListener<JSONObject>() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        fl=1;
                        Log.d("INFO","Sucess Data Location");
                    }
                    @Override
                    public void onFailure(Exception e) {
                        fl=2;
                        Log.d("INFO","Error Data Location");
                        //Toast.makeText(SendFormsActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                        ToolBox newtool = new ToolBox();
                        newtool.sendNotification("DL:"+selectedForms.get(index).getName_form(),e.toString(),getApplicationContext());
                    }
                });
                dataLocation.execute();
            }
            allqr = QrType.find(QrType.class,"idsavedform = ?",String.valueOf(selectedForms.get(index).getId_savedform()));
            Log.d("INFO","Before Data QR:"+allqr.size());
            for(int item = 0; item<allqr.size();item++){
                String postQrType = "nombre_form="+selectedForms.get(index).getName_form()+"&fechacreacion="+selectedForms.get(index).getCreated()+"&version="+Integer.toString(selectedForms.get(index).getVersion())+
                        "&terminado="+Boolean.toString(selectedForms.get(index).getFinished())+ "&texto="+allqr.get(item).getQrType()+"&created="+allqr.get(item).getCreated()+"&id_campo="+allqr.get(item).getId_field()+"&id_form="+allqr.get(item).getId_form()+"&posiciontomo="+allqr.get(item).getPosition();

                HttpPost dataQr = new HttpPost(urlsesionqr, "POST", postQrType, session, getApplicationContext(), new EventListener<JSONObject>() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        fq=1;
                        Log.d("INFO","Sucess Data QR");
                    }
                    @Override
                    public void onFailure(Exception e) {
                        fq=2;
                        Log.d("INFO","Error Data QR");
                        //Toast.makeText(SendFormsActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                        ToolBox newtool = new ToolBox();
                        newtool.sendNotification("QR:"+selectedForms.get(index).getName_form(),e.toString(),getApplicationContext());
                    }
                });
                dataQr.execute();
            }

            allimage = ImageType.find(ImageType.class,"idsavedform = ?",String.valueOf(selectedForms.get(index).getId_savedform()));
            Log.d("INFO","Before Data DI:"+allimage.size());
            for(int item = 0; item<allimage.size();item++){
                String im = allimage.get(item).getImageType();
                if(im!=null){
                    String postImageType = "";
                    try {
                        postImageType = "nombre_form="+selectedForms.get(index).getName_form()+"&fechacreacion="+selectedForms.get(index).getCreated()+"&version="+Integer.toString(selectedForms.get(index).getVersion())+
                                "&terminado="+Boolean.toString(selectedForms.get(index).getFinished())+ "&imagen="+ URLEncoder.encode(im, "UTF-8")+"&id_campo="+allimage.get(item).getId_field()+"&created="+allimage.get(item).getCreated()+"&id_form="+allimage.get(item).getId_form()+"&posiciontomo="+allimage.get(item).getPosition();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    HttpPost dataImage = new HttpPost(urlsesionimagen, "POST", postImageType, session,getApplicationContext(), new EventListener<JSONObject>() {
                        @Override
                        public void onSuccess(JSONObject response) {
                            fi=1;
                            Log.d("INFO","Success Image");
                        }
                        @Override
                        public void onFailure(Exception e) {
                            fi=2;
                            Log.d("INFO","Error Image");
                            //Toast.makeText(SendFormsActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                            ToolBox newtool = new ToolBox();
                            newtool.sendNotification("DI:"+selectedForms.get(index).getName_form(),e.toString(),getApplicationContext());

                        }
                    });
                    dataImage.execute();
                }
            }
            allsign = SignType.find(SignType.class,"idsavedform = ?",String.valueOf(selectedForms.get(index).getId_savedform()));
            Log.d("INFO","Before Data Sign:"+allsign.size());
            for(int item = 0; item<allsign.size();item++){
                String im = allsign.get(item).getSignType();
                String postDataType = "";
                if(im!=null){
                    try {
                        postDataType = "nombre_form="+selectedForms.get(index).getName_form()+"&fechacreacion="+selectedForms.get(index).getCreated()+"&version="+Integer.toString(selectedForms.get(index).getVersion())+
                                "&terminado="+Boolean.toString(selectedForms.get(index).getFinished())+ "&firma="+ URLEncoder.encode(im, "UTF-8")+"&appID="+user.get(0).getApplication_id()+"&id_campo="+allsign.get(item).getId_field()+"&created="+allsign.get(item).getCreated()+"&id_form="+allsign.get(item).getId_form()+"&posiciontomo="+allsign.get(item).getPosition();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    HttpPost dataSign = new HttpPost(urlsesionfirma, "POST", postDataType,session, getApplicationContext(), new EventListener<JSONObject>() {
                        @Override
                        public void onSuccess(JSONObject response) {
                            fs=1;
                            Log.d("INFO","Success Data Sign");
                        }
                        @Override
                        public void onFailure(Exception e) {
                            fs=2;
                            Log.d("INFO","Failure Data sign ");
                            //Toast.makeText(SendFormsActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                            ToolBox newtool = new ToolBox();
                            newtool.sendNotification("DS:"+selectedForms.get(index).getName_form(),e.toString(),getApplicationContext());
                        }
                    });
                    dataSign.execute();
                }
            }


            if (fw==2){
                send_error=true;
                Log.d("INFO","Error");
            }
            if (alltext.size()>0){
                while (fx == 0) {
                    Log.d("INFO","esperando");
                    // Wait until finish
                }
                if (fx==2){
                    send_error=true;
                    Log.d("INFO","Error");
                }
            }
            if (allnumber.size()>0){
                while (fn == 0) {
                    Log.d("INFO","esperando");
                    // Wait until finish
                }
                if (fn==2){
                    send_error=true;
                    Log.d("INFO","Error");
                }
            }
            if (alloption.size()>0){
                while (fo == 0) {
                    Log.d("INFO","esperando");
                    // Wait until finish
                }
                if (fo==2){
                    send_error=true;
                    Log.d("INFO","Error");
                }
            }
            if (alldate.size()>0){
                while (fd == 0) {
                    Log.d("INFO","esperando");
                    // Wait until finish
                }
                if (fd==2){
                    send_error=true;
                    Log.d("INFO","Error");
                }
            }
            if (alldatetime.size()>0){
                while (ft == 0) {
                    Log.d("INFO","esperando");
                    // Wait until finish
                }
                if (ft==2){
                    send_error=true;
                    Log.d("INFO","Error");
                }
            }
            if (alllocation.size()>0){
                while (fl == 0) {
                    Log.d("INFO","esperando");
                    // Wait until finish
                }
                if (fl==2){
                    send_error=true;
                    Log.d("INFO","Error");
                }
            }
            if (allqr.size()>0){
                while (fq == 0) {
                    Log.d("INFO","esperando");
                    // Wait until finish
                }
                if (fq==2){
                    send_error=true;
                    Log.d("INFO","Error");
                }
            }
            if (allimage.size()>0){
                while (fi == 0) {
                    Log.d("INFO","esperando");
                    // Wait until finish
                }
                if (fi==2){
                    send_error=true;
                    Log.d("INFO","Error");
                }
            }
            if (allsign.size()>0){
                while (fs == 0) {
                    Log.d("INFO","esperando");
                    // Wait until finish
                }
                if (fs==2){
                    send_error=true;
                    Log.d("INFO","Error");
                }
            }
            Log.d("INFO","Before Final Requests");

            String postDataType = "nombre_form="+selectedForms.get(index).getName_form()+"&fechacreacion="+selectedForms.get(index).getCreated()+"&id_form="+Integer.toString(selectedForms.get(index).getId_form())+"&version="+Integer.toString(selectedForms.get(index).getVersion())+
                    "&terminado="+Boolean.toString(selectedForms.get(index).getFinished())+"&appID="+user.get(0).getApplication_id()+"&store_name="+configDB.get(0).getInventory_name();
            Log.d("INFO",""+fx+":"+fn+":"+fo+":"+fd+":"+ft+":"+fl+":"+fq+":"+fi+":"+fs);
            HttpPost finalrequest = new HttpPost(urlsesionfinal, "POST", postDataType, session, getApplicationContext(), new EventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    Log.d("INFO","Sucess Final Response");
                    if(response.has("error")){
                        Integer savedform = response.optInt("savedform");
                        Log.i("FOLIO:: ",Integer.toString(savedform));
                        runOnUiThread(new Runnable(){
                            @Override
                            public void run() {
                                if (counter == selectedForms.size()-1){
                                    if(dialogpro.isShowing())
                                        dialogpro.dismiss();
                                    Log.e("lkyo","FINAL");
                                    finish();
                                }
                                counter++;


                            }

                        });
                    }
                }
                @Override
                public void onFailure(Exception e) {
                    if(dialogpro.isShowing())
                        dialogpro.dismiss();
                    Log.d("INFO","Error Final Response");
                    //Toast.makeText(SendFormsActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                    ToolBox newtool = new ToolBox();
                    newtool.sendNotification("E:"+selectedForms.get(index).getName_form(),e.toString(),getApplicationContext());
                    finish();
                }
            });
            finalrequest.execute();
            Log.d("INFO","Before Close Forms");
            if (send_error){
                ToolBox newtool = new ToolBox();
                newtool.sendNotification("Error de envio:","",getApplicationContext());
            } else {
                selectedForms.get(index).setSended(true);
                selectedForms.get(index).setSelected(false);
                selectedForms.get(index).save();
            }
        }
    }

    private boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        Log.d("INFO","Have networkL:"+haveConnectedWifi+haveConnectedMobile);
        return haveConnectedWifi || haveConnectedMobile;

    }

}
