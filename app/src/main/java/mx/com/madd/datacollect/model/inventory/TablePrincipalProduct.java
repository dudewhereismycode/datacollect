package mx.com.madd.datacollect.model.inventory;

import com.orm.SugarRecord;

public class TablePrincipalProduct extends SugarRecord {

    int idprincipal,idproduct;
    String productName,series1,series2;

    public TablePrincipalProduct() {
    }

    public TablePrincipalProduct(int idprincipal, int idproduct, String productName, String series1, String series2) {
        this.idprincipal = idprincipal;
        this.idproduct = idproduct;
        this.productName = productName;
        this.series1 = series1;
        this.series2 = series2;
    }

    public int getIdprincipal() {
        return idprincipal;
    }

    public void setIdprincipal(int idprincipal) {
        this.idprincipal = idprincipal;
    }

    public int getIdproduct() {
        return idproduct;
    }

    public void setIdproduct(int idproduct) {
        this.idproduct = idproduct;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSeries1() {
        return series1;
    }

    public void setSeries1(String series1) {
        this.series1 = series1;
    }

    public String getSeries2() {
        return series2;
    }

    public void setSeries2(String series2) {
        this.series2 = series2;
    }


}
