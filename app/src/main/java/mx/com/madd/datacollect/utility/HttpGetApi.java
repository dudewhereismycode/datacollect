package mx.com.madd.datacollect.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class HttpGetApi extends AsyncTask<Void, Void, String> {
    private OnEventListener<JSONObject> mCallBack;
    Context mContext;
    public String urldata;
    public Exception mException;
    String authorization;
    public static final String MY_PREFS_NAME = "MyPrefsFile";

    public HttpGetApi(String urlparam,String auth,Context context, OnEventListener<JSONObject> callback) {
        mCallBack = callback;
        mContext = context;
        urldata=urlparam;
        authorization = auth;
    }
    @Override
    protected String doInBackground(Void... params) {
        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        Log.d("INFO","Executing in background<<<<<<<<<<");
        // Will contain the raw JSON response as a string.
        String JsonStr = "";
        mException=new Exception("Http unknown Error");
        if(haveNetworkConnection(mContext)){
            try {
                Log.d("INFO","HTTP GET API URL:"+urldata);
                Log.d("INFO","HTTP sp_api_token:"+authorization);
                URL url = new URL(urldata);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setConnectTimeout(10000); //set timeout to 5 seconds
                urlConnection.setRequestProperty ("Authorization", authorization);
                urlConnection.connect();
                int code = urlConnection.getResponseCode();
                Log.d("INFO","code:"+code);
                if (code!=200){
                    mException=new Exception("Http Error:"+code);
                    return "";
                }
                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return "";
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return "";
                }
                JsonStr = buffer.toString();

                return JsonStr;
            } catch (Exception e) {
                Log.d("INFO", "Sin conexion a internet", e);
                // If the code didn't successfully get the data, there's no point in attemping
                // to parse it.
                mException = e;
                return "";
            } finally{
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        mException = e;
                        Log.e("PlaceholderFragment", "Error closing stream", e);
                    }
                }
            }
        }
        else {
            mException=new Exception("No Internet!");
            return "";
        }
    }

    @Override
    protected void onPostExecute(String response)
    {
        super.onPostExecute(response);
        if (response.equals("")){
            mCallBack.onFailure(mException);
        } else {
            try {
                JSONObject obj = new JSONObject(response);
                mCallBack.onSuccess(obj);
            } catch (Throwable t) {
                Log.e("My App", "Could not parse malformed JSON:" );
                mCallBack.onFailure(mException);
            }

        }
        /*
        Log.d("INFO","Post execution");
        if (mCallBack != null)
        {
            Log.d("INFO","mcallback <> null");
            if (mException == null)
            {
                JSONObject obj=null;
                JSONObject errorjson=null;
                Log.d("INFO","mException = null");
                try {
                    String errorstring = "{\"Error\":\"Could not parse malformed JSON\"}";
                    errorjson = new JSONObject(errorstring);
                }
                catch(Throwable t)
                {
                    //Log.d("INFO", "Could not parse malformed ERORR JSON");
                }
                try {
                    obj = new JSONObject(response);
                }
                catch (Throwable t) {
                    //Log.d("INFO", "Could not parse malformed JSON:" );
                    mCallBack.onSuccess(errorjson);
                }
                //Log.d("INFO", "mcallback on sucess obj" );
                mCallBack.onSuccess(obj);
            }
            else
            {
                Log.d("INFO", "Bottom");
                //mException=new Exception("Error con el server");
                mCallBack.onFailure(mException);
            }
        }
        else
        {
            Log.d("INFO","mcallback failure");
            mCallBack.onFailure(mException);
        }
         */
    }
    private boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}

