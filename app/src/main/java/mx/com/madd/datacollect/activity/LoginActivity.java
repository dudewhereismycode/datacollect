package mx.com.madd.datacollect.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.profile.CreateAccountActivity;
import mx.com.madd.datacollect.activity.profile.RecoverAccountActivity;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.HttpGetApi;
import mx.com.madd.datacollect.utility.HttpPost;
import mx.com.madd.datacollect.utility.OnEventListener;
import mx.com.madd.datacollect.utility.EventListener;
import mx.com.madd.datacollect.utility.ToolBox;

import org.json.JSONObject;

import java.util.List;

public class LoginActivity extends AppCompatActivity {

    List<TableUser> userSession;
    List<TableConfiguration> configDB;

    Button loginButton;
    TextView createAccount;
    TextView recoverAccount;
    LinearLayout loader;

    EditText userName;
    EditText password;

    String error;
    String token;
    String update = "no";
    String user;
    String pwd;
    String authorization;
    String from;

    String url_sun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginButton = findViewById(R.id.iniciarSesion);
        createAccount = findViewById(R.id.crearCuenta);
        recoverAccount = findViewById(R.id.recuperarCuenta);
        userName = findViewById(R.id.userName);
        password = findViewById(R.id.password);
        loader = findViewById(R.id.loader);
        loader.setVisibility(View.GONE);
        error = "Bienvenido!";
        authorization = "";

        userSession = TableUser.listAll(TableUser.class);
        if (!userSession.isEmpty()){
            user = userSession.get(0).getUser();
            pwd = userSession.get(0).getPassword();
            userName.setText(user);
            password.setText(pwd);
            from = "login";
            if(error.equals("ok")){
                Intent openMainActivity = new Intent(getApplicationContext(),MainActivity.class);
                openMainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(openMainActivity);
            }
        }

        configDB = TableConfiguration.listAll(TableConfiguration.class);

        if(configDB.isEmpty()){
            String userP = "https://api4sun.dudewhereismy.com.mx";
            String formP = "https://api4mars.dudewhereismy.com.mx";
            String inventoryP = "";   //"https://venus.dudewhereismy.com.mx/";
            String ticketP = "";      //https://venus.dudewhereismy.com.mx/";
            String inventoryN = "";
            TableConfiguration newConfig = new TableConfiguration(userP,formP,inventoryP,ticketP,inventoryN);
            newConfig.save();
            url_sun = userP;
        }else{
            url_sun = configDB.get(0).getUserPlatform();
        }
        getToken();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.config, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.configs) {
            Intent openConfigurationActivity = new Intent(getApplicationContext(), ConfigurationActivity.class);
            startActivity(openConfigurationActivity);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void authenticateUser(){
        loginButton.setVisibility(View.GONE);
        createAccount.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);

        if(haveNetworkConnection(LoginActivity.this)){
            Log.d("INFO","Login");
            String url = url_sun + "/login/app";
            Log.d("INFO","Login URL:"+url);
            String postData = "username="+user+"&password="+pwd+"&token="+token+"&update="+update;
            Log.d("INFO","Login postData"+postData);
            HttpPost webService = new HttpPost(url, "POST", postData,"", getApplicationContext(), new EventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response) {
                    Log.d("INFO","Login Success");
                    if(!response.equals(null)){
                        Log.d("INFO","Login (1)");
                        if(response.has("error")) {
                            Log.d("INFO","Login (2)");
                            if (!response.optString("error").equals("ok")) {
                                Log.d("INFO","Login (3)");
                                if(response.optString("error").equals("Usuario Registrado con otro mobil")){
                                    Log.d("INFO","Login (4)");
                                    String message = "Usuario Registrado con otro celular. Contacte al administrador para liberar la licencia y poder iniciar sesión desde este celular.";
                                    AlertDialog dialog=new AlertDialog.Builder(LoginActivity.this)
                                            .setTitle("Atención.")
                                            .setMessage(message)
                                            .setIcon(R.drawable.alert)
                                            .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            })
                                            /*.setNeutralButton("Olvidar", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    update = "yes";
                                                    dialog.dismiss();
                                                    authenticateUser();

                                                }
                                            })*/
                                            .setCancelable(false)
                                            .create();
                                    dialog.show();

                                }else{
                                    Log.d("INFO","Login (5)");
                                    Toast.makeText(LoginActivity.this, response.optString("error"), Toast.LENGTH_LONG).show();
                                }
                            }else{

                                authorization = response.optString("Authorized");
                                String url2 = url_sun + "/users/profile";
                                String postData2 = "";
                                Log.d("INFO","Login (6):"+url2);
                                Log.d("INFO","Login (6):"+authorization);
                                HttpGetApi webService2 = new HttpGetApi(url2, authorization, getApplicationContext(), new OnEventListener<JSONObject>() {
                                    @Override
                                    public void onSuccess(JSONObject response) {
                                        Log.d("INFO","Login (6) Success:"+response.toString());
                                        if (response.has("error")) {
                                            Log.d("INFO","Login (6) has error:"+response.toString());
                                            Toast.makeText(LoginActivity.this,response.optString("error"),Toast.LENGTH_LONG).show();
                                        } else {
                                            if (userSession.isEmpty()) {
                                                Log.d("INFO","Login (7)");
                                                TableUser newUser = new TableUser();
                                                newUser.setUser(user);
                                                newUser.setPassword(pwd);
                                                newUser.setSession(authorization);
                                                newUser.setEmail(response.optString("email"));
                                                newUser.setName(response.optString("name"));
                                                newUser.setInternal_id(Integer.parseInt(response.optString("internal_id")));
                                                newUser.setApplication_id(Integer.parseInt(response.optString("application_id")));
                                                newUser.save();
                                            } else {
                                                Log.d("INFO","Login (8)");
                                                userSession.get(0).setInternal_id(Integer.parseInt(response.optString("internal_id")));
                                                userSession.get(0).setApplication_id(Integer.parseInt(response.optString("application_id")));
                                                userSession.get(0).setSession(authorization);
                                                userSession.get(0).save();
                                            }
                                            Log.d("INFO","Login (9)");
                                            Intent openMainActivity = new Intent(getApplicationContext(), MainActivity.class);
                                            startActivity(openMainActivity);
                                            finish();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Exception e) {
                                        Log.d("INFO","Login (10)");
                                        ToolBox newtool = new ToolBox();
                                        newtool.sendNotification("Error:",e.toString(),getApplicationContext());
                                        Toast.makeText(LoginActivity.this, "Error en el servidor", Toast.LENGTH_LONG).show();
                                    }
                                });
                                webService2.execute();
                            }
                        }
                    }else{
                        Toast.makeText(LoginActivity.this,"Datos invalidos",Toast.LENGTH_LONG).show();
                    }
                    Log.d("INFO","Login (11)");
                    loginButton.setVisibility(View.VISIBLE);
                    createAccount.setVisibility(View.VISIBLE);
                    loader.setVisibility(View.GONE);
                }
                @Override
                public void onFailure(Exception e) {
                    Log.d("INFO","Login");
                    loginButton.setVisibility(View.VISIBLE);
                    createAccount.setVisibility(View.VISIBLE);
                    loader.setVisibility(View.GONE);
                    ToolBox newtool = new ToolBox();
                    newtool.sendNotification("Error",e.toString(),getApplicationContext());
                    Toast.makeText(LoginActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                }
            });
            webService.execute();
        }else{
            Log.d("INFO","Login (12)");
            user = userName.getText().toString();
            pwd = password.getText().toString();
            String [] whereArgs = new String[3];
            whereArgs[0] = user;
            whereArgs[1] = pwd;
            whereArgs[2] = "ok";
            List<TableUser> userf = TableUser.find(TableUser.class,"user = ? and password = ? and session = ?",whereArgs);
            if(!userf.isEmpty()){
                Log.d("INFO","Login (13)");
                Toast.makeText(LoginActivity.this,"Sin internet no se puede validar su usuario",Toast.LENGTH_LONG).show();
                finish();
                Intent openMainActivity = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(openMainActivity);
            }else{
                Log.d("INFO","Login (14)");
                TableUser newUser = new TableUser();
                newUser.setUser(user);
                newUser.setPassword(pwd);
                newUser.setSession("notok");
                newUser.setEmail("sin validar");
                newUser.setName("sin validar");
                newUser.setInternal_id(0);
                newUser.setApplication_id(0);
                newUser.save();
                loginButton.setVisibility(View.VISIBLE);
                createAccount.setVisibility(View.VISIBLE);
                loader.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this,"Sin internet no se puede validar su usuario",Toast.LENGTH_LONG).show();
                finish();
                Intent openMainActivity = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(openMainActivity);
            }
        }

    }

    public void loginButtonAction(View v){
        user = userName.getText().toString();
        pwd = password.getText().toString();
        authenticateUser();
    }

    public void dwimAgreement(View v){
        Intent openCreateAccount = new Intent(getApplicationContext(), DWIMActivity.class);
        startActivity(openCreateAccount);
    }

    public void recoverAccountButtonAction(View v){
        Intent openRecoverAccount = new Intent(getApplicationContext(), RecoverAccountActivity.class);
        startActivity(openRecoverAccount);
    }

    public void createAccountButtonAction(View v){
        Intent openCreateAccount = new Intent(getApplicationContext(), CreateAccountActivity.class);
        startActivity(openCreateAccount);
    }

    private boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public void getToken(){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("w", "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        token = task.getResult().getToken();
                        // Log and toast
                        if(user!=null && pwd!= null){
                            authenticateUser();
                        }

                    }
                });
    }
}