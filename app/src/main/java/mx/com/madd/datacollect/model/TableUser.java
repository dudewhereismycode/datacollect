package mx.com.madd.datacollect.model;

import com.orm.SugarRecord;

public class TableUser extends SugarRecord{

    String name, phone, email, user, password, session,logo;
    int application_id, internal_id;

    public TableUser() {

    }

    public TableUser(String name, String phone, String email, String user, String password, String session, int application_id, int internal_id) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.user = user;
        this.password = password;
        this.session = session;
        this.application_id = application_id;
        this.internal_id = internal_id;
    }

    public String getPassword() {
        return password;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public void setApplication_id(int application_id) {
        this.application_id = application_id;
    }

    public void setInternal_id(int internal_id) {
        this.internal_id = internal_id;
    }

    public int getApplication_id() {
        return application_id;
    }

    public int getInternal_id() {
        return internal_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
