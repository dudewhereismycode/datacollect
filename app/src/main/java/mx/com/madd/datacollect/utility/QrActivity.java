package mx.com.madd.datacollect.utility;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.utility.barcode.BarcodeCaptureActivity;

public class QrActivity extends Activity{
    private CompoundButton autoFocus;
    private CompoundButton useFlash;
    private TextView statusMessage;
    private TextView barcodeValue;

    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";

    public String nombre, qrlector_display;
    public String flag;

    Integer id_form, pointer,laposicion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        Intent resultIntent = getIntent();
        id_form = Integer.parseInt(resultIntent.getStringExtra("form"));
        pointer = Integer.parseInt(resultIntent.getStringExtra("pointer"));
        qrlector_display = "";
        laposicion = Integer.parseInt(resultIntent.getStringExtra("posicionqr"));

        statusMessage = (TextView) findViewById(R.id.status_message);
        barcodeValue = (TextView) findViewById(R.id.barcode_value);

        autoFocus = (CompoundButton) findViewById(R.id.auto_focus);
        useFlash = (CompoundButton) findViewById(R.id.use_flash);

    }

    public void readCode(View v) {

        if (v.getId() == R.id.read_barcode) {

            Intent intent = new Intent(this, BarcodeCaptureActivity.class);
            intent.putExtra(BarcodeCaptureActivity.AutoFocus, autoFocus.isChecked());
            intent.putExtra(BarcodeCaptureActivity.UseFlash, useFlash.isChecked());

            startActivityForResult(intent, RC_BARCODE_CAPTURE);
        }

    }

    public void saveCode(View v){
        Intent returnIntent = new Intent();
        returnIntent.putExtra("qrlector_display",qrlector_display);
        returnIntent.putExtra("laposicion",Integer.toString(laposicion));
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    //statusMessage.setText(R.string.barcode_success);
                    //barcodeValue.setText(barcode.displayValue);
                    if(String.valueOf(flag).equals("1")) {
                        //barcode_value
                        barcodeValue.setText(barcode.displayValue);
                        qrlector_display = barcode.displayValue;
                    }else{
                        barcodeValue.setText(barcode.displayValue);
                        qrlector_display = barcode.displayValue;
                    }
                }
            }


        }
    }
}