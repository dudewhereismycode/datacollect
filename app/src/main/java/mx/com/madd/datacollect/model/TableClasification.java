package mx.com.madd.datacollect.model;

import com.orm.SugarRecord;

public class TableClasification  extends SugarRecord {

    String clasification;

    public TableClasification() {
    }

    public TableClasification(String clasification) {
        this.clasification = clasification;
    }

    public String getClasification() {
        return clasification;
    }

    public void setClasification(String clasification) {
        this.clasification = clasification;
    }

}
