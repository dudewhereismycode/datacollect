package mx.com.madd.datacollect.model.dataTypes;

import com.orm.SugarRecord;

public class NumberType extends SugarRecord {

    int id_numbertype, numbertype, id_form,application_id,internal_id,version,id_savedform, position, id_field, id_originalform;
    String created;

    public NumberType() {
    }

    public NumberType(int id_numbertype, int id_form, int application_id, int internal_id, int version, int id_savedform, int position, int id_field, int id_originalform, int numbertype, String created) {
        this.id_numbertype = id_numbertype;
        this.id_form = id_form;
        this.application_id = application_id;
        this.internal_id = internal_id;
        this.version = version;
        this.id_savedform = id_savedform;
        this.position = position;
        this.id_field = id_field;
        this.id_originalform = id_originalform;
        this.numbertype = numbertype;
        this.created = created;
    }

    public int getId_numberType() {
        return id_numbertype;
    }

    public void setId_numberType(int id_numbertype) {
        this.id_numbertype = id_numbertype;
    }

    public int getNumberType() {
        return numbertype;
    }

    public void setNumbeType(int numbertype) {
        this.numbertype = numbertype;
    }

    public int getId_form() {
        return id_form;
    }

    public void setId_form(int id_form) {
        this.id_form = id_form;
    }

    public int getApplication_id() {
        return application_id;
    }

    public void setApplication_id(int application_id) {
        this.application_id = application_id;
    }

    public int getInternal_id() {
        return internal_id;
    }

    public void setInternal_id(int internal_id) {
        this.internal_id = internal_id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getId_savedForm() {
        return id_savedform;
    }

    public void setId_savedForm(int id_savedform) {
        this.id_savedform = id_savedform;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getId_field() {
        return id_field;
    }

    public void setId_field(int id_field) {
        this.id_field = id_field;
    }

    public int getId_originalForm() {
        return id_originalform;
    }

    public void setId_originalForm(int id_originalform) {
        this.id_originalform = id_originalform;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
