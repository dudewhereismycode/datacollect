package mx.com.madd.datacollect.activity.inventory;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.EditText;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.OnEventListener;

public class ReadFormActivity extends AppCompatActivity {

    TextView bgetforms;
    EditText contentFormText;
    String alldata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_form);
        //TextView texttipo = (TextView) lastview.findViewById(R.id.texttipoid);
        final Intent myIntent = getIntent();
        int position=Integer.parseInt(myIntent.getStringExtra("position"));

        bgetforms=(TextView)findViewById(R.id.getFormName);
        contentFormText=(EditText)findViewById(R.id.contentFormText);
        contentFormText.setFocusableInTouchMode(false);
        alldata = "";

        List<TableUser> userSession = TableUser.listAll(TableUser.class);
        String session = userSession.get(0).getSession();
        final List<TableConfiguration> confieditPasswd= TableConfiguration.listAll(TableConfiguration.class);
        String urlmars = confieditPasswd.get(0).getFormPlatform()+ "/form/info?appID="+userSession.get(0).getApplication_id();
        String postData = "savedform="+Integer.toString(position);
        Http requestsession = new Http(urlmars, "POST", postData,session, getApplicationContext(), new OnEventListener<JSONObject>() {
            @Override
            public void onSuccess(JSONObject object) {
                try{
                    Object name_saved = object.get("namesavedform");

                    bgetforms.setText(name_saved.toString());
                    JSONArray listtextos = object.getJSONArray("listtextos");
                    for (int i = 0; i < listtextos.length(); i++) {
                        JSONObject oo = listtextos.getJSONObject(i);
                        Object texto = oo.get("texto");
                        Object campo = oo.get("campo");
                        alldata+= campo.toString()+":\n"+texto.toString()+'\n';
                        alldata+='\n';
                    }
                    JSONArray listqr = object.getJSONArray("listqr");
                    for (int i = 0; i < listqr.length(); i++) {
                        JSONObject oo = listqr.getJSONObject(i);
                        Object texto = oo.get("texto");
                        Object campo = oo.get("campo");
                        alldata+= campo.toString()+":\n"+texto.toString()+'\n';
                        alldata+='\n';
                    }
                    JSONArray listopciones = object.getJSONArray("listopciones");
                    for (int i = 0; i < listopciones.length(); i++) {
                        JSONObject oo = listopciones.getJSONObject(i);
                        Object texto = oo.get("opcion");
                        Object campo = oo.get("campo");
                        alldata+= campo.toString()+":\n"+texto.toString()+'\n';
                        alldata+='\n';
                    }
                    JSONArray listnumeros = object.getJSONArray("listnumeros");
                    for (int i = 0; i < listnumeros.length(); i++) {
                        JSONObject oo = listnumeros.getJSONObject(i);
                        Object texto = oo.get("numero");
                        Object campo = oo.get("campo");
                        alldata+= campo.toString()+":\n"+texto.toString()+'\n';
                        alldata+='\n';
                    }
                    JSONArray listubicacion = object.getJSONArray("listubicacion");
                    for (int i = 0; i < listubicacion.length(); i++) {
                        JSONObject oo = listubicacion.getJSONObject(i);
                        Object latitud = oo.get("latitud");
                        Object campo = oo.get("campo");
                        Object altitud = oo.get("altitud");
                        Object longitud = oo.get("longitud");
                        alldata+= campo.toString()+":\nLatitud: "+latitud.toString()+'\n';
                        alldata+= "Longitud: "+longitud.toString()+'\n';
                        alldata+= "Altitud: "+altitud.toString()+'\n';
                        alldata+='\n';
                    }

                    contentFormText.setText(alldata);
                    contentFormText.setScroller(new Scroller(ReadFormActivity.this));
                    contentFormText.setVerticalScrollBarEnabled(true);
                    contentFormText.setMovementMethod(new ScrollingMovementMethod());


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Exception e) {

            }
        });
        requestsession.execute();
    }
}
