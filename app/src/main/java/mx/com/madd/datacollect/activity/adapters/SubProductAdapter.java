package mx.com.madd.datacollect.activity.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.holders.SubProductViewHolder;
import mx.com.madd.datacollect.model.inventory.TablePrincipalProduct;
import mx.com.madd.datacollect.model.inventory.TableProduct;
import mx.com.madd.datacollect.model.inventory.TableSubProduct;

public class SubProductAdapter extends RecyclerView.Adapter<SubProductViewHolder>{
    private Context context;
    private List<TableSubProduct> subObject;

    public SubProductAdapter(Context context, List<TableSubProduct> subObject){
        this.context = context;
        this.subObject = subObject;
    }

    @Override
    public SubProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.subproduct_layout, parent, false);
        return new SubProductViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(SubProductViewHolder holder, int position) {
        final TableSubProduct currentProduct = subObject.get(position);
        String a1=currentProduct.getProductName();
        String a2=currentProduct.getSeries1();
        String a3=currentProduct.getSeries2();
        int a4=currentProduct.getTotal();
        int idp = currentProduct.getIdprincipal();
        List<TablePrincipalProduct> principal = TablePrincipalProduct.find(TablePrincipalProduct.class,"idprincipal = ?",Integer.toString(idp));
        if (!principal.toString().equals("[]")) {
            int idproduct = principal.get(0).getIdproduct();
            List<TableProduct> products = TableProduct.find(TableProduct.class,"idproduct = ?", Integer.toString(idproduct));
            String serie1 = products.get(0).getSeriesName1();
            String serie2 = products.get(0).getSeriesName2();
            holder.productName.setText(a1);
            if(!a2.equals("0")){
                holder.loteSeries1.setText(a2);
            }
            if(!a3.equals("0")){
                holder.loteSeries2.setText(a3);
            }
            if(!String.valueOf(a4).equals("0")){
                holder.quantity.setText("Cantidad: "+Integer.toString(a4));
            }

        }else{
            if(!a2.equals("0")){
                holder.loteSeries1.setText(a2);
            }else{
                holder.loteSeries1.setText("");
            }
            if(!a3.equals("0")){
                holder.loteSeries2.setText(a3);
            }else{
                holder.loteSeries2.setText("");
            }
            if(!String.valueOf(a4).equals("0")){
                holder.quantity.setText("Cantidad: "+Integer.toString(a4));
            }else{
                holder.quantity.setText("");
            }
        }


    }

    @Override
    public int getItemCount() {
        return subObject.size();
    }
}
