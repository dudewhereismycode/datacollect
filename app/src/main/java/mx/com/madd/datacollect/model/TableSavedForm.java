package mx.com.madd.datacollect.model;

import com.orm.SugarRecord;

public class TableSavedForm extends SugarRecord {
    int id_savedform,id_form,version;
    String name_form, user, created;
    Boolean sended, finished;
    Boolean selected = false;
    String latitude, longitude, altitude;
    String jobID;
    int folio;

    public TableSavedForm() {
    }

/*
    public TableSavedForm(int id_savedform, int id_form, int version, String name_form, String user, String created, Boolean sended, Boolean finished) {
        this.id_savedform = id_savedform;
        this.id_form = id_form;
        this.version = version;
        this.name_form = name_form;
        this.user = user;
        this.created = created;
        this.sended = sended;
        this.finished = finished;
    }
*/

    public TableSavedForm(int id_savedform, int id_form, int version, String name_form, String user, String created, Boolean sended, Boolean finished, String latitude, String longitude, String altitude, String jobID) {
        this.id_savedform = id_savedform;
        this.id_form = id_form;
        this.version = version;
        this.name_form = name_form;
        this.user = user;
        this.created = created;
        this.sended = sended;
        this.finished = finished;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.jobID = jobID;
    }

    public int getId_savedform() {
        return id_savedform;
    }

    public void setId_savedform(int id_savedform) {
        this.id_savedform = id_savedform;
    }

    public int getId_form() {
        return id_form;
    }

    public void setId_form(int id_form) {
        this.id_form = id_form;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getName_form() {
        return name_form;
    }

    public void setName_form(String name_form) {
        this.name_form = name_form;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Boolean getSended() {
        return sended;
    }

    public void setSended(Boolean sended) {
        this.sended = sended;
    }

    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAltitude() {
        return altitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }
}
