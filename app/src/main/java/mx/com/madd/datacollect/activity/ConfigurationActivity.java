package mx.com.madd.datacollect.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.OnEventListener;

public class ConfigurationActivity extends AppCompatActivity {

    ListView configList;
    List<TableConfiguration> configDB;
    List<TableUser> userSession;

    String userP, formP, inventoryP, ticketP,inventoryN,error;
    String[] listItems;

    EditText platform,name;
    //ProgressBar progress;
    Button save;

    View mView;
    Boolean resp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        configList = findViewById(R.id.listConfig);

        configDB = TableConfiguration.listAll(TableConfiguration.class);
        userSession = TableUser.listAll(TableUser.class);

        if(configDB.isEmpty()){
            userP = "https://api4sun.dudewhereismy.com.mx";
            formP = "https://api4mars.dudewhereismy.com.mx";
            inventoryP = "";   //"https://venus.dudewhereismy.com.mx/";
            ticketP = "";      //https://venus.dudewhereismy.com.mx/";
            inventoryN = "";
            TableConfiguration newConfig = new TableConfiguration(userP,formP,inventoryP,ticketP,inventoryN);
            newConfig.save();
        }else{
            userP = configDB.get(0).getUserPlatform();
            formP = configDB.get(0).getFormPlatform();
            inventoryP = configDB.get(0).getInventoryPlatform();
            ticketP = configDB.get(0).getTicketPlatform();
            inventoryN = configDB.get(0).getTicketPlatform();
        }

        listItems = new String[]{
          "Plataforma de Login \n"+ userP,
          "Plataforma de Formularios \n"+ formP,
          "Plataforma de Inventarios \n"+ inventoryP,
          "Vista de Formulario \n"+ ticketP,
        };

        final ArrayAdapter<String> adapterArray = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,android.R.id.text1, listItems);

        configList.setAdapter(adapterArray);

        configList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                String key = adapterView.getAdapter().getItem(position).toString();
                String[] keyArray = key.split(" ");
                final String keyP = keyArray[2];
                String[] namesArray = key.split("\n");
                String title = namesArray[0];
                String text = "";
                if(namesArray.length == 2){
                    text = namesArray[1];
                }

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(ConfigurationActivity.this);
                mBuilder.setTitle(namesArray[0]);
                mBuilder.setIcon(R.drawable.gear_black);
                if(keyP.equals("Inventarios")){
                    mView = getLayoutInflater().inflate(R.layout.dialog_inventory,null);
                    platform = mView.findViewById(R.id.savenameform);
                    name = mView.findViewById(R.id.inventaryName);

                    List<TableConfiguration> configEdit = TableConfiguration.listAll(TableConfiguration.class);
                    platform.setText(platform.getText().toString());
                    platform.setText(configEdit.get(0).getInventoryPlatform());
                    name.setText(configEdit.get(0).getInventory_name());

                }else{
                    mView = getLayoutInflater().inflate(R.layout.dialog_normal,null);
                    //TextView name = mView.findViewById(R.id.formName);
                    //name.setText(title);
                    platform = mView.findViewById(R.id.savenameform);
                    platform.setText(text);
                }


                mBuilder.setView(mView).setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        List<TableConfiguration> configEdit = TableConfiguration.listAll(TableConfiguration.class);
                        if(keyP.equals("Login")){
                            configEdit.get(0).setUserPlatform(platform.getText().toString());
                            configEdit.get(0).save();
                        }else if(keyP.equals("Formularios")){
                            configEdit.get(0).setFormPlatform(platform.getText().toString());
                            configEdit.get(0).save();
                        }else if(keyP.equals("Formulario")){
                            String texto = platform.getText().toString();
                            if(texto.equals("Todo") || texto.equals("Clasificación")){
                                configEdit.get(0).setTicketPlatform(platform.getText().toString());
                                configEdit.get(0).save();
                            }else{
                                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ConfigurationActivity.this);
                                builder.setTitle("Atención");
                                builder.setMessage(Html.fromHtml("Respuesta invalida"));
                                builder.setPositiveButton("OK", null);
                                builder.create();
                                builder.show();
                            }
                        }else if(keyP.equals("Inventarios")){
                            configEdit.get(0).setInventoryPlatform(platform.getText().toString());
                            configEdit.get(0).setInventory_name(name.getText().toString());
                            configEdit.get(0).save();
                        }
                        //recreate();

                        Toast.makeText(ConfigurationActivity.this,"Cambios Guardados",Toast.LENGTH_LONG).show();
                    }
                }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                AlertDialog dialog = mBuilder.create();
                dialog.show();
            }
        });
    }

    public void validateURL(String url, String store_name){
        if(url.length()>0){
            String postData = "user="+userSession.get(0).getUser()+"&application_id="+userSession.get(0).getApplication_id()+"&internal_id="+userSession.get(0).getInternal_id()+"&store_id="+store_name+"&appID="+userSession.get(0).getApplication_id();
            String session = userSession.get(0).getSession();
            Http webService = new Http(url, "POST", postData, session,getApplicationContext(), new OnEventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response){
                    resp = true;
                    if(response.has("error")){
                        error = response.optString("error");
                        if(error.equals("ok")){
                            if(response.has("principalproduct")){
                                try{
                                    JSONArray principalproduct = response.getJSONArray("principalproduct");
                                    for(int i = 0; i < principalproduct.length(); i++){
                                        JSONObject pProduct = principalproduct.getJSONObject(i);
                                        if(!pProduct.has("idunitary")){
                                            resp = false;
                                        }
                                        if(!pProduct.has("idproduct")){
                                            resp = false;
                                        }
                                        if(!pProduct.has("productName")){
                                            resp = false;
                                        }
                                        if(!pProduct.has("series1")){
                                            resp = false;
                                        }
                                        if(!pProduct.has("series2")){
                                            resp = false;
                                        }
                                    }
                                }catch (Exception e){
                                    resp = false;
                                }
                            }else{
                                resp = false;
                            }

                            if(response.has("products")){
                                try{
                                    JSONArray products = response.getJSONArray("products");
                                    for(int i = 0; i < products.length(); i++){
                                        JSONObject product = products.getJSONObject(i);
                                        if(!product.has("idproduct")){
                                            resp = false;
                                        }
                                        if(!product.has("productName")){
                                            resp = false;
                                        }
                                        if(!product.has("sku")){
                                            resp = false;
                                        }
                                        if(!product.has("seriesName1")){
                                            resp = false;
                                        }
                                        if(!product.has("seriesName2")){
                                            resp = false;
                                        }
                                    }
                                }catch (Exception e){
                                    resp = false;
                                }
                            }else{
                                resp = false;
                            }

                            if(response.has("subproduct")){
                                try{
                                    JSONArray subproduct = response.getJSONArray("subproduct");
                                    for(int i = 0; i < subproduct.length(); i++){
                                        JSONObject sProduct = subproduct.getJSONObject(i);
                                        if(!sProduct.has("idsub")){
                                            resp = false;
                                        }
                                        if(!sProduct.has("idprincipal")){
                                            resp = false;
                                        }
                                        if(!sProduct.has("total")){
                                            resp = false;
                                        }
                                        if(!sProduct.has("productName")){
                                            resp = false;
                                        }
                                        if(!sProduct.has("series1")){
                                            resp = false;
                                        }
                                        if(!sProduct.has("series2")){
                                            resp = false;
                                        }
                                    }
                                }catch (Exception e){
                                    resp = false;
                                }

                            }else{
                                resp = false;
                            }

                            if(!response.has("totals")){
                                resp = false;
                            }
                        }else{
                            resp = false;
                        }
                    }else{
                        resp = false;
                    }

                    if(resp){
                        List<TableConfiguration> configEdit = TableConfiguration.listAll(TableConfiguration.class);
                        configEdit.get(0).setInventoryPlatform(platform.getText().toString());
                        configEdit.get(0).setInventory_name(name.getText().toString());
                        configEdit.get(0).save();
                    }else{
                        //progress.setVisibility(View.INVISIBLE);
                        save.setVisibility(View.VISIBLE);
                        Toast.makeText(ConfigurationActivity.this,"No cumple con los requisitos el URL proporcionado",Toast.LENGTH_LONG).show();
                    }
                    recreate();
                }
                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(ConfigurationActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                }
            });
            webService.execute();
        }

    }
}
