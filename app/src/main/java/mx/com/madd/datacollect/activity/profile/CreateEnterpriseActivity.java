package mx.com.madd.datacollect.activity.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.MainActivity;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableEnterprise;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.OnEventListener;

public class CreateEnterpriseActivity extends AppCompatActivity {

    EditText enterprise,rfc,phone,email;
    String enterpriseString,rfcString,phoneString,emailString;

    LinearLayout loader;
    Button createEnterpriseButton;
    String ok, url_sun;

    List<TableUser> userSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise);
        enterprise = findViewById(R.id.enterprise);
        rfc = findViewById(R.id.enterprise_rfc);
        phone = findViewById(R.id.enterpise_phone);
        email = findViewById(R.id.enterprise_email);

        loader = findViewById(R.id.loader);
        loader.setVisibility(View.INVISIBLE);

        createEnterpriseButton = findViewById(R.id.createEnteprice);

        List<TableConfiguration> configDB = TableConfiguration.listAll(TableConfiguration.class);
        if(configDB.isEmpty()){
            String userP = "https://api4sun.dudewhereismy.com.mx";
            String formP = "https://api4mars.dudewhereismy.com.mx";
            String inventoryP = "";   //"https://venus.dudewhereismy.com.mx";
            String ticketP = "";      //https://venus.dudewhereismy.com.mx/";
            String inventoryN = "";
            TableConfiguration newConfig = new TableConfiguration(userP,formP,inventoryP,ticketP,inventoryN);
            newConfig.save();
            url_sun = userP;
        }else{
            url_sun = configDB.get(0).getUserPlatform();
        }
    }

    public void alertError(String error){
        Toast.makeText(this,error,Toast.LENGTH_LONG).show();
        createEnterpriseButton.setVisibility(View.VISIBLE);
        loader.setVisibility(View.GONE);
    }

    public void createEnterprise(View v){
        createEnterpriseButton.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);

        ok="ok";

        enterpriseString = enterprise.getText().toString();
        if(enterpriseString.length()<5){
            alertError("Nombre muy corto");
            return;
        }else if(enterpriseString.length()>255){
            alertError("Nombre muy largo");
            return;

        }

        phoneString = phone.getText().toString();
        if(phoneString.length()<10){
            alertError("Celular muy corto");
            return;
        }else if(phoneString.length()>15){
            alertError("Celular muy largo");
            return;

        }

        emailString = email.getText().toString();
        if(emailString.length()<5){
            alertError("Correo electrónico obligatorio");
            return;
        }else if(emailString.length()>200){
            alertError("Correo electrónico muy largo");
            return;

        }

        rfcString = rfc.getText().toString();
        if(rfcString.length()<11){
            alertError("RFC muy corto");
            return;
        }else if(rfcString.length()>20){
            alertError("RFC muy largo");
            return;
        }

        String url = url_sun + "/enterprise";
        userSession = TableUser.listAll(TableUser.class);
        String postData = "enterprise="+enterpriseString+"&phone="+phoneString+"&email="+emailString+"&internal_id="+userSession.get(0).getInternal_id()+"&user="+userSession.get(0).getUser()+"&rfc="+rfcString;
        //List<TableUser> userSession = TableUser.listAll(TableUser.class);
        String session = userSession.get(0).getSession();
        Http webService = new Http(url, "POST", postData,session, getApplicationContext(), new OnEventListener<JSONObject>() {
            @Override
            public void onSuccess(JSONObject response) {
                if(response.has("error")){
                    ok = response.optString("error");
                    if(ok.equals("ok")){
                        TableEnterprise.deleteAll(TableEnterprise.class);
                        String app_id = response.optString("application_id");
                        alertError("Grupo creado con exito");
                        TableEnterprise newEnterprise = new TableEnterprise(enterpriseString,rfcString,emailString,phoneString,Integer.parseInt(app_id),userSession.get(0).getInternal_id());
                        newEnterprise.save();
                        Intent openMainActivity = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(openMainActivity);

                    }else{
                        alertError(ok);
                    }
                }
                createEnterpriseButton.setVisibility(View.VISIBLE);
                loader.setVisibility(View.GONE);
            }
            @Override
            public void onFailure(Exception e) {
                //ok = e.toString();
                alertError("Error en el servidor");
            }
        });
        webService.execute();
    }
}
