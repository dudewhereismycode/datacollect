package mx.com.madd.datacollect.activity.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import mx.com.madd.datacollect.R;

public class InventoriesViewHolder extends RecyclerView.ViewHolder{
    public TextView productName,loteSeries1,idProduct;
    public View mItemView;
    public ImageView imgproduct;

    public InventoriesViewHolder(View itemView) {
        super(itemView);
        mItemView = itemView;
        productName = (TextView)itemView.findViewById(R.id.product_name);
        loteSeries1 = (TextView)itemView.findViewById(R.id.lote_series_1);
        imgproduct = (ImageView) itemView.findViewById(R.id.fImage);
        idProduct = (TextView) itemView.findViewById(R.id.id_product);
        idProduct.setVisibility(View.INVISIBLE);
    }

    public View gettheitemview(){
        return mItemView;
    }
}
