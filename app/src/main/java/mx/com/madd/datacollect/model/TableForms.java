package mx.com.madd.datacollect.model;

import com.orm.SugarRecord;

public class TableForms extends SugarRecord {
    String name, created,clasification;
    int id_form, version, aplication_id,internal_id;

    public TableForms() {

    }

    public TableForms(String name, String created,String clasification, int id_form, int version, int aplication_id, int internal_id) {
        this.name = name;
        this.created = created;
        this.id_form = id_form;
        this.version = version;
        this.clasification = clasification;
        this.aplication_id = aplication_id;
        this.internal_id = internal_id;
    }

    public int getAplication_id() {
        return aplication_id;
    }

    public void setAplication_id(int aplication_id) {
        this.aplication_id = aplication_id;
    }

    public int getInternal_id() {
        return internal_id;
    }

    public void setInternal_id(int internal_id) {
        this.internal_id = internal_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedForm() {
        return created;
    }

    public void setCreatedForm(String created) {
        this.created = created;
    }

    public int getId_form() {
        return id_form;
    }

    public void setId_form(int id_form) {
        this.id_form = id_form;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getClasification() {
        return clasification;
    }

    public void setClasification(String clasification) {
        this.clasification = clasification;
    }
}
