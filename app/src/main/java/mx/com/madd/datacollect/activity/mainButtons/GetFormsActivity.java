package mx.com.madd.datacollect.activity.mainButtons;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.adapters.GetFormsAdapter;
import mx.com.madd.datacollect.activity.inventory.ReadFormActivity;
import mx.com.madd.datacollect.activity.listeners.GetFormsTouchListener;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.model.inventory.TableHistorial;
import mx.com.madd.datacollect.utility.EventListener;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.HttpPost;
import mx.com.madd.datacollect.utility.OnEventListener;

public class GetFormsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    GetFormsAdapter  mAdapter;
    List dataList;
    String start;
    String end;
    String searchby;

    ArrayList<String> data;
    ArrayList<String> dates;
    ArrayList<String> ids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_forms);
        Log.d("Info: onCreate", "GET FORMS ACTIVITY");

        //finding views
        final List<TableConfiguration> confieditPasswd=TableConfiguration.listAll(TableConfiguration.class);
        final List<TableUser> user=TableUser.listAll(TableUser.class);
        data = new ArrayList<String>();
        dates = new ArrayList<String>();
        ids = new ArrayList<String>();

        Intent resultIntent = getIntent();
        start = resultIntent.getStringExtra("start");
        end = resultIntent.getStringExtra("end");
        searchby = resultIntent.getStringExtra("searchby");

        String url = confieditPasswd.get(0).getFormPlatform()+ "/form/historial?appID="+user.get(0).getApplication_id();

        String postData2 = "username="+user.get(0).getUser()+"&start="+start+"&end="+end+"&searchby="+searchby;

        List<TableUser> userSession = TableUser.listAll(TableUser.class);
        String session2 = userSession.get(0).getSession();

        HttpPost webService = new HttpPost(url, "POST", postData2, session2, getApplicationContext(), new EventListener<JSONObject>() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d("INFO","Success on GETFormsActivity");
                try{
                    JSONArray jsonArray = response.getJSONArray("forms");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject oo = jsonArray.getJSONObject(i);
                        Object fecha_creacion = oo.get("fechacreacion");
                        Object id_savedform = oo.get("id_savedform");
                        Object nombre_form = oo.get("nombre_form");
                        data.add(nombre_form.toString());
                        dates.add(fecha_creacion.toString());
                        ids.add(id_savedform.toString());
                    }
                    loadData();
                } catch (JSONException e) {
                    Log.d("INFO","Error on GetFormsActivity:"+e.toString());

                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Exception e) {
                //error = e.toString();
                Toast.makeText(GetFormsActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
            }
        });
        webService.execute();


        recyclerView = findViewById(R.id.recyclerView);


        recyclerView.setHasFixedSize(true);

        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        dataList = new ArrayList<>();
        mAdapter = new GetFormsAdapter(this,dataList);
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new GetFormsTouchListener(getApplicationContext(), new GetFormsTouchListener.OnItemTouchListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent myIntent = new Intent(GetFormsActivity.this, ReadFormActivity.class);
                myIntent.putExtra("position",ids.get(position));
                startActivity(myIntent);
            }
        }));
    }

    private void loadData() {
        Log.d("INFO","Load Data");
        for(int i=0; i<data.size();i++) {
            dataList.add(new TableHistorial(data.get(i), dates.get(i),Integer.parseInt(ids.get(i))));
        }
        mAdapter = new GetFormsAdapter(this,dataList);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }

        });
        return true;
    }
}
