package mx.com.madd.datacollect.utility;

import org.json.JSONException;

public interface OnEventListener<T> {
    public void onSuccess(T object) throws JSONException;
    public void onFailure(Exception e);
}