package mx.com.madd.datacollect.activity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.model.TableForms;

public class FormsAdapter extends RecyclerView.Adapter<mx.com.madd.datacollect.activity.holders.FormsViewHolder>  implements Filterable {

    private List<TableForms> formsObject;
    private List<TableForms> formsObjectAll;
    Boolean show;

    public FormsAdapter(Context context, List<TableForms> formsObject, Boolean show) {
        this.formsObject = formsObject;
        formsObjectAll=new ArrayList<>();
        formsObjectAll.addAll(formsObject);
        this.show = show;
    }

    @Override
    public mx.com.madd.datacollect.activity.holders.FormsViewHolder onCreateViewHolder(@androidx.annotation.NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.forms_layout, parent, false);
        ImageView imageView = layoutView.findViewById(R.id.fImage);
        imageView.setImageResource(R.drawable.fllenar);
        return new mx.com.madd.datacollect.activity.holders.FormsViewHolder(layoutView);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<TableForms> filteredList = new ArrayList<>();
            if(constraint.toString().isEmpty()|| constraint.length()==0){
                filteredList.addAll(formsObjectAll);
            }else{
                for(TableForms product: formsObjectAll){
                    if(product.getName().toLowerCase().contains(constraint.toString().toLowerCase())){
                        filteredList.add(product);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            formsObject.clear();
            formsObject.addAll((Collection<? extends TableForms>) results.values);
            notifyDataSetChanged();
        }
    };
    @Override
    public void onBindViewHolder(@androidx.annotation.NonNull mx.com.madd.datacollect.activity.holders.FormsViewHolder holder, int position) {
        final TableForms currentForms = formsObject.get(position);
        String a1=currentForms.getName();
        String a2=currentForms.getCreatedForm();
        int a3=currentForms.getId_form();
        holder.nameForm.setText(a1);
        holder.createdForm.setText(a2);
        holder.idForm.setText(Integer.toString(a3));
    }

    @Override
    public int getItemCount() {
        return formsObject.size();
    }
}
