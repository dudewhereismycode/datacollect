package mx.com.madd.datacollect.activity.mainButtons;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.adapters.SendFormsAdapter;
import mx.com.madd.datacollect.model.TableSavedForm;

public class DeleteFormActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SendFormsAdapter mAdapter;
    Context context;
    int position;
    String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_form);

        recyclerView = findViewById(R.id.edit_recycler);
        GridLayoutManager mGrid = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mGrid);
        recyclerView.setHasFixedSize(true);

        List<TableSavedForm> savedForms = TableSavedForm.listAll(TableSavedForm.class);
        List<TableSavedForm> sendedForms=new ArrayList<>();
        for(int index = 0; index < savedForms.size(); index ++){
            if(savedForms.get(index).getSended()){
                sendedForms.add(savedForms.get(index));
            }
        }
        mAdapter = new SendFormsAdapter(this,sendedForms);
        recyclerView.setAdapter(mAdapter);
        context=getApplicationContext();

        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallbackItemTouchHelper);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }


    ItemTouchHelper.SimpleCallback simpleCallbackItemTouchHelper = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN ,ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT){

        @Override
        public boolean onMove(@androidx.annotation.NonNull RecyclerView recyclerView, @androidx.annotation.NonNull RecyclerView.ViewHolder viewHolder, @androidx.annotation.NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@androidx.annotation.NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            position = viewHolder.getAdapterPosition();
            View view = viewHolder.itemView;
            TextView id = view.findViewById(R.id.savedFormId);
            data = (String) id.getText();
            List<TableSavedForm> tableFormsList = TableSavedForm.find(TableSavedForm.class, "idsavedform = ?",data);
            String nameForm = tableFormsList.get(0).getName_form();
            if (direction == ItemTouchHelper.LEFT){
                String message = "¿Seguro que deseas reactivar el formulario: '"+nameForm+"'?";
                AlertDialog dialog=new AlertDialog.Builder(DeleteFormActivity.this)
                        .setTitle("Reactivar")
                        .setMessage(message)
                        .setIcon(R.mipmap.getback)
                        .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setNeutralButton("Reactivar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                List<TableSavedForm> tableFormsList = TableSavedForm.find(TableSavedForm.class, "idsavedform = ?",data);
                                tableFormsList.get(0).setSended(false);
                                tableFormsList.get(0).setSelected(false);
                                tableFormsList.get(0).save();
                                recreate();
                            }
                        })
                        .setCancelable(false)
                        .create();
                dialog.show();
            }

            mAdapter.notifyDataSetChanged();
        }

        //La magia del dibujo
        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            Paint p = new Paint();
            Bitmap icon;
            if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                View itemView = viewHolder.itemView;
                float height = (float) itemView.getBottom() - (float) itemView.getTop();
                float width = height / 3;

                if(dX < 0){ //IZQUIERDA
                    p.setColor(Color.parseColor("#D01020"));//b31302
                    RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                    c.drawRect(background,p);
                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.gear);
                    RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                    c.drawBitmap(icon,null,icon_dest,p);
                }
            }
            if(isCurrentlyActive == false){
                c.drawColor(Color.TRANSPARENT);
            }
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.delete, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }

        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.delete) {
            AlertDialog dialog=new AlertDialog.Builder(this)
                    .setTitle("Borrando")
                    .setMessage("¿Seguro que desea borrar los formularios ya enviados? recuerde que podrá consultarlos en el menú Historial")
                    .setIcon(R.drawable.deleteblack)
                    .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setNeutralButton("Borrar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            List<TableSavedForm> savedForms = TableSavedForm.listAll(TableSavedForm.class);
                            for(int index = 0; index < savedForms.size(); index ++){
                                if(savedForms.get(index).getSended()){
                                    if(savedForms.get(index).getSelected()){
                                        savedForms.get(index).delete();
                                    }
                                }
                            }
                            recreate();
                        }
                    })
                    .setCancelable(false)
                    .create();
            dialog.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
