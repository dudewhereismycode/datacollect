package mx.com.madd.datacollect.activity.holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import mx.com.madd.datacollect.R;

public class SubProductViewHolder extends RecyclerView.ViewHolder{
    public TextView productName;
    public TextView loteSeries1;
    public TextView loteSeries2;
    public TextView quantity;
    public View mItemView;

    public SubProductViewHolder(View itemView) {
        super(itemView);
        mItemView = itemView;
        productName = (TextView)itemView.findViewById(R.id.product_name);
        loteSeries1 = (TextView)itemView.findViewById(R.id.lote_series_1);
        loteSeries2 = (TextView)itemView.findViewById(R.id.lote_series_2);
        quantity = (TextView)itemView.findViewById(R.id.quantity);
    }

    public View gettheitemview(){
        return mItemView;
    }
}