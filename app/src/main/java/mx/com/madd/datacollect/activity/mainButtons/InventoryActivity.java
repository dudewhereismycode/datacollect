package mx.com.madd.datacollect.activity.mainButtons;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.inventory.PrincipalProductActivity;
import mx.com.madd.datacollect.activity.adapters.InventoriesAdapter;
import mx.com.madd.datacollect.activity.listeners.InventoriesTouchListener;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.model.dataTypes.SignType;
import mx.com.madd.datacollect.model.inventory.TablePrincipalProduct;
import mx.com.madd.datacollect.model.inventory.TableProduct;
import mx.com.madd.datacollect.model.inventory.TableSubProduct;
import mx.com.madd.datacollect.utility.Http;
import mx.com.madd.datacollect.utility.OnEventListener;

public class InventoryActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    InventoriesAdapter mAdapter;
    Context context;
    Boolean resp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);

        recyclerView = (RecyclerView) findViewById(R.id.inventories_recycler);
        GridLayoutManager mGrid = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mGrid);
        recyclerView.setHasFixedSize(true);

        List<TableProduct> products = TableProduct.listAll(TableProduct.class);
        mAdapter = new InventoriesAdapter(this, products);
        recyclerView.setAdapter(mAdapter);
        context=getApplicationContext();

        recyclerView.addOnItemTouchListener(new InventoriesTouchListener(getApplicationContext(), new InventoriesTouchListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                //Place item click action here
                TextView id = (TextView)view.findViewById(R.id.id_product);
                String data = (String) id.getText();
                List<TableProduct> products = TableProduct.find(TableProduct.class,"idproduct = ?",data);
                int idproduct = products.get(0).getIdproduct();
                Intent myIntent = new Intent(InventoryActivity.this, PrincipalProductActivity.class);
                myIntent.putExtra("idproduct",Long.toString(idproduct));
                startActivity(myIntent);
            }
        }));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.inventory, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_ws) {
            AlertDialog dialog=new AlertDialog.Builder(this)
                    .setTitle("Actualizando")
                    .setMessage("¿Deseas actualizar tu inventario? se restablecerá con lo que exista en el servidor")
                    .setIcon(R.drawable.info)
                    .setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setNeutralButton("Recargar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Boolean network = haveNetworkConnection(InventoryActivity.this);
                            if(network) {
                                reloadInventory();
                            }else{
                                Toast.makeText(InventoryActivity.this,"Necesita internet para recargar el inventario",Toast.LENGTH_LONG).show();
                            }
                        }
                    })
                    .setCancelable(false)
                    .create();
            dialog.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public void reloadInventory(){
        List<TableConfiguration> configDB = TableConfiguration.listAll(TableConfiguration.class);
        String url = configDB.get(0).getInventoryPlatform() + "/inventory";
        String store_name = configDB.get(0).getInventory_name();
        List<TableUser> userSession = TableUser.listAll(TableUser.class);
        String session = userSession.get(0).getSession();
        if(url.length()>0){
            String postData = "store_id="+store_name;
            Http webService = new Http(url, "POST", postData, session,getApplicationContext(), new OnEventListener<JSONObject>() {
                @Override
                public void onSuccess(JSONObject response){
                    TableProduct.deleteAll(TableProduct.class);
                    TablePrincipalProduct.deleteAll(TablePrincipalProduct.class);
                    TableSubProduct.deleteAll(TableSubProduct.class);
                    if(response.has("error")){
                        String error = response.optString("error");
                        if(error.equals("ok")){
                            if(response.has("principalproduct")) {
                                try {
                                    JSONArray principalproduct = response.getJSONArray("principalproduct");
                                    for (int i = 0; i < principalproduct.length(); i++) {
                                        JSONObject pProduct = principalproduct.getJSONObject(i);
                                        int id_product_unitary = Integer.parseInt(pProduct.optString("idunitary"));
                                        int id_product = Integer.parseInt(pProduct.optString("idproduct"));
                                        String productname = pProduct.optString("productName");
                                        String lote_series_1 = pProduct.optString("series1");
                                        String lote_series_2 = pProduct.optString("series2");
                                        TablePrincipalProduct inventory = new TablePrincipalProduct(id_product_unitary, id_product, productname, lote_series_1, lote_series_2);
                                        inventory.save();
                                    }
                                } catch (Exception e) {
                                }
                            }

                            if(response.has("products")){
                                try{
                                    JSONArray products = response.getJSONArray("products");
                                    for(int i = 0; i < products.length(); i++){
                                        JSONObject product = products.getJSONObject(i);
                                        int idproduct = Integer.parseInt(product.optString("idproduct"));
                                        //int total = Integer.parseInt(product.optString("idproduct"));
                                        String productName = product.optString("productName");
                                        String codesku = product.optString("sku");
                                        String seriesName1 = product.optString("seriesName1");
                                        String seriesName2 = product.optString("seriesName2");
                                        TableProduct pro = new TableProduct(idproduct,0,productName,codesku,seriesName1,seriesName2);
                                        pro.save();
                                    }
                                }catch (Exception e){
                                }
                            }

                            if(response.has("subproduct")){
                                try{
                                    JSONArray subproduct = response.getJSONArray("subproduct");
                                    for(int i = 0; i < subproduct.length(); i++){
                                        JSONObject sProduct = subproduct.getJSONObject(i);
                                        int idsub = Integer.parseInt(sProduct.optString("idsub"));
                                        int idprincipal = Integer.parseInt(sProduct.optString("idprincipal"));
                                        int total = Integer.parseInt(sProduct.optString("total"));
                                        String productName = sProduct.optString("productName");
                                        String series1 = sProduct.optString("series1");
                                        String series2 = sProduct.optString("series2");
                                        TableSubProduct subProduct = new TableSubProduct(idsub,idprincipal, total,productName,series1,series2);
                                        subProduct.save();
                                    }
                                }catch (Exception e){
                                }

                            }

                            if(response.has("iostotals")){
                                try{
                                    JSONArray iostotals = response.getJSONArray("iostotals");

                                    for(int i = 0; i < iostotals.length(); i++){
                                        JSONObject pProduct = iostotals.getJSONObject(i);

                                        String idproduct = pProduct.optString("idproduct");
                                        String totals = pProduct.optString("totals");

                                        List<TableProduct> pro = TableProduct.find(TableProduct.class,"idproduct = ?",idproduct);
                                        for(int j=0; j< pro.size(); j++){
                                            pro.get(j).setTotal(Integer.parseInt(totals));
                                            pro.get(j).save();
                                        }
                                        /*if(totals.has(Integer.toString(pro.get(i).getIdproduct()))){
                                            String total = totals.optString(Integer.toString(pro.get(i).getIdproduct()));
                                            Log.i("lkyo ID",pro.get(i).toString());
                                            Log.i("lkyo total",total);
                                            pro.get(i).setTotal(Integer.parseInt(total));
                                            pro.get(i).save();
                                        }*/
                                    }
                                }catch (Exception e){
                                    Log.i("lkyo ID","MORI");
                                }
                            }
                        }
                    }
                    recreate();
                }
                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(InventoryActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                }
            });
            webService.execute();
        }

    }

}



