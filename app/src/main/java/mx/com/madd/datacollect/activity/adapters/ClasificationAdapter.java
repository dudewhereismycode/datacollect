package mx.com.madd.datacollect.activity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mx.com.madd.datacollect.R;import mx.com.madd.datacollect.model.TableClasification;

public class ClasificationAdapter extends RecyclerView.Adapter<mx.com.madd.datacollect.activity.holders.ClasificationViewHolder>  implements Filterable {

    private List<TableClasification> formsObject;
    private List<TableClasification> formsObjectAll;
    Boolean show;

    public ClasificationAdapter(Context context, List<TableClasification> formsObject, Boolean show) {
        this.formsObject = formsObject;
        formsObjectAll=new ArrayList<>();
        formsObjectAll.addAll(formsObject);
        this.show = show;
    }

    @Override
    public mx.com.madd.datacollect.activity.holders.ClasificationViewHolder onCreateViewHolder(@androidx.annotation.NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.forms_layout, parent, false);
        ImageView imageView = layoutView.findViewById(R.id.fImage);
        imageView.setImageResource(R.drawable.fllenar);
        return new mx.com.madd.datacollect.activity.holders.ClasificationViewHolder(layoutView);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<TableClasification> filteredList = new ArrayList<>();
            if(constraint.toString().isEmpty()|| constraint.length()==0){
                filteredList.addAll(formsObjectAll);
            }else{
                for(TableClasification product: formsObjectAll){
                    if(product.getClasification().toLowerCase().contains(constraint.toString().toLowerCase())){
                        filteredList.add(product);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            formsObject.clear();
            formsObject.addAll((Collection<? extends TableClasification>) results.values);
            notifyDataSetChanged();
        }
    };
    @Override
    public void onBindViewHolder(@androidx.annotation.NonNull mx.com.madd.datacollect.activity.holders.ClasificationViewHolder holder, int position) {
        final TableClasification currentClasification = formsObject.get(position);
        String a1=currentClasification.getClasification();
        holder.nameForm.setText(a1);
    }

    @Override
    public int getItemCount() {
        return formsObject.size();
    }
}
