package mx.com.madd.datacollect.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mx.com.madd.datacollect.R;
import mx.com.madd.datacollect.activity.mainButtons.ClasificationActivity;
import mx.com.madd.datacollect.activity.mainButtons.DeleteFormActivity;
import mx.com.madd.datacollect.activity.mainButtons.EditFormActivity;
import mx.com.madd.datacollect.activity.mainButtons.HistorialActivity;
import mx.com.madd.datacollect.activity.mainButtons.InventoryActivity;
import mx.com.madd.datacollect.activity.mainButtons.SendFormsActivity;

import mx.com.madd.datacollect.activity.mainButtons.ShowFieldsActivity;
import mx.com.madd.datacollect.activity.profile.ProfileActivity;
import mx.com.madd.datacollect.model.TableClasification;
import mx.com.madd.datacollect.model.TableConfiguration;
import mx.com.madd.datacollect.model.TableEnterprise;
import mx.com.madd.datacollect.model.TableFormFields;
import mx.com.madd.datacollect.model.TableFormFieldsOptions;
import mx.com.madd.datacollect.model.TableForms;
import mx.com.madd.datacollect.model.TableSavedForm;
import mx.com.madd.datacollect.model.dataTypes.DateTimeType;
import mx.com.madd.datacollect.model.dataTypes.DateType;
import mx.com.madd.datacollect.model.dataTypes.ImageType;
import mx.com.madd.datacollect.model.dataTypes.LocationType;
import mx.com.madd.datacollect.model.dataTypes.NumberType;
import mx.com.madd.datacollect.model.dataTypes.OptionType;
import mx.com.madd.datacollect.model.dataTypes.QrType;
import mx.com.madd.datacollect.model.dataTypes.SignType;
import mx.com.madd.datacollect.model.dataTypes.TextType;
import mx.com.madd.datacollect.model.TableUser;
import mx.com.madd.datacollect.model.inventory.TablePrincipalProduct;
import mx.com.madd.datacollect.model.inventory.TableProduct;
import mx.com.madd.datacollect.model.inventory.TableSubProduct;
import mx.com.madd.datacollect.utility.OnEventListener;
import mx.com.madd.datacollect.utility.ToolBox;
import android.content.SharedPreferences;
import mx.com.madd.datacollect.utility.HttpGetApi;
import mx.com.madd.datacollect.utility.HttpPost;
import mx.com.madd.datacollect.utility.EventListener;
import android.os.Handler;
import java.util.List;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class MainActivity extends AppCompatActivity {
    List<TableUser> userSession;
    List<TableConfiguration> config;
    ImageView setLogo;
    Button inventory;
    String json_param;
    String error;
    String message,messageReady, messageUpdate, messageNew;
    JSONObject jsonObject;
    String fieldName,fieldOptionType,fieldValues,fieldUrlValidation,fieldUrlPost,fieldCreated,fieldType;
    int fieldTypeId,fieldRequired,fieldInventory,fieldLongitud,fieldPosition,fieldFieldId,fieldVersion,fieldSavedFormId;
    String optativeValue,oSavedFormId;
    int optativeForm,optativeFather,optativeFieldId,optativePosition,optativeVersion,optativeSavedFormId ;
    int newForm,updateForm,okForm;
    public void fijar_estado_de_formularios_nuevos_en_el_server(Boolean nuevo_estado){
        SharedPreferences.Editor editor = MainActivity.this.getSharedPreferences("MyPrefsFile", MainActivity.MODE_PRIVATE).edit();
        editor.putBoolean("new_forms", nuevo_estado);
        editor.commit();
    }
    public boolean hay_formularios_nuevos_en_el_server(){
        SharedPreferences prefs = MainActivity.this.getSharedPreferences("MyPrefsFile", MainActivity.this.MODE_PRIVATE);
        Boolean new_form_flag =prefs.getBoolean("new_forms",false);
        return new_form_flag;
    }
    public boolean hay_formularios_pendientes_por_terminar(){
        List<TableSavedForm> tableFormsList = TableSavedForm.find(TableSavedForm.class, "finished = ?","0");
        Boolean hay_pendientes = false;
        if (!tableFormsList.isEmpty()){
            hay_pendientes = true;
        }
        return hay_pendientes;
    }
    public boolean hay_formularios_cargados(){
        List<TableForms> allForms = TableForms.listAll(TableForms.class);
        Boolean hay_formularios = false;
        if (!allForms.isEmpty()){
            hay_formularios = true;
        }
        return hay_formularios;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inventory = findViewById(R.id.inventoryButton);
        setLogo = findViewById(R.id.imageView2);
        userSession = TableUser.listAll(TableUser.class);
        if (userSession.isEmpty()){
            logOut();
        }
        if (Build.VERSION.SDK_INT >= 25) {
            boolean mispermisos = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS) == PackageManager.PERMISSION_GRANTED;
        }

        String log = userSession.get(0).getLogo();
        if(log != null && log.length()>10){
            try{
                byte [] encodeByte= Base64.decode(log,Base64.DEFAULT);
                Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                setLogo.setImageBitmap(bitmap);
            }catch(Exception e){
                e.getMessage();
            }
        }
        if(!hay_formularios_cargados()){
            bringForms();
        } else {
            Log.d("INFO","Checking for Update");
            final ProgressDialog progress = new ProgressDialog(this);
            progress.setTitle("Validando Version de Formularios");
            progress.setMessage("Por favor espere en lo que se revisa");
            progress.show();
            if(haveNetworkConnection(MainActivity.this)){
                progress.show();
                config = TableConfiguration.listAll(TableConfiguration.class);
                String url_sun = config.get(0).getUserPlatform() + "/login";
                String url = url_sun;
                Log.d("INFO","url_sun:"+url);
                String postData = "username="+userSession.get(0).getUser()+"&password="+userSession.get(0).getPassword();
                String session = userSession.get(0).getSession();
                HttpPost webService = new HttpPost(url, "POST", postData, session,getApplicationContext(), new EventListener<JSONObject>() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        Log.d("INFO","On Success Obteniendo Token para validacion formularios");
                        userSession.get(0).setSession(response.optString("Authorized"));
                        userSession.get(0).save();
                        List<TableForms> allForms = TableForms.listAll(TableForms.class);
                        Log.d("INFO", "Length:" + allForms.size());
                        jsonObject = new JSONObject();
                        try {
                            for (TableForms currentForm : allForms) {
                                jsonObject.put("" + currentForm.getId_form(), currentForm.getVersion());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("INFO","json:"+jsonObject.toString());
                        json_param="";
                        try {
                            String q= jsonObject.toString();
                            json_param = URLEncoder.encode(q, String.valueOf(StandardCharsets.UTF_8));

                        } catch (Exception e) {
                            json_param ="";
                        }
                        Log.d("INFO","json encoded:"+json_param);
                        String url = "https://api4mars.dudewhereismy.com.mx/form/versions/"+userSession.get(0).getApplication_id()+"/compare?forms="+json_param;
                        Log.d("INFO","URL de api para validar versiones:"+url);
                        Log.d("INFO","Token:"+userSession.get(0).getSession());
                        Log.d("INFO","AppID:"+userSession.get(0).getApplication_id());
                        String my_token=userSession.get(0).getSession();
                        Log.d("INFO","New Token:"+my_token);
                        Log.d("INFO","URL get forms version:"+url);
                        HttpGetApi webServicev = new HttpGetApi(url,my_token , getApplicationContext(), new OnEventListener<JSONObject>() {
                            @Override
                            public void onSuccess(JSONObject response) {
                                Log.d("INFO","On Success validar si update formularios");
                                Log.e("lkyo response",response.toString());
                                if(response.has("changes")){
                                    //Toast.makeText(MainActivity.this,"Información correcta !",Toast.LENGTH_LONG).show();
                                    String changes = response.optString("changes");
                                    Log.d("INFO","Valor Changes:"+changes);
                                    if (changes.equals("false")){
                                        Log.d("INFO","No se actualiza !!");
                                    } else {
                                        Log.d("INFO","Se actualiza !!");
                                        if (!hay_formularios_pendientes_por_terminar()){
                                            Log.d("INFO","Actualizando Formularios");
                                            bringForms();
                                        } else {
                                            fijar_estado_de_formularios_nuevos_en_el_server(true);
                                            showAlerDialog("Alerta!","Hay formularios nuevos. Terminar de llenar/envisr formularios y Descargue los nuevos.",true);
                                        }
                                    }
                                }else{
                                    Toast.makeText(MainActivity.this,"Información obtenida incorrecta 1",Toast.LENGTH_LONG).show();
                                    ToolBox newtool = new ToolBox();
                                    newtool.sendNotification("Error Formulario","Información obtenida incorrecta 1",getApplicationContext());
                                }
                                progress.dismiss();
                            }
                            @Override
                            public void onFailure(Exception e) {
                                //error = e.toString();
                                Log.d("INFO","On on Failure validar update de formularios:"+e.toString());
                                progress.dismiss();
                                Toast.makeText(MainActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                                ToolBox newtool = new ToolBox();
                                newtool.sendNotification("Error Formulario",e.toString(),getApplicationContext());
                                //finish();
                            }
                        });
                        webServicev.execute();
                    }
                    @Override
                    public void onFailure(Exception e) {
                        ToolBox newtool = new ToolBox();
                        newtool.sendNotification("Error autentificacion",e.toString(),getApplicationContext());
                        Toast.makeText(MainActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                    }
                });
                webService.execute();
            }else{
                ToolBox newtool = new ToolBox();
                newtool.sendNotification("Error","Sin internet",getApplicationContext());
                Toast.makeText(MainActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                Toast.makeText(MainActivity.this, "Necesita internet para descargar los formularios", Toast.LENGTH_SHORT).show();
                Runnable progressRunnable = new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                };

                Handler pdCanceller = new Handler();
                pdCanceller.postDelayed(progressRunnable, 3000);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        config = TableConfiguration.listAll(TableConfiguration.class);
        String inventory_platform = "";
        if(config.isEmpty()){
            String userP = "https://api4sun.dudewhereismy.com.mx";
            String formP = "https://api4mars.dudewhereismy.com.mx";
            String inventoryP = "";   //"https://venus.dudewhereismy.com.mx/";
            String ticketP = "";      //https://venus.dudewhereismy.com.mx/";
            String inventoryN = "";
            TableConfiguration newConfig = new TableConfiguration(userP,formP,inventoryP,ticketP,inventoryN);
            newConfig.save();
        }else{
            inventory_platform = config.get(0).getInventoryPlatform();
        }

        if(inventory_platform.length()<1){
            inventory.setVisibility(View.INVISIBLE);
            TableProduct.deleteAll(TableProduct.class);
            TableSubProduct.deleteAll(TableSubProduct.class);
            TablePrincipalProduct.deleteAll(TablePrincipalProduct.class);
        }else{
            inventory.setVisibility(View.VISIBLE);
        }

    }

    public void getForms(View v){
        if (hay_formularios_nuevos_en_el_server() && hay_formularios_pendientes_por_terminar()){
            AlertDialog dialog=new AlertDialog.Builder(MainActivity.this)
                    .setTitle("No se puede acceder")
                    .setMessage("Hay formularios nuevos para descargar y hay formularios pendientes por terminar/enviar")
                    .setIcon(R.drawable.alert)
                    .setNeutralButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setCancelable(false)
                    .create();
            dialog.show();
        } else {
            bringForms();
        }
    }

    public void fillForms(View v){
        if (hay_formularios_nuevos_en_el_server() && hay_formularios_pendientes_por_terminar()){
            showAlerDialog("No se puede acceder","Hay formularios nuevos para descargar y hay formularios pendientes por terminar/enviar",true);
        } else {
            String x = config.get(0).getTicketPlatform();
            if(x.equals("Todo")){
                Intent myIntent = new Intent(MainActivity.this, ShowFieldsActivity.class);
                myIntent.putExtra("show",false);
                myIntent.putExtra("clasification","0");
                startActivity(myIntent);
            }else{
                Intent myIntent = new Intent(MainActivity.this, ClasificationActivity.class);
                myIntent.putExtra("show",false);
                startActivity(myIntent);
            }
        }
    }

    public void editForms(View v){
        Intent myIntent = new Intent(MainActivity.this, EditFormActivity.class);
        myIntent.putExtra("show",false);
        startActivity(myIntent);
    }

    public void sendForms(View v){
        Intent myIntent = new Intent(MainActivity.this, SendFormsActivity.class);
        startActivity(myIntent);
    }

    public void historialForms(View v){
        Intent myIntent = new Intent(MainActivity.this, HistorialActivity.class);
        startActivity(myIntent);
    }

    public void deleteForms(View v){
        Intent myIntent = new Intent(MainActivity.this, DeleteFormActivity.class);
        myIntent.putExtra("show",true);
        startActivity(myIntent);
    }

    public void openInventory(View v){
        Intent myIntent = new Intent(MainActivity.this, InventoryActivity.class);
        startActivity(myIntent);
    }

    public void dwimAgreement(View v){
        Intent openCreateAccount = new Intent(getApplicationContext(), DWIMActivity.class);
        startActivity(openCreateAccount);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.myprofile) {
            Intent openProfileActivity = new Intent(getApplicationContext(), ProfileActivity.class);
            startActivity(openProfileActivity);
            return true;
        }else if (id == R.id.configurations) {
            Intent openConfigurationActivity = new Intent(getApplicationContext(), ConfigurationActivity.class);
            startActivity(openConfigurationActivity);
            return true;
        }else if (id == R.id.logout) {
            logOut();
            return true;
        }else if(id == R.id.delete){
            Intent myIntent = new Intent(MainActivity.this, DeleteFormActivity.class);
            startActivity(myIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showAlerDialog(String title, String message, Boolean button){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(Html.fromHtml(message));
        if(button == true){
            builder.setPositiveButton("OK", null);
        }
        builder.create();
        builder.show();
    }

    public void logOut(){
        if(haveNetworkConnection(MainActivity.this)){
            TableUser.deleteAll(TableUser.class);
            TableSavedForm.deleteAll(TableSavedForm.class);
            TableForms.deleteAll(TableForms.class);
            TableFormFieldsOptions.deleteAll(TableFormFieldsOptions.class);
            TableFormFields.deleteAll(TableFormFields.class);
            TableEnterprise.deleteAll(TableEnterprise.class);
            TableConfiguration.deleteAll(TableConfiguration.class);
            TableClasification.deleteAll(TableClasification.class);

            TablePrincipalProduct.deleteAll(TablePrincipalProduct.class);
            TableProduct.deleteAll(TableProduct.class);
            TableSubProduct.deleteAll(TableSubProduct.class);

            TextType.deleteAll(TextType.class);
            SignType.deleteAll(SignType.class);
            QrType.deleteAll(QrType.class);
            OptionType.deleteAll(OptionType.class);
            NumberType.deleteAll(NumberType.class);
            LocationType.deleteAll(LocationType.class);
            ImageType.deleteAll(ImageType.class);
            DateType.deleteAll(DateType.class);
            DateTimeType.deleteAll(DateTimeType.class);
        }

        finish();
        Intent openLoginActivity = new Intent(getApplicationContext(),LoginActivity.class);
        startActivity(openLoginActivity);
    }

    private boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public void bringForms(){
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle("Descargando Formularios");
        progressDialog.setMessage("Tomará unos segundos...");
        config = TableConfiguration.listAll(TableConfiguration.class);
        Log.d("INFO","Descargando Formularios");
        String url_sun = config.get(0).getUserPlatform() + "/login";
        if(url_sun!=""){
            if(haveNetworkConnection(MainActivity.this)){
                progressDialog.show();
                String url = url_sun;
                String postData = "username="+userSession.get(0).getUser()+"&password="+userSession.get(0).getPassword();
                String session = userSession.get(0).getSession();
                HttpPost webService = new HttpPost(url, "POST", postData, session,getApplicationContext(), new EventListener<JSONObject>() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        Log.d("INFO","On Success Descargando Formularios");
                        //if(response != null){
                        //userSession.get(0).setInternal_id(Integer.parseInt(response.optString("internal_id")));
                        //userSession.get(0).setApplication_id(Integer.parseInt(response.optString("application_id")));
                        userSession.get(0).setSession(response.optString("Authorized"));
                        userSession.get(0).save();
                        String url = config.get(0).getFormPlatform() + "/form?appID="+userSession.get(0).getApplication_id();
                        String postData = "user="+userSession.get(0).getUser()+"&internal_id="+userSession.get(0).getInternal_id()+"&app_id="+userSession.get(0).getApplication_id();
                        Log.d("INFO","On Success URL:"+url);
                        url=url+postData;
                        Log.d("INFO","Post Data:"+postData);
                        Log.d("INFO","hola:"+url);
                        HttpGetApi webService2 = new HttpGetApi(url, userSession.get(0).getSession(), getApplicationContext(), new OnEventListener<JSONObject>() {
                            //HttpGetApi webService2 = new HttpGetApi(url, postData, userSession.get(0).getSession(), getApplicationContext(), new EventListener<JSONObject>() {
                            @Override
                            public void onSuccess(JSONObject response) {
                                Log.d("INFO","On Success webservice2 Descargando formularios:");
                                Log.e("lkyo response",response.toString());
                                if(response.has("error")){
                                    String log = response.optString("logo");
                                    userSession.get(0).setLogo(log);
                                    userSession.get(0).save();
                                    if(log.length()>10){
                                        try{
                                            byte [] encodeByte= Base64.decode(log,Base64.DEFAULT);
                                            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                                            setLogo.setImageBitmap(bitmap);
                                        }catch(Exception e){
                                            e.getMessage();
                                        }
                                    }
                                    error = response.optString("error");
                                    if(error.equals("ok")){
                                        if(response.has("allforms")){
                                            fijar_estado_de_formularios_nuevos_en_el_server(false);
                                            message = "";
                                            messageReady = "";
                                            messageUpdate = "";
                                            messageNew = "";
                                            try {

                                                JSONArray allFormsArray = response.getJSONArray("allforms");

                                                for(int i=0; i<allFormsArray.length(); i++){
                                                    JSONObject dataFromArray = allFormsArray.getJSONObject(i);
                                                    if(dataFromArray.has("Form"+Integer.toString(i+1))){
                                                        JSONArray formsArray = dataFromArray.getJSONArray("Form"+(i+1));
                                                        for(int j=0; j<formsArray.length(); j++){
                                                            JSONObject dataForm = formsArray.getJSONObject(j);
                                                            String name = dataForm.getString("nombreform");
                                                            String created = dataForm.getString("fechacreacionform");
                                                            String clasification = dataForm.getString("clasification");
                                                            int id_form = dataForm.getInt("id_form");
                                                            int version = dataForm.getInt("version");
                                                            int application_id = userSession.get(0).getApplication_id();
                                                            int internal_id = userSession.get(0).getInternal_id();
                                                            List<TableForms> tableFormsList = TableForms.find(TableForms.class, "idform = ?",String.valueOf(id_form));
                                                            newForm = 0;
                                                            updateForm = 0;
                                                            okForm = 0;
                                                            if(!tableFormsList.isEmpty()){
                                                                tableFormsList = TableForms.find(TableForms.class, "idform = ? and version = ?",String.valueOf(id_form),String.valueOf(version));
                                                                if(!tableFormsList.isEmpty()){
                                                                    messageReady = messageReady + "-> " + name + "<br>";
                                                                    okForm = 1;
                                                                }else{
                                                                    messageUpdate = messageUpdate + "-> " + name + "<br>";
                                                                    updateForm = 1;
                                                                }
                                                            }else{
                                                                messageNew = messageNew + "-> " + name + "<br>";
                                                                newForm = 1;
                                                            }
                                                            if(okForm == 0){
                                                                JSONArray fieldsForm = dataForm.getJSONArray("campos");
                                                                for(int k=0;k<fieldsForm.length();k++){
                                                                    JSONObject fieldSelected = fieldsForm.getJSONObject(k);

                                                                    fieldName = fieldSelected.getString("nombre");
                                                                    fieldTypeId = fieldSelected.getInt("id_tipo");
                                                                    fieldPosition = fieldSelected.getInt("posicion");

                                                                    String required = fieldSelected.getString("requerido");

                                                                    fieldRequired = 0;
                                                                    if(!required.equals("null")){fieldRequired=Integer.parseInt(required);}

                                                                    String inventory = fieldSelected.getString("inventory");

                                                                    fieldInventory = 0;
                                                                    if(!inventory.equals("null")){fieldInventory=Integer.parseInt(inventory);}

                                                                    String longitud = fieldSelected.getString("longitud");
                                                                    fieldLongitud = 0;
                                                                    if(!longitud.equals("null")){fieldLongitud=Integer.parseInt(longitud);}

                                                                    String optionType = fieldSelected.getString("tipoopcion");
                                                                    fieldOptionType = "";
                                                                    if(!optionType.equals("null")){fieldOptionType=optionType;}
                                                                    String values = fieldSelected.getString("valores");
                                                                    fieldValues = "";
                                                                    if(!values.equals("null")){fieldValues=values;}

                                                                    String savedFormId = fieldSelected.getString("id_savedform");
                                                                    fieldSavedFormId = 0;
                                                                    if(!savedFormId.equals("null")){fieldSavedFormId=Integer.parseInt(savedFormId);}

                                                                    fieldUrlValidation = fieldSelected.getString("url1");
                                                                    fieldUrlPost = fieldSelected.getString("url2");
                                                                    fieldCreated = fieldSelected.getString("fechacreacion");
                                                                    fieldType = fieldSelected.getString("tipo");
                                                                    fieldFieldId = fieldSelected.getInt("id_campo");
                                                                    fieldVersion = fieldSelected.getInt("version");

                                                                    JSONArray optionalValues = new JSONArray(fieldSelected.getString("formstodo"));
                                                                    for(int x = 0; x < optionalValues.length(); x++){
                                                                        JSONObject optionalData = optionalValues.getJSONObject(x);

                                                                        optativeForm = optionalData.getInt("formtodo");
                                                                        optativeFather = optionalData.getInt("id_formparent");
                                                                        optativeFieldId = optionalData.getInt("id_campo");
                                                                        optativeValue = optionalData.getString("valor");
                                                                        optativePosition = optionalData.getInt("posicion");
                                                                        optativeVersion = optionalData.getInt("version");
                                                                        oSavedFormId = optionalData.getString("id_savedform");
                                                                        optativeSavedFormId = 0;
                                                                        if(!oSavedFormId.equals("None")){optativeSavedFormId=Integer.parseInt(oSavedFormId);}

                                                                        if(updateForm == 1){
                                                                            int deleteFields = TableFormFieldsOptions.deleteAll(TableFormFieldsOptions.class,"idfieldparent = ? and idfieldoption = ? and idfield = ?", String.valueOf(optativeFather),String.valueOf(optativeForm),String.valueOf(optativeFieldId));
                                                                        }
                                                                        TableFormFieldsOptions formOptionFields = new TableFormFieldsOptions(optativeValue,optativeFather,optativeForm,optativeFieldId,optativePosition,optativeVersion,optativeSavedFormId);
                                                                        formOptionFields.save();
                                                                    }
                                                                    if(updateForm == 1){
                                                                        int deleteFields = TableFormFields.deleteAll(TableFormFields.class,"idform = ? and idfield = ?",String.valueOf(id_form),String.valueOf(fieldFieldId));
                                                                    }

                                                                    TableFormFields formFields = new TableFormFields(fieldOptionType,fieldValues,fieldUrlPost,fieldUrlValidation,fieldName,fieldType,id_form,fieldFieldId,fieldRequired,fieldLongitud,fieldPosition,fieldVersion,fieldSavedFormId,fieldInventory,fieldTypeId);
                                                                    formFields.save();
                                                                }
                                                                if(newForm == 1){
                                                                    TableForms formTable = new TableForms(name,created,clasification,id_form,version,application_id,internal_id);
                                                                    formTable.save();
                                                                }else if(updateForm == 1){
                                                                    //TableForms formTable = new TableForms(name,created,id_form,version,application_id,internal_id);
                                                                    //formTable.save();

                                                                    tableFormsList = TableForms.find(TableForms.class, "idform = ? ",String.valueOf(id_form));
                                                                    TableForms formTable = tableFormsList.get(0);
                                                                    formTable.setVersion(version);
                                                                    formTable.setName(name);
                                                                    formTable.setAplication_id(application_id);
                                                                    formTable.setInternal_id(internal_id);
                                                                    formTable.save();

                                                                }

                                                                List<TableClasification> tableClasificationList = TableClasification.find(TableClasification.class, "clasification = ?",String.valueOf(clasification));
                                                                if(tableClasificationList.isEmpty()){
                                                                    TableClasification clasificationTable = new TableClasification(clasification);
                                                                    clasificationTable.save();
                                                                }
                                                            }
                                                        }
                                                    }else{
                                                        Toast.makeText(MainActivity.this,"Información obtenida incorrecta 4",Toast.LENGTH_LONG).show();
                                                        ToolBox newtool = new ToolBox();
                                                        newtool.sendNotification("Error Formulario","Información obtenida incorrecta 4",getApplicationContext());
                                                    }
                                                }
                                            } catch (JSONException e) {
                                                Toast.makeText(MainActivity.this,"Información obtenida incorrecta3 ",Toast.LENGTH_LONG).show();
                                                ToolBox newtool = new ToolBox();
                                                newtool.sendNotification("Error Formulario","Información obtenida incorrecta 3",getApplicationContext());
                                            }
                                        }else{
                                            Toast.makeText(MainActivity.this,"Información obtenida incorrecta 2",Toast.LENGTH_LONG).show();
                                            ToolBox newtool = new ToolBox();
                                            newtool.sendNotification("Error Formulario","Información obtenida incorrecta 2",getApplicationContext());
                                        }
                                    }else{
                                        Toast.makeText(MainActivity.this,error,Toast.LENGTH_LONG).show();
                                        ToolBox newtool = new ToolBox();
                                        newtool.sendNotification("Error Formulario","Información obtenida incorrecta X",getApplicationContext());
                                    }
                                }else{
                                    Toast.makeText(MainActivity.this,"Información obtenida incorrecta 1",Toast.LENGTH_LONG).show();
                                    ToolBox newtool = new ToolBox();
                                    newtool.sendNotification("Error Formulario","Información obtenida incorrecta 1",getApplicationContext());
                                }
                                progressDialog.dismiss();
                                message = "<b>Formularios Listos.</b><br>"+messageReady+"<b>Formularios Actualizados.</b><br>"+messageUpdate+"<b>Formularios Nuevos.</b><br>"+messageNew;
                                showAlerDialog("Descarga Completa",message,true);
                            }
                            @Override
                            public void onFailure(Exception e) {
                                //error = e.toString();
                                Log.d("INFO","On on Failure webservice2 Descargando formularios:"+e.toString());
                                progressDialog.dismiss();
                                Toast.makeText(MainActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                                ToolBox newtool = new ToolBox();
                                newtool.sendNotification("Error Formulario",e.toString(),getApplicationContext());
                            }
                        });
                        webService2.execute();
                        //}else{
                        //    Toast.makeText(MainActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                        //}
                    }
                    @Override
                    public void onFailure(Exception e) {
                        ToolBox newtool = new ToolBox();
                        newtool.sendNotification("Error autentificacion",e.toString(),getApplicationContext());
                        Toast.makeText(MainActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                    }
                });
                webService.execute();
            }else{
                ToolBox newtool = new ToolBox();
                newtool.sendNotification("Error","Sin internet",getApplicationContext());
                Toast.makeText(MainActivity.this,"Error en el servidor",Toast.LENGTH_LONG).show();
                Toast.makeText(MainActivity.this, "Necesita internet para descargar los formularios", Toast.LENGTH_SHORT).show();
            }

        }else{
            Toast.makeText(MainActivity.this, "Plataforma de Formularios inexistente", Toast.LENGTH_SHORT).show();
            ToolBox newtool = new ToolBox();
            newtool.sendNotification("Error","Plataforma de Formularios inexistente",getApplicationContext());
        }
    }

}
